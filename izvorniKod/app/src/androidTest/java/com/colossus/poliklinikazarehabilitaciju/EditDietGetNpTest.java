package com.colossus.poliklinikazarehabilitaciju;

import android.content.Context;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class EditDietGetNpTest {


    @Rule
    public ActivityTestRule<EditDiet> mActivityTestRule = new ActivityTestRule<EditDiet>(EditDiet.class) {
        @Override
        protected Intent getActivityIntent() {
            Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
            Intent result = new Intent(targetContext, EditDiet.class);
            result.putExtra("USER_UID", "1c2K5ug2TmQRSzWWDAa3YP7Jk6p2");
            result.putExtra("ROLE_ID", "2");
            result.putExtra("NEW", true);
            return result;
        }
    };

    private EditDiet mEditDiet = null;


    @Before
    public void setUp() throws Exception {
        mEditDiet = mActivityTestRule.getActivity();

    }

    @Test
    public void test1() {
        mEditDiet.setCarbsValue(20);
        assertEquals(20, mEditDiet.getCarbsValue());
    }

    @Test
    public void test2() {
        mEditDiet.setCarbsValue(-20);
        assertNotEquals(20, mEditDiet.getCarbsValue());
    }

    @Test
    public void test3() {
        mEditDiet.setCarbsValue(120);
        assertNotEquals(20, mEditDiet.getCarbsValue());
    }

    @Test
    public void test4() {
        mEditDiet.setCarbsValue(0);
        assertEquals(0, mEditDiet.getCarbsValue());
    }

    @Test
    public void test5() {
        mEditDiet.setCarbsValue(100);
        assertEquals(100, mEditDiet.getCarbsValue());
    }



    @After
    public void tearDown() throws Exception {
        mEditDiet = null;
    }
}