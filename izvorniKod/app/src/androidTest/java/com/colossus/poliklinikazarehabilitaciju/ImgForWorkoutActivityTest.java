package com.colossus.poliklinikazarehabilitaciju;

import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class ImgForWorkoutActivityTest {

    private ImgForWorkoutActivity mImg = null;
    private static String testString = "";

    @Rule
    public ActivityTestRule<ImgForWorkoutActivity> mActivityTestRule = new ActivityTestRule<>(ImgForWorkoutActivity.class);

    @Before
    public void setUp() throws Exception {
        mImg = mActivityTestRule.getActivity();
    }

    @Test
    public void test() {
        //ako slika nije unesena, zastavica picChanged nije postavljena (false)

        if (!mImg.picChanged) {
            testString = "Potrebno je odabrati sliku";
        }

        assertEquals("Potrebno je odabrati sliku", testString);

    }

    @After
    public void tearDown() throws Exception {
        mImg = null;
    }
}