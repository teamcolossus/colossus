package com.colossus.poliklinikazarehabilitaciju;

import android.content.Context;
import android.content.Intent;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread;
import static org.junit.Assert.*;

public class EditProfileUserTest {


    @Rule
    public ActivityTestRule<EditProfileUser> mActivityTestRule = new ActivityTestRule<EditProfileUser>(EditProfileUser.class) {
        @Override
        protected Intent getActivityIntent() {
            Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
            Intent result = new Intent(targetContext, EditProfileUser.class);
            result.putExtra("USER", new User());
            return result;
        }
    };

    private EditProfileUser mEditProfileUser = null;


    @Before
    public void setUp() throws Exception {
        mEditProfileUser = mActivityTestRule.getActivity();

    }

    @Test
    public void test1() throws Throwable {
        mEditProfileUser.currenUser = new User(null, "mail@mail.com", "ime", "kori_ime", "prezime", null);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEditProfileUser.initializeLayout();
            }
        });

        mEditProfileUser.usernameInput.setText(mEditProfileUser.currenUser.getKorisnicko_ime());

        assertEquals("kori_ime", mEditProfileUser.usernameInput.getText().toString());
    }


    @After
    public void tearDown() throws Exception {
        mEditProfileUser = null;
    }
}