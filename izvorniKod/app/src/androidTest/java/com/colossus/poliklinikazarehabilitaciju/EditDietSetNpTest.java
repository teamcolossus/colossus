package com.colossus.poliklinikazarehabilitaciju;

import android.content.Context;
import android.content.Intent;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class EditDietSetNpTest {


    @Rule
    public ActivityTestRule<EditDiet> mActivityTestRule = new ActivityTestRule<EditDiet>(EditDiet.class) {
        @Override
        protected Intent getActivityIntent() {
            Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
            Intent result = new Intent(targetContext, EditDiet.class);
            result.putExtra("USER_UID", "1c2K5ug2TmQRSzWWDAa3YP7Jk6p2");
            result.putExtra("ROLE_ID", "2");
            result.putExtra("NEW", true);
            return result;
        }
    };

    private EditDiet mEditDiet = null;


    @Before
    public void setUp() throws Exception {
        mEditDiet = mActivityTestRule.getActivity();

    }

    @Test
    public void test1() {
        int input = 50;
        int output = input/5;
        mEditDiet.setFatValue(input);
        int value = ((NumberPicker) mEditDiet.findViewById(R.id.npFat)).getValue();

        assertEquals(output, value);
    }

    @Test
    public void test2() {
        int input = 100;
        int output = input/5;
        mEditDiet.setFatValue(input);
        int value = ((NumberPicker) mEditDiet.findViewById(R.id.npFat)).getValue();

        assertEquals(output, value);
    }

    @Test
    public void test3() {
        int input = 0;
        int output = input/5;
        mEditDiet.setFatValue(input);
        int value = ((NumberPicker) mEditDiet.findViewById(R.id.npFat)).getValue();

        assertEquals(output, value);
    }

    @Test
    public void test4() {
        int input = 120;
        int output = input/5;
        mEditDiet.setFatValue(input);
        int value = ((NumberPicker) mEditDiet.findViewById(R.id.npFat)).getValue();

        assertNotEquals(output, value);
    }

    @Test
    public void test5() {
        int input = -20;
        int output = input/5;
        mEditDiet.setFatValue(input);
        int value = ((NumberPicker) mEditDiet.findViewById(R.id.npFat)).getValue();

        assertNotEquals(output, value);
    }

    @After
    public void tearDown() throws Exception {
        mEditDiet = null;
    }
}