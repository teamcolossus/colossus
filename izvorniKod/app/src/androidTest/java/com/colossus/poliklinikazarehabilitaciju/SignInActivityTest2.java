package com.colossus.poliklinikazarehabilitaciju;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.test.rule.ActivityTestRule;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class SignInActivityTest2 {

    FirebaseFirestore mFirestore;
    FirebaseAuth mFirebaseAuth;
    private SignInActivity mSignInActivity = null;
    private static String testString = "";

    @Rule
    public ActivityTestRule<SignInActivity> mActivityTestRule = new ActivityTestRule<>(SignInActivity.class);



    @Before
    public void setUp() throws Exception {

        mSignInActivity = mActivityTestRule.getActivity();
        mFirestore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
    }

    @Test
    public void testLogin() {

        mFirestore.collection("reroute")
                .document("OlRps9Etavdrql6NMmj2nq1c8EG2")
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            Reroute reroute = documentSnapshot.toObject(Reroute.class);
                            boolean confirmed = reroute.isPotvrden();

                            if (!confirmed) {
                                testString = "Čeka se potvrda admina";
                            }

                        }

                        assertEquals("Čeka se potvrda admina", testString);
                    }
                });
    }

    @After
    public void tearDown() throws Exception {
        mSignInActivity = null;
    }
}