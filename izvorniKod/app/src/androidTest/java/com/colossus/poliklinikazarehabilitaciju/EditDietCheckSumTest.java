package com.colossus.poliklinikazarehabilitaciju;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread;
import static org.junit.Assert.*;

public class EditDietCheckSumTest {


    @Rule
    public ActivityTestRule<EditDiet> mActivityTestRule = new ActivityTestRule<EditDiet>(EditDiet.class) {
        @Override
        protected Intent getActivityIntent() {
            Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
            Intent result = new Intent(targetContext, EditDiet.class);
            result.putExtra("USER_UID", "1c2K5ug2TmQRSzWWDAa3YP7Jk6p2");
            result.putExtra("ROLE_ID", "2");
            result.putExtra("NEW", true);
            return result;
        }
    };

    private EditDiet mEditDiet = null;
    private final int RED = Color.parseColor("#CE0000");
    private final int GREEN = Color.parseColor("#0ECE00");


    @Before
    public void setUp() throws Exception {
        mEditDiet = mActivityTestRule.getActivity();

    }

    @Test
    public void test1() throws Throwable {
        mEditDiet.setCarbsValue(20);
        mEditDiet.setFatValue(40);
        mEditDiet.setProtValue(40);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEditDiet.forceCheckSum();
            }
        });

        int color = ((TextView) mEditDiet.findViewById(R.id.sum)).getCurrentTextColor();
        assertEquals(GREEN, color);
    }


    @Test
    public void test2() throws Throwable {
        mEditDiet.setCarbsValue(0);
        mEditDiet.setFatValue(0);
        mEditDiet.setProtValue(0);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEditDiet.forceCheckSum();
            }
        });

        int color = ((TextView) mEditDiet.findViewById(R.id.sum)).getCurrentTextColor();
        assertEquals(RED, color);
    }

    @Test
    public void test3() throws Throwable {
        mEditDiet.setCarbsValue(0);
        mEditDiet.setFatValue(0);
        mEditDiet.setProtValue(101);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEditDiet.forceCheckSum();
            }
        });

        int color = ((TextView) mEditDiet.findViewById(R.id.sum)).getCurrentTextColor();
        assertEquals(RED, color);
    }

    @Test
    public void test4() throws Throwable {
        mEditDiet.setCarbsValue(0);
        mEditDiet.setFatValue(0);
        mEditDiet.setProtValue(-1);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEditDiet.forceCheckSum();
            }
        });

        int color = ((TextView) mEditDiet.findViewById(R.id.sum)).getCurrentTextColor();
        assertEquals(RED, color);
    }

    @Test
    public void test5() throws Throwable {
        mEditDiet.setCarbsValue(100);
        mEditDiet.setFatValue(20);
        mEditDiet.setProtValue(-20);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEditDiet.forceCheckSum();
            }
        });

        int color = ((TextView) mEditDiet.findViewById(R.id.sum)).getCurrentTextColor();
        assertEquals(RED, color);
    }

    @Test
    public void test6() throws Throwable {
        mEditDiet.setCarbsValue(100);
        mEditDiet.setFatValue(0);
        mEditDiet.setProtValue(0);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEditDiet.forceCheckSum();
            }
        });

        int color = ((TextView) mEditDiet.findViewById(R.id.sum)).getCurrentTextColor();
        assertEquals(GREEN, color);
    }

    @Test
    public void test7() throws Throwable {
        mEditDiet.setCarbsValue(0);
        mEditDiet.setFatValue(0);
        mEditDiet.setProtValue(-101);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEditDiet.forceCheckSum();
            }
        });

        int color = ((TextView) mEditDiet.findViewById(R.id.sum)).getCurrentTextColor();
        assertEquals(RED, color);
    }

    @After
    public void tearDown() throws Exception {
        mEditDiet = null;
    }
}