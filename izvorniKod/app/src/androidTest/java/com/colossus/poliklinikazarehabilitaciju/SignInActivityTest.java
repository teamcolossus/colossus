package com.colossus.poliklinikazarehabilitaciju;

import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.test.rule.ActivityTestRule;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class SignInActivityTest {

    FirebaseFirestore mFirestore;
    FirebaseAuth mFirebaseAuth;
    private SignInActivity mSignInActivity = null;
    private static String testString = "";
    private static String username = "korisnik";
    private static String pwd = "nekiKrivi";
    private static String email = "";

    @Rule
    public ActivityTestRule<SignInActivity> mActivityTestRule = new ActivityTestRule<>(SignInActivity.class);



    @Before
    public void setUp() throws Exception {

        mSignInActivity = mActivityTestRule.getActivity();
        mFirestore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
    }

    @Test
    public void testLogin() {

        mFirestore.collection("usernames").document(username).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    email = documentSnapshot.getString("email");
                    mFirebaseAuth.signInWithEmailAndPassword(email, pwd).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            testString = "Pogrešno korisničko ime ili lozinka, pokušaj ponovno";
                        }
                    });
                } else {
                    testString = "Korisničko ime ne postoji";
                }

                assertEquals("Pogrešno korisničko ime ili lozinka, pokušaj ponovno", testString);
            }
        });

    }

    @After
    public void tearDown() throws Exception {
        mSignInActivity = null;
    }
}