package com.colossus.poliklinikazarehabilitaciju;

import android.widget.EditText;
import android.widget.Toast;

import androidx.test.rule.ActivityTestRule;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread;
import static org.junit.Assert.*;

public class CreateNewWorkoutActivityTest {

    private CreateNewWorkoutActivity mNewWorkout = null;

    @Rule
    public ActivityTestRule<CreateNewWorkoutActivity> mActivityTestRule = new ActivityTestRule<>(CreateNewWorkoutActivity.class);

    @Before
    public void setUp() throws Exception {
        mNewWorkout = mActivityTestRule.getActivity();
    }

    @Test
    public void test() throws Throwable {
        final int input = -5;
        int output = Math.abs(input);

        final EditText easy = mNewWorkout.findViewById(R.id.laganoValue);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                easy.setText(Integer.toString(input));
            }
        });

        int result = Integer.parseInt(easy.getText().toString());
        assertEquals(output, result);

    }

    @After
    public void tearDown() throws Exception {
        mNewWorkout = null;
    }
}