package com.colossus.poliklinikazarehabilitaciju;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import com.google.android.gms.common.util.ArrayUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.lang.reflect.Array;

import static androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread;
import static org.junit.Assert.*;

public class SignInCheckEmailTest {


    @Rule
    public ActivityTestRule<SignInActivity> mActivityTestRule = new ActivityTestRule<>(SignInActivity.class);

    private SignInActivity mSignInActivity = null;

    @Before
    public void setUp() throws Exception {
        mSignInActivity = mActivityTestRule.getActivity();

    }

    @Test
    public void test1() throws Throwable {
        String emails[] = {"mail@mail.hr","mail@m.", "mail@.com", "mail.com", "@mail.com", "@com", "mail @mail.com"};
        boolean output[] = {true, false, false, false, false, false, false};

        boolean result[]= new boolean[7];

        for(int i = 0; i<emails.length; i++){
            result[i] = mSignInActivity.isValidEmail(emails[i]);
        }

        assertArrayEquals(output,result);
    }



    @After
    public void tearDown() throws Exception {
        mSignInActivity = null;
    }
}