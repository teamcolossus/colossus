package com.colossus.poliklinikazarehabilitaciju;

import android.widget.TextView;

import androidx.test.rule.ActivityTestRule;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class RegisterUserActivityTest {

    private RegisterUserActivity mRegister = null;
    private static String testString = "";

    @Rule
    public ActivityTestRule<RegisterUserActivity> mActivityTestRule = new ActivityTestRule<>(RegisterUserActivity.class);

    @Before
    public void setUp() throws Exception {
        mRegister = mActivityTestRule.getActivity();
    }


    @Test
    public void test() {
        TextView lozinka = mRegister.findViewById(R.id.passwordSignUpUser);
        TextView potvrdi = mRegister.findViewById(R.id.confirmPasswordSignUpUser);

        //nemam dopustenje postavljat text u ovoj dretvi
        //lozinka.setText("lozinka");
        //lozinka.setText("drugaLozinka");

        String string1 = "lozinka";
        String string2 = "drugaLozinka";

        if (!string1.equals(string2)) {
            testString = "Lozinke se ne podudaraju";
        }

        assertEquals("Lozinke se ne podudaraju", testString);

    }

    @After
    public void tearDown() throws Exception {
        mRegister = null;
    }
}