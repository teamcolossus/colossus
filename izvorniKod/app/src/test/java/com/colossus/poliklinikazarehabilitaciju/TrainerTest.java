package com.colossus.poliklinikazarehabilitaciju;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

public class TrainerTest {

    private AndroidDriver driver;

    @Before
    public void setUp() throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("deviceName", "2d0eff9");
        desiredCapabilities.setCapability("platformName", "android");
        desiredCapabilities.setCapability("appPackage", "com.colossus.poliklinikazarehabilitaciju");
        desiredCapabilities.setCapability("appActivity", "com.colossus.poliklinikazarehabilitaciju.Splash");
        desiredCapabilities.setCapability("noReset", true);

        URL remoteUrl = new URL("http://localhost:4723/wd/hub");

        driver = new AndroidDriver(remoteUrl, desiredCapabilities);
    }

    @Test
    public void sampleTest() throws InterruptedException {
        TouchAction touchAction= new TouchAction(driver);

        Thread.sleep(6000);

        //Logiranje
        MobileElement el19 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/usernameSignIn");
        el19.sendKeys("trener@mail.hr");

        MobileElement el29 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/passwordSignIn");
        el29.sendKeys("123456");

        MobileElement el35 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/btnSignIn");
        el35.click();
        Thread.sleep(3000);


        MobileElement el2 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/add_excercise_employee_wrapper");
        el2.click();
        Thread.sleep(2000);
        MobileElement el3 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/imeVjezbe");
        el3.sendKeys("pasivno pusenje");
        MobileElement el4 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/laganoValue");
        el4.sendKeys("100");
        MobileElement el5 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/srednjeValue");
        el5.sendKeys("100");
        MobileElement el6 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/intenzivnoValue");
        el6.sendKeys("100");
        MobileElement el7 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/opisVjezbe");
        el7.sendKeys("caffe bar BB");

        Thread.sleep(2000);
        touchAction.tap(PointOption.point(958, 155)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(540, 1033)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(319, 1052)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(864, 1878)).perform();
        Thread.sleep(3000);
        touchAction.tap(PointOption.point(531, 1822)).perform();
        Thread.sleep(3000);
        touchAction.tap(PointOption.point(841, 1859)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(1000, 164)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(995, 169)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(554, 2038)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(902, 376)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(451, 1850)).perform();
        Thread.sleep(2000);


        MobileElement el8 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/addExcerciseContainer");
        el8.click();
        Thread.sleep(2000);


        touchAction.tap(PointOption.point(504, 1138)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(803, 1456)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(788, 1875)).perform();
        Thread.sleep(2000);
       // touchAction.tap(PointOption.point(814, 1678)).perform();
       // Thread.sleep(2000);

        MobileElement el9 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/trainingTime");
        el9.sendKeys("300");


        Thread.sleep(2000);
        touchAction.tap(PointOption.point(985, 164)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(135, 153)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(135, 153)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(135, 153)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(193, 2014)).perform();
        Thread.sleep(2000);


        touchAction.tap(PointOption.point(135, 153)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(266, 1343)).perform();
        Thread.sleep(2000);
        Assert.assertNotNull(driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/usernameSignIn"));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}

