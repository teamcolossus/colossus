package com.colossus.poliklinikazarehabilitaciju;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;

public class AddExcerciseTest {

    private AndroidDriver driver;

    @Before
    public void setUp() throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("deviceName", "2d0eff9");
        desiredCapabilities.setCapability("platformName", "android");
        desiredCapabilities.setCapability("appPackage", "com.colossus.poliklinikazarehabilitaciju");
        desiredCapabilities.setCapability("appActivity", "com.colossus.poliklinikazarehabilitaciju.Splash");
        desiredCapabilities.setCapability("noReset", true);

        URL remoteUrl = new URL("http://localhost:4723/wd/hub");

        driver = new AndroidDriver(remoteUrl, desiredCapabilities);
    }
    //TouchAction touchAction
    //touchAction.tap(PointOption.point(77, 154)).perform();
    //touchAction.press(PointOption.point(, )).moveTo(PointOption.point(, )).release().perform();

    @Test
    public void sampleTest() throws InterruptedException {
        Thread.sleep(6000);
        TouchAction touchAction=new TouchAction(driver);

        //Logiranje
        MobileElement el19 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/usernameSignIn");
        el19.sendKeys("teamcolossusfer@gmail.com");
        MobileElement el22 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/passwordSignIn");
        el22.sendKeys("colossus123");
        MobileElement el33 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/btnSignIn");
        el33.click();
        Thread.sleep(3000);

        MobileElement el1 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/add_excercise_settings_wrapper");
        el1.click();
        Thread.sleep(2000);

        MobileElement el2 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/imeVjezbe");
        el2.sendKeys("test vjezba");
        MobileElement el3 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/laganoValue");
        el3.sendKeys("10");
        MobileElement el4 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/srednjeValue");
        el4.sendKeys("10");
        MobileElement el5 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/intenzivnoValue");
        el5.sendKeys("10");
        MobileElement el6 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/opisVjezbe");
        el6.sendKeys("opis vjezbe");

        touchAction.tap(PointOption.point(945, 159)).perform();
        Thread.sleep(2000);

        touchAction.tap(PointOption.point(558, 1005)).perform();
        Thread.sleep(2000);

        touchAction.tap(PointOption.point(330, 1051)).perform();
        Thread.sleep(2000);

        touchAction.tap(PointOption.point(539, 1864)).perform();
        Thread.sleep(2000);

        touchAction.tap(PointOption.point(847, 1850)).perform();
        Thread.sleep(2000);

        touchAction.tap(PointOption.point(998, 161)).perform();
        Thread.sleep(2000);

        touchAction.tap(PointOption.point(993, 161)).perform();
        Thread.sleep(2000);

//logoff
        touchAction.tap(PointOption.point(70, 156)).perform();
        Thread.sleep(2000);

        touchAction.tap(PointOption.point(300, 1630)).perform();
        Thread.sleep(2000);

        Assert.assertNotNull(driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/usernameSignIn"));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}