package com.colossus.poliklinikazarehabilitaciju;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;

public class DoctorTest {

    private AndroidDriver driver;

    @Before
    public void setUp() throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("deviceName", "2d0eff9");
        desiredCapabilities.setCapability("platformName", "android");
        desiredCapabilities.setCapability("appPackage", "com.colossus.poliklinikazarehabilitaciju");
        desiredCapabilities.setCapability("appActivity", "com.colossus.poliklinikazarehabilitaciju.Splash");
        desiredCapabilities.setCapability("noReset", true);

        URL remoteUrl = new URL("http://localhost:4723/wd/hub");

        driver = new AndroidDriver(remoteUrl, desiredCapabilities);
    }

    //TouchAction touchAction = new TouchAction(driver);
    //touchAction.tap(PointOption.point()).perform();
    //touchAction.press(PointOption.point(, )).moveTo(PointOption.point(, )).release().perform();
    @Test
    public void sampleTest() throws InterruptedException {
        Thread.sleep(6000);
        TouchAction touchAction = new TouchAction(driver);

        //Logiranje
        MobileElement el19 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/usernameSignIn");
        el19.sendKeys("doktor@mail.hr");
        MobileElement el2 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/passwordSignIn");
        el2.sendKeys("123456");
        MobileElement el3 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/btnSignIn");
        el3.click();
        Thread.sleep(3000);
        touchAction.tap(PointOption.point(75, 166)).perform();
        Thread.sleep(2000);

        touchAction.tap(PointOption.point(303, 1217)).perform();

        Thread.sleep(2000);

        MobileElement el4 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/maxClientsSignUpEmployee");
        el4.sendKeys("6");

        MobileElement el1 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/usernameSignUpEmployee");
        String text= el1.getText();
        if(text.equals("doktor"))
            el1.sendKeys("dokturica");
        else
            el1.sendKeys("doktor");
        Thread.sleep(2000);

        MobileElement el6 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/nameSignUpEmployee");
        el6.sendKeys("Dok");
        Thread.sleep(2000);

        MobileElement el7 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/surnameSignUpEmployee");
        el7.sendKeys("Thor");
        Thread.sleep(2000);

        touchAction.tap(PointOption.point(991, 166)).perform();
        Thread.sleep(2000);

        MobileElement el12 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/new_clients_wrapper");
        el12.click();
        Thread.sleep(2000);

        touchAction.tap(PointOption.point(522, 363)).perform();
        Thread.sleep(2000);
        touchAction.press(PointOption.point(435, 2035)).moveTo(PointOption.point(387, 582)).release().perform();

        Thread.sleep(2000);
        touchAction.tap(PointOption.point(108, 164)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(96, 161)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(539, 2011)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(486, 346)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(483, 1398)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(82, 164)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(508, 1542)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(599, 1465)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(82, 185)).perform();
        Thread.sleep(3000);//trener
        touchAction.tap(PointOption.point(82, 180)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(572, 1674)).perform();
        Thread.sleep(2000);

        //statistika back
        touchAction.tap(PointOption.point(70, 156)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(70, 156)).perform();
        Thread.sleep(2000);
        touchAction.tap(PointOption.point(170, 2000)).perform();
        Thread.sleep(2000);

        //logoff
        touchAction.tap(PointOption.point(70, 156)).perform();
        Thread.sleep(2000);

        touchAction.tap(PointOption.point(300, 1350)).perform();
        Thread.sleep(2000);

        Assert.assertNotNull(driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/usernameSignIn"));

    }

    @After
    public void tearDown() {
        driver.quit();
    }
}

