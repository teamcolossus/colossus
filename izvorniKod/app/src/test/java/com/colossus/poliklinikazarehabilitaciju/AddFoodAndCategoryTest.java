package com.colossus.poliklinikazarehabilitaciju;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.offset.PointOption;

public class AddFoodAndCategoryTest {

    private AndroidDriver driver;

    @Before
    public void setUp() throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("deviceName", "2d0eff9");
        desiredCapabilities.setCapability("platformName", "android");
        desiredCapabilities.setCapability("appPackage", "com.colossus.poliklinikazarehabilitaciju");
        desiredCapabilities.setCapability("appActivity", "com.colossus.poliklinikazarehabilitaciju.Splash");
        desiredCapabilities.setCapability("noReset", true);

        URL remoteUrl = new URL("http://127.0.0.1:4723/wd/hub");
        driver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"),desiredCapabilities);
        //driver = new AndroidDriver(remoteUrl, desiredCapabilities);
    }
    //touchAction.tap(PointOption.point(77, 154)).perform();
    //touchAction.press(PointOption.point(, )).moveTo(PointOption.point(, )).release().perform();
    @Test
    public void sampleTest() throws InterruptedException {

        Thread.sleep(5000);
        //touchAction.press(PointOption.point(, )).moveTo(PointOption.point(, )).release().perform();
        TouchAction touchAction = new TouchAction(driver);

        //Logiranje
        MobileElement el19 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/usernameSignIn");
        el19.sendKeys("teamcolossusfer@gmail.com");
        MobileElement el22 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/passwordSignIn");
        el22.sendKeys("colossus123");
        MobileElement el33 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/btnSignIn");
        el33.click();
        Thread.sleep(3000);


        touchAction.tap(PointOption.point(760, 1294)).perform();
        Thread.sleep(2000);

        //MobileElement el1 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.ImageView[1]");
        //el1.click();
        touchAction.tap(PointOption.point(800, 300)).perform();
        Thread.sleep(2000);

        MobileElement el3 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/addCategoryContainer");
        el3.click();
        Thread.sleep(2000);

        MobileElement el4 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/tvNazivKategorije");
        el4.sendKeys("articoka");
        Thread.sleep(2000);

        MobileElement el5 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/action_menu_done");
        el5.click();
        Thread.sleep(2000);

        touchAction.tap(PointOption.point(77, 154)).perform();
        Thread.sleep(2000);


        MobileElement el7 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/add_food_settings_wrapper");
        el7.click();
        Thread.sleep(2000);
        MobileElement el8 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/nazivProizvodaca");
        el8.sendKeys("naziv");
        MobileElement el9 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/nazivProizvoda");
        el9.sendKeys("naziv");
        MobileElement el10 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/barkod");
        el10.sendKeys("123456789");



        Thread.sleep(2000);
        //MobileElement el12 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[7]");
        //el12.click();
        MobileElement el13 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/kalorije");
        el13.sendKeys("10");

        MobileElement el14 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/masnoca");
        el14.sendKeys("10");

        MobileElement el15 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/zasiceneMasneKiseline");
        el15.sendKeys("10");

        MobileElement el16 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/ugljikohidrati");
        el16.sendKeys("10");

        MobileElement el17 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/seceri");
        el17.sendKeys("10");

        touchAction.press(PointOption.point(565,1778 )).moveTo(PointOption.point(565, 940)).release().perform();

        MobileElement el18 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/bjelancevine");
        el18.sendKeys("10");

        MobileElement el134 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/sol");
        el134.sendKeys("10");




        MobileElement el1 = (MobileElement) driver.findElementById("android:id/text1");
        el1.click();
        Thread.sleep(2000);


        touchAction.tap(PointOption.point(649, 1188)).perform();

        MobileElement el20 = (MobileElement) driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/action_menu_done");
        el20.click();
        Thread.sleep(2000);


        touchAction.press(PointOption.point(1010,150 )).perform();

        //logoff
        touchAction.tap(PointOption.point(70, 156)).perform();
        Thread.sleep(2000);

        touchAction.tap(PointOption.point(300, 1630)).perform();
        Thread.sleep(2000);

        Assert.assertNotNull(driver.findElementById("com.colossus.poliklinikazarehabilitaciju:id/usernameSignIn"));

    }

    @After
    public void tearDown() {
        driver.quit();
    }
}

