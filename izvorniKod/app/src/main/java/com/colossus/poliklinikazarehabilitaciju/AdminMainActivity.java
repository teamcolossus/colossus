package com.colossus.poliklinikazarehabilitaciju;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class AdminMainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navigationView;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseFirestore mFirestore;
    private FirebaseStorage mStorage;
    private LinearLayout navHeader;
    private TextView tvUsername, tvName;
    private ImageView profileNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
        mStorage = FirebaseStorage.getInstance();

        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation_admin);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

        initializeNavigationDrawer();
        initializeNavHeader();

        //I added this if statement to keep the selected fragment when rotating the device
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new HomeAdminFragment()).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    private void initializeNavigationDrawer() {
        drawerLayout = findViewById(R.id.activity_main_admin);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);

        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigationView = findViewById(R.id.navigationDrawer);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.drawer_clients:
                        Intent intToUsers = new Intent(AdminMainActivity.this, AllUsersActivity.class);
                        startActivity(intToUsers);
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.drawer_employees:
                        Intent intToEmployees = new Intent(AdminMainActivity.this, AllEmployeesActivity.class);
                        startActivity(intToEmployees);
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.drawer_waiting:
                        findViewById(R.id.nav_waiting).performClick();
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.drawer_food_add:
                        Intent intToFoodAdd = new Intent(AdminMainActivity.this, FoodAddActivity.class);
                        startActivity(intToFoodAdd);
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.drawer_food_edit:
                        Intent intToFoodList = new Intent(AdminMainActivity.this, EditFoodTabActivity.class);
                        startActivity(intToFoodList);
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.drawer_excercise_add:
                        Intent insertExercise = new Intent(AdminMainActivity.this, CreateNewWorkoutActivity.class);
                        startActivity(insertExercise);
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.drawer_excercise_edit:
                        Intent exerciseList = new Intent(AdminMainActivity.this, ExerciseListActivity.class);
                        startActivity(exerciseList);
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.signOutDrawer:
                        FirebaseAuth.getInstance().signOut();
                        Intent intToMain = new Intent(AdminMainActivity.this, SignInActivity.class);
                        startActivity(intToMain);
                        drawerLayout.closeDrawer(GravityCompat.START);
                        finish();
                        break;
                    default:
                        return true;
                }
                return true;
            }
        });


    }

    private void initializeNavHeader(){
        NavigationView nesto = findViewById(R.id.navigationDrawer);
        View navigationHeaderWrapper = nesto.getHeaderView(0);
        navHeader = navigationHeaderWrapper.findViewById(R.id.navigation_header_container);
        tvName = navigationHeaderWrapper.findViewById(R.id.nav_user_full_name);
        tvUsername = navigationHeaderWrapper.findViewById(R.id.nav_user_name);
        navHeader.setClickable(false);
        profileNav = navigationHeaderWrapper.findViewById(R.id.nav_profile_image);

        mFirestore.collection("zaposlenik")
                .document(mFirebaseAuth.getCurrentUser().getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            String name = documentSnapshot.getString("ime");
                            String surname = documentSnapshot.getString("prezime");
                            String username = documentSnapshot.getString("korisnicko_ime");

                            tvName.setText(name + " " + surname);
                            tvUsername.setText("@" + username);
                            final String filepath = "profilePictures/" + mFirebaseAuth.getCurrentUser().getUid() + ".jpg";
                            mStorage.getReference().child(filepath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    StorageReference mImageRef = mStorage.getReference(filepath);
                                    GlideApp.with(getApplicationContext())
                                            .load(mImageRef)
                                            .into(profileNav);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    StorageReference mImageRef = mStorage.getReference("profilePictures/default.jpg");
                                    GlideApp.with(getApplicationContext())
                                            .load(mImageRef)
                                            .into(profileNav);
                                }
                            });
                        }
                    }
                });

//        navHeader.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String uid = mFirebaseAuth.getCurrentUser().getUid();
//                Toast.makeText(getApplicationContext(), uid, Toast.LENGTH_SHORT).show();
//            }
//        });
    }


    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;

                    switch (item.getItemId()) {
                        case R.id.nav_home:
                            selectedFragment = new HomeAdminFragment();
                            break;
                        case R.id.nav_waiting:
                            selectedFragment = new ConfirmEmployeeFragment();
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            selectedFragment).commit();

                    return true;
                }
            };
}