package com.colossus.poliklinikazarehabilitaciju;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class FoodDiaryAdapter extends RecyclerView.Adapter<FoodDiaryAdapter.ViewHolder> {
    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    public List<DiaryFood> foodDiaryList;
    public Context context;
    private String userId;
    private String date;


    public FoodDiaryAdapter(List<DiaryFood> foodDiaryList, String userId,String date) {
        this.foodDiaryList = foodDiaryList;
        this.userId = userId;
        this.date = date;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_diary_food, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final DiaryFood currentDiaryElement = foodDiaryList.get(position);
        final String barkod = currentDiaryElement.getBarkod();
        final int masa = currentDiaryElement.getMasa();

        mFirebaseFirestore.collection("hrana")
                .document(barkod)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            Product currentProduct = documentSnapshot.toObject(Product.class);

                            int caloriesValue = Math.round((float) currentProduct.getEnergija() / 100 * masa);

                            holder.brand.setText(currentProduct.getProizvodac());
                            holder.name.setText(currentProduct.getNaziv());
                            holder.mass.setText(Integer.toString(masa));
                            holder.calories.setText(Integer.toString(caloriesValue));

                            final String CATEGORY = currentProduct.getKategorija();
                            final String BARCODE = currentProduct.getBarkod();

                            holder.elementContainer.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(context, WatchConsumedFoodActivity.class);
                                    intent.putExtra("USER", userId);
                                    intent.putExtra("CONSUMED_ID", currentDiaryElement.getUid());
                                    intent.putExtra("DATE", date);
                                    context.startActivity(intent);
                                }
                            });

                            //warning
                            final float carb = currentProduct.getUgljikohidrati();
                            final float prot = currentProduct.getBjelancevine();
                            final float fat = currentProduct.getMasnoca();
                            final float salt = currentProduct.getSol();
                            final float saturated = currentProduct.getZas_masne_kiseline();
                            final float sugar = currentProduct.getSeceri();

                            final float carbR = (float) carb / 100 * masa;
                            final float protR = (float) prot / 100 * masa;
                            final float fatR = (float) fat / 100 * masa;
                            final float saltR = (float) salt / 100 * masa;
                            final float saturatedR = (float) saturated / 100 * masa;
                            final float sugarR = (float) sugar / 100 * masa;

                            mFirebaseFirestore.collection("dijeta").document(userId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    if (documentSnapshot.exists()) {
                                        Diet diet = documentSnapshot.toObject(Diet.class);

                                        if (!diet.getNoProteini().isEmpty()) {
                                            if (protR > Float.valueOf(diet.getNoProteini())) {
                                                holder.elementContainer.setBackgroundResource(R.drawable.background_warning);
                                                holder.warn.setVisibility(View.VISIBLE);
                                            }
                                        }
                                        if (!diet.getNoUgljikohidrati().isEmpty()) {
                                            if (carbR > Float.valueOf(diet.getNoUgljikohidrati())) {
                                                holder.elementContainer.setBackgroundResource(R.drawable.background_warning);
                                                holder.warn.setVisibility(View.VISIBLE);
                                            }
                                        }
                                        if (!diet.getNoMasti().isEmpty()) {
                                            if (fatR > Float.valueOf(diet.getNoMasti())) {
                                                holder.elementContainer.setBackgroundResource(R.drawable.background_warning);
                                                holder.warn.setVisibility(View.VISIBLE);
                                            }
                                        }
                                        if (!diet.getNoSecer().isEmpty()) {
                                            if (sugarR > Float.valueOf(diet.getNoSecer())) {
                                                holder.elementContainer.setBackgroundResource(R.drawable.background_warning);
                                                holder.warn.setVisibility(View.VISIBLE);
                                            }
                                        }
                                        if (!diet.getNoSol().isEmpty()) {
                                            if (saltR > Float.valueOf(diet.getNoSol())) {
                                                holder.elementContainer.setBackgroundResource(R.drawable.background_warning);
                                                holder.warn.setVisibility(View.VISIBLE);
                                            }
                                        }
                                        if (!diet.getNoZasicene().isEmpty()) {
                                            if (saturatedR > Float.valueOf(diet.getNoZasicene())) {
                                                holder.elementContainer.setBackgroundResource(R.drawable.background_warning);
                                                holder.warn.setVisibility(View.VISIBLE);
                                            }
                                        }

                                        mFirebaseFirestore.collection("dijeta").document(userId).collection("hrana").document(BARCODE).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                            @Override
                                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                if (documentSnapshot.exists()) {
                                                    holder.elementContainer.setBackgroundResource(R.drawable.background_warning);
                                                    holder.warn.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        });
                                        mFirebaseFirestore.collection("dijeta").document(userId).collection("kategorija").document(CATEGORY).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                            @Override
                                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                if (documentSnapshot.exists()) {
                                                    holder.elementContainer.setBackgroundResource(R.drawable.background_warning);
                                                    holder.warn.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
    }

    @Override
    public int getItemCount() {
        return foodDiaryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        public TextView name, mass, calories, brand, warn;
        public RelativeLayout elementContainer;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            brand = (TextView) mView.findViewById(R.id.tvBrand);
            name = (TextView) mView.findViewById(R.id.tvName);
            mass = mView.findViewById(R.id.tvMass);
            warn = mView.findViewById(R.id.tvWarning);
            calories = mView.findViewById(R.id.tvCalories);
            elementContainer = mView.findViewById(R.id.adapter_wrapper);
        }


    }
}
