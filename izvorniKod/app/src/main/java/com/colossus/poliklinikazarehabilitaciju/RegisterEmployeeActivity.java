package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class RegisterEmployeeActivity extends AppCompatActivity {

    EditText usernameInput, nameInput, surnameInput, emailInput, passwordInput, confrimPasswordInput, maxClientsInput;
    FirebaseAuth mFirebaseAuth;
    FirebaseFirestore mFirestore;
    ImageView profilePic, addPhotoIcon;
    FirebaseStorage mStorage;
    StorageReference storageRef;
    Uri picForUpload;
    boolean picChanged = false;
    int userType;

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_employee);

        mFirestore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mStorage = FirebaseStorage.getInstance();
        storageRef = mStorage.getReference();
        usernameInput = findViewById(R.id.usernameSignUpEmployee);
        nameInput = findViewById(R.id.nameSignUpEmployee);
        surnameInput = findViewById(R.id.surnameSignUpEmployee);
        emailInput = findViewById(R.id.emailSignUpEmployee);
        passwordInput = findViewById(R.id.passwordSignUpEmployee);
        confrimPasswordInput = findViewById(R.id.confirmPasswordSignUpEmployee);
        maxClientsInput = findViewById(R.id.maxClientsSignUpEmployee);
        profilePic = findViewById(R.id.registerPicture);
        addPhotoIcon = findViewById(R.id.addPhotoIcon);

        maxClientsInput.requestFocus();

        //trebalo bi smislit koju default vrijednost
        userType = getIntent().getIntExtra("EXTRA_USER_TYPE", 2);
        setTitle("Registracija " + (userType == 2 ? "doktora" : "trenera"));

        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                ImagePicker.Companion.with(RegisterEmployeeActivity.this)
                        .crop(1f,1f)
                        .compress(1024)
                        .maxResultSize(1080, 1080)
                        .start();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImagePicker.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                if (data == null) {
                    Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
                    return;
                }
                picForUpload = data.getData();
                profilePic.setImageURI(picForUpload);
                picChanged = true;
                addPhotoIcon.setVisibility(View.GONE);

                //You can get File object from intent
                File file = ImagePicker.Companion.getFile(data);

                //You can also get File Path from intent
                String filePath = ImagePicker.Companion.getFilePath(data);
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.Companion.getError(data), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Ne bi trebao dobiti ovu gresku", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu_done:
                final String email = emailInput.getText().toString().trim();
                final String username = usernameInput.getText().toString().trim();
                final String name = nameInput.getText().toString().trim();
                final String surname = surnameInput.getText().toString().trim();
                final String password = passwordInput.getText().toString();
                String confirmPassword = confrimPasswordInput.getText().toString();
                String maxClientsString = maxClientsInput.getText().toString();
                final int maxClients = maxClientsString.equals("") ? -1 : Integer.parseInt(maxClientsString);
                boolean valid = true;


                if (confirmPassword.isEmpty()) {
                    confrimPasswordInput.setError("Potvrdi lozinku");
                    confrimPasswordInput.requestFocus();
                    valid = false;
                }
                //TODO: valid password

                if (password.isEmpty()) {
                    passwordInput.setError("Upiši lozinku");
                    passwordInput.requestFocus();
                    valid = false;
                } else if (!password.equals(confirmPassword) && !confirmPassword.isEmpty()) {
                    confrimPasswordInput.setError("Lozinke se ne podudaraju");
                    confrimPasswordInput.requestFocus();
                    valid = false;
                }


                if (surname.isEmpty()) {
                    surnameInput.setError("Upiši svoje prezime");
                    surnameInput.requestFocus();
                    valid = false;
                }

                if (name.isEmpty()) {
                    nameInput.setError("Upiši svoje ime");
                    nameInput.requestFocus();
                    valid = false;
                }

                if (username.isEmpty()) {
                    usernameInput.setError("Upiši korisničko ime");
                    usernameInput.requestFocus();
                    valid = false;
                }

                //TODO: provjeriti unikatnost usernamea i sadrzi li dobar format

                if (email.isEmpty()) {
                    emailInput.setError("Upiši email");
                    emailInput.requestFocus();
                    valid = false;
                } else if (!isValidEmail(email)) {
                    emailInput.setError("Email nije dobrog formata");
                    emailInput.requestFocus();
                    valid = false;
                }

                if (maxClientsString.isEmpty()) {
                    maxClientsInput.setError("Upiši maksimalan broj klijenata");
                    maxClientsInput.requestFocus();
                    valid = false;
                } else if (maxClients <= 0) {
                    maxClientsInput.setError("Maksimalan broj klijenata mora biti veći od 0");
                    maxClientsInput.requestFocus();
                    valid = false;
                }

                if (!picChanged){
                    AlertDialog alertDialog = new AlertDialog.Builder(RegisterEmployeeActivity.this).create();
                    alertDialog.setTitle("Nepotpuna prijava:");
                    alertDialog.setMessage("Potrebno je postaviti sliku profila.");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                    valid = false;
                }

                if (valid && picChanged) {
                    mFirestore.collection("usernames").document(username).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if(documentSnapshot.exists()){
                                usernameInput.setError("Korisničko ime zauzeto");
                                usernameInput.requestFocus();
                            } else {
                                mFirebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(RegisterEmployeeActivity.this, "Došlo je do greške, pokušaj ponovno", Toast.LENGTH_SHORT).show();
                                        } else {
                                            String uid = mFirebaseAuth.getCurrentUser().getUid();

                                            Map<String, Object> employee = new HashMap<>();
                                            employee.put("max_br_korisnika", maxClients);
                                            employee.put("email", email);
                                            employee.put("korisnicko_ime", username);
                                            employee.put("ime", name);
                                            employee.put("prezime", surname);
                                            employee.put("potvrden", false);
                                            employee.put("trenutno_korisnika", 0);
                                            employee.put("broj_recenzija", 0);
                                            employee.put("zbroj_ocjena", 0);
                                            employee.put("id_ovlasti", Integer.toString(userType));

                                            Map<String, Object> reroute = new HashMap<>();
                                            reroute.put("id_ovlasti", Integer.toString(userType));
                                            reroute.put("potvrden", false);
                                            reroute.put("delete", false);

                                            Map<String, Object> uName = new HashMap<>();
                                            uName.put("email", email);

                                            mFirestore.collection("reroute")
                                                    .document(uid)
                                                    .set(reroute);

                                            mFirestore.collection("zaposlenik")
                                                    .document(uid)
                                                    .set(employee);

                                            mFirestore.collection("usernames")
                                                    .document(username)
                                                    .set(uName);

                                            //Upload slike
                                            StorageReference riversRef = storageRef.child("profilePictures/" + uid + ".jpg");
                                            riversRef.putFile(picForUpload);
                                        }

                                        Intent intToSignIn = new Intent(getApplicationContext(), ReroutingActivity.class);
                                        startActivity(intToSignIn);
                                        finish();
                                    }
                                });
                            }
                        }
                    });

                }
                break;

        }
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    Log.d("focus", "touchevent");
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}