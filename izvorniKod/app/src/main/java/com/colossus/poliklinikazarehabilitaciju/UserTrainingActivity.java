package com.colossus.poliklinikazarehabilitaciju;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class UserTrainingActivity extends AppCompatActivity {

    private TextView tvTrainingDate;
    private LinearLayout dateContainer;
    private RecyclerView mMainList;
//    private RelativeLayout addExercise;
    private FirebaseFirestore mFirestore;
    private UserTrainingAdapter userTrainingAdapter;
    private List<Training> trainingList;

    private String userUid;
    private boolean ADD;
    private String selectedDate, currentDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_training);

        userUid = (String) getIntent().getSerializableExtra("userUid");
        ADD = (boolean) getIntent().getSerializableExtra("ADD");

        mFirestore = FirebaseFirestore.getInstance();

        tvTrainingDate = findViewById(R.id.tvTrainingDate);
        dateContainer = findViewById(R.id.dateContainerUser);

        currentDate = new SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(new Date());
        int dayT = Integer.parseInt(currentDate.substring(0, 2));
        int monthT = Integer.parseInt(currentDate.substring(2, 4));
        int yearT = Integer.parseInt(currentDate.substring(4, 8));
        selectedDate = currentDate;

        getExerForTraining(yearT, monthT, dayT);

        dateContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(tvTrainingDate);
            }
        });

        setTitle("Trening");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if(ADD){
            setTitle("Odaberi vježbu");
        }

    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerUserExerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void getExerForTraining(int year, int month, int day) {
        selectedDate = String.format("%02d%02d%04d", day, month, year);
        String selectedDateText = String.format("%d.%d.%d.", day, month, year);
        tvTrainingDate.setText(selectedDateText);
        if (Integer.parseInt(selectedDate) == Integer.parseInt(currentDate)) {
            tvTrainingDate.setText("Danas");
        } else if (Integer.parseInt(selectedDate) == Integer.parseInt(currentDate) - 1000000) {
            tvTrainingDate.setText("Jučer");
        }else if (Integer.parseInt(selectedDate) == Integer.parseInt(currentDate) + 1000000) {
            tvTrainingDate.setText("Sutra");
        }

        trainingList = new ArrayList<>();
        userTrainingAdapter = new UserTrainingAdapter(trainingList, selectedDate, userUid, ADD);

        mMainList = findViewById(R.id.recViewExc);
        mMainList.setLayoutManager(new LinearLayoutManager(this));
        mMainList.setNestedScrollingEnabled(false);
        mMainList.setHasFixedSize(true);
        mMainList.setAdapter(userTrainingAdapter);

        mFirestore.collection("trening")
                .document(userUid)
                .collection("datum")
                .document(selectedDate)
                .collection("trening")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                            QueryDocumentSnapshot qds = doc.getDocument();

                            String uid = qds.getId();
                            System.out.println(uid + " " + qds.getData());

                            Training training = qds.toObject(Training.class).withUid(uid);

                            if(ADD){
                                if(training.getOdradeno()){
                                    continue;
                                }
                            }

                            trainingList.add(training);
                            userTrainingAdapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
