package com.colossus.poliklinikazarehabilitaciju;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class InsertProductActivity extends AppCompatActivity {

    TextView tvNaziv,
            tvEnergija,
            tvMasnoca,
            tvKiseline,
            tvSeceri,
            tvSol,
            tvUgljikohidrati,
            tvProizvodac,
            tvBjelancevine;

    FirebaseAuth mFirebaseAuth;
    FirebaseFirestore mFirestore;
    Spinner kategorijaProizvodaSpinner;

    String odabranProizvod = null; // u slucaju da treba urediti proizvod pa se salje id
    QueryDocumentSnapshot dokument;
    Bundle extra_params;
    ArrayList<String> sveKategorijeProizvoda = new ArrayList<>();
    ArrayAdapter<String> adapter;
    String BARCODE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_product2);

        mFirestore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();

        tvBjelancevine = findViewById(R.id.protein);
        tvEnergija = findViewById(R.id.energy);
        tvNaziv = findViewById(R.id.name);
        tvProizvodac = findViewById(R.id.brand);
        tvMasnoca = findViewById(R.id.fat);
        tvKiseline = findViewById(R.id.satFat);
        tvSeceri = findViewById(R.id.sugar);
        tvSol = findViewById(R.id.salt);
        tvUgljikohidrati = findViewById(R.id.carbs);

        BARCODE = getIntent().getStringExtra("BARCODE");


        // toolbar
        setTitle("Izmjeni proizvod");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        if (BARCODE != null) {
            mFirestore.collection("hrana")
                    .document(BARCODE)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot.exists()) {
                                Product p = documentSnapshot.toObject(Product.class);

                                tvNaziv.setText(p.getNaziv());
                                tvProizvodac.setText(p.getProizvodac());
                                tvEnergija.setText(Float.toString(p.getEnergija()));
                                tvMasnoca.setText(Float.toString(p.getMasnoca()));
                                tvKiseline.setText(Float.toString(p.getZas_masne_kiseline()));
                                tvUgljikohidrati.setText(Float.toString(p.getUgljikohidrati()));
                                tvSeceri.setText(Float.toString(p.getSeceri()));
                                tvSol.setText(Float.toString(p.getSol()));
                                tvBjelancevine.setText(Float.toString(p.getBjelancevine()));
                            }
                        }
                    });
//            mFirestore.collection("hrana")
//                    .whereEqualTo("naziv", nazivProizvoda)
//                    .get()
//                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                        @Override
//                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
//                            if(task.isSuccessful()){
//                                for(QueryDocumentSnapshot qds: task.getResult()){
//                                    dokument = qds;
//                                    odabranProizvod = dokument.getId();
//                                }
//                            }
//                            else{
//                                System.out.println("Nisam uspio pronaci proizvod");
//                            }
//                        }
//                    });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        if (item.getItemId() == R.id.action_menu_done) {
            final String naziv = tvNaziv.getText().toString();
            final String proizvodac = tvProizvodac.getText().toString();
            final String energija = tvEnergija.getText().toString();
            final String masnoca = tvMasnoca.getText().toString();
            final String kiseline = tvKiseline.getText().toString();
            final String ugljikohidrati = tvUgljikohidrati.getText().toString();
            final String bjelancevine = tvBjelancevine.getText().toString();
            final String seceri = tvSeceri.getText().toString();
            final String sol = tvSol.getText().toString();
            boolean valid = true;

            if (proizvodac.isEmpty()) {
                tvProizvodac.setError("Unesi naziv proizvođača");
                tvProizvodac.requestFocus();
                valid = false;
            }
            if (naziv.isEmpty()) {
                tvNaziv.setError("Unesi naziv proizvoda");
                tvNaziv.requestFocus();
                valid = false;
            }
            if (energija.isEmpty()) {
                tvEnergija.setError("Unesi energiju proizvoda");
                tvEnergija.requestFocus();
                valid = false;
            }
            if (masnoca.isEmpty()) {
                tvMasnoca.setError("Unesi masnocu proizvoda");
                tvMasnoca.requestFocus();
                valid = false;
            }
            if (kiseline.isEmpty()) {
                tvKiseline.setError("Unesi kiseline proizvoda");
                tvKiseline.requestFocus();
                valid = false;
            }
            if (ugljikohidrati.isEmpty()) {
                tvUgljikohidrati.setError("Unesi ugljikohidrate proizvoda");
                tvUgljikohidrati.requestFocus();
                valid = false;
            }
            if (bjelancevine.isEmpty()) {
                tvBjelancevine.setError("Unesi bjelancevine proizvoda");
                tvBjelancevine.requestFocus();
                valid = false;
            }
            if (seceri.isEmpty()) {
                tvSeceri.setError("Unesi secere proizvoda");
                tvSeceri.requestFocus();
                valid = false;
            }
            if (sol.isEmpty()) {
                tvSol.setError("Unesi soli proizvoda");
                tvSol.requestFocus();
                valid = false;
            }
            if (valid) {
                final Map<String, Object> proizvod = new HashMap<>();
                proizvod.put("naziv", naziv);
                proizvod.put("proizvodac", proizvodac);
                proizvod.put("energija", Float.parseFloat(energija));
                proizvod.put("zas_masne_kiseline", Float.parseFloat(kiseline));
                proizvod.put("ugljikohidrati", Float.parseFloat(ugljikohidrati));
                proizvod.put("bjelancevine", Float.parseFloat(bjelancevine));
                proizvod.put("masnoca", Float.parseFloat(masnoca));
                proizvod.put("seceri", Float.parseFloat(seceri));
                proizvod.put("sol", Float.parseFloat(sol));
                proizvod.put("is_deleted", false);


                mFirestore.collection("hrana")
                        .document(BARCODE)
                        .update(proizvod)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                System.out.println("Uspjesno azurirao proizvod sa idjem " + odabranProizvod);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                System.out.println("Neuspjelo azuriranje proizvoda sa idjem " + odabranProizvod);
                            }
                        });


//                Intent intent = new Intent(InsertProductActivity.this, AdminMainActivity.class);
//                startActivity(intent);
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
