package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class EditArticleFragment extends Fragment {

    private RecyclerView mMainList;
    private FirebaseFirestore mFirestore;
    private FoodListAdapter foodListAdapter;
    private List<Product> foodList;
    private RelativeLayout loadingContainer, addFoodContainer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_food_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        addFoodContainer = getView().findViewById(R.id.addFoodContainer);
        loadingContainer = getView().findViewById(R.id.loadingContainer);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Activity mainActivity = getActivity();

        mFirestore = FirebaseFirestore.getInstance();

        foodList = new ArrayList<>();
        foodListAdapter = new FoodListAdapter(foodList);

        mMainList =(RecyclerView) getView().findViewById(R.id.food_list);
        mMainList.setHasFixedSize(true);
        mMainList.setLayoutManager(new LinearLayoutManager(mainActivity));
        mMainList.setNestedScrollingEnabled(false);
        mMainList.setAdapter(foodListAdapter);

        mFirestore.collection("hrana").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (DocumentChange docChange : queryDocumentSnapshots.getDocumentChanges()) {
                    QueryDocumentSnapshot qds = docChange.getDocument();

                    String uid = qds.getId();

                    System.out.println(uid + " " + qds.getData());
                    if ((boolean) qds.get("is_deleted")) {
                        continue;
                    }
                    Product product = qds.toObject(Product.class).withUid(uid);
                    System.out.println(product.toString());
                    foodList.add(product);
                    foodListAdapter.notifyDataSetChanged();
                }
                loadingContainer.setVisibility(View.GONE);
            }
        });

        addFoodContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToInsertCategory = new Intent(mainActivity, FoodAddActivity.class);
                startActivity(intToInsertCategory);
            }
        });
    }

}
