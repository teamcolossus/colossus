package com.colossus.poliklinikazarehabilitaciju;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;


public class ExcerciseListActivity2 extends AppCompatActivity {

    private RecyclerView mMainList;
    private FirebaseFirestore mFirestore;
    private ExerciseListAdapter2 exerciseListAdapter;
    private List<Exercise> exerciseList;
    public final static int REQ = 101;

    private String userUid; // za dodavanje vjezbe korisnikovom treningu
    private String date;// za dodavanje vjezbe korisnikovom treningu

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_list);

        userUid = (String)getIntent().getSerializableExtra("user_uid");
        date = (String)getIntent().getSerializableExtra("datum");

        mFirestore = FirebaseFirestore.getInstance();

        exerciseList = new ArrayList<>();

        mFirestore.collection("vjezbe").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (DocumentChange documentChange : queryDocumentSnapshots.getDocumentChanges()) {
                    QueryDocumentSnapshot qds = documentChange.getDocument();

                    String uid = qds.getId();

                    System.out.println(uid + " " + qds.getData());
                    if ((boolean)qds.get("is_deleted")) {
                        continue;
                    }
                    Exercise exercise = qds.toObject(Exercise.class).withUid(uid);
                    exerciseList.add(exercise);
                    exerciseListAdapter.notifyDataSetChanged();
                }
            }

        });

        // toolbar
        setTitle("Odaberi vježbu treninga");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mMainList = findViewById(R.id.exercise_list);
        mMainList.setHasFixedSize(true);
        mMainList.setLayoutManager(new LinearLayoutManager(this));
        exerciseListAdapter = new ExerciseListAdapter2(exerciseList, userUid, date);
        mMainList.setAdapter(exerciseListAdapter);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ) {
            if (resultCode == RESULT_OK) {
                setResult(Activity.RESULT_OK);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
