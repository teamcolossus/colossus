package com.colossus.poliklinikazarehabilitaciju;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {
    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    public List<Review> reviewList;
    private Context context;


    public ReviewAdapter(List<Review> reviewList){
        this.reviewList = reviewList;
    }
    public ReviewAdapter(){
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( parent.getContext()).inflate(R.layout.activity_list_reviews, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Review currentReview = reviewList.get(position);
        final String senderUid = currentReview.getUid_klijent();
        final String receiverUid = currentReview.getUid_zaposlenik();


        holder.review.setText(currentReview.getOpis());
        holder.score.setText(currentReview.getOcjena());

        holder.elementContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Intent intToReview = new Intent(context, ReviewActivity.class);
                intToReview.putExtra("EMPLOYEE_UID", receiverUid);
                intToReview.putExtra("USER_UID", senderUid);
                ((Activity) context).startActivity(intToReview);
            }});
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View mView;
        public TextView review, score;
        public LinearLayout elementContainer;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            review = (TextView) mView.findViewById(R.id.review);
            score = mView.findViewById(R.id.reviewScore);
            elementContainer = mView.findViewById(R.id.reviewContainer);
        }


    }
}
