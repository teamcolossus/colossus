package com.colossus.poliklinikazarehabilitaciju;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

public class AllEmployeesAdapter extends RecyclerView.Adapter<AllEmployeesAdapter.ViewHolder> {
    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    public List<Employee> employeeList;
    private Context context;
    private FirebaseStorage mStorage = FirebaseStorage.getInstance();


    public AllEmployeesAdapter(List<Employee> employeeList){
        this.employeeList = employeeList;
    }



    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_list_users,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.nameText.setText(employeeList.get(position).getFullName());

        final Employee currentEmployee = employeeList.get(position);
        final String uid = currentEmployee.getUid();

        final String filepath = "profilePictures/" + uid + ".jpg";
        mStorage.getReference().child(filepath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                StorageReference mImageRef = mStorage.getReferenceFromUrl(uri.toString());
                GlideApp.with(context.getApplicationContext())
                        .load(mImageRef)
                        .into(holder.profileImg);
                holder.loadingContainer.setVisibility(View.GONE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                StorageReference mImageRef = mStorage.getReference("profilePictures/default.jpg");
                GlideApp.with(context.getApplicationContext())
                        .load(mImageRef)
                        .into(holder.profileImg);
                holder.loadingContainer.setVisibility(View.GONE);
            }
        });

        holder.wrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToProfile = new Intent(context, ProfileEmployeeActivity.class);
                intToProfile.putExtra("EMPLOYEE_UID", uid);
                context.startActivity(intToProfile);
            }
        });
    }

    @Override
    public int getItemCount() {
        return employeeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View mView;
        public TextView nameText;
        public RelativeLayout wrapper;
        public ImageView profileImg;
        public ProgressBar loadingContainer;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            profileImg = (ImageView) mView.findViewById(R.id.profilePicture);
            loadingContainer = (ProgressBar) mView.findViewById(R.id.progressBar);
            wrapper = mView.findViewById(R.id.userElementContainer);
            nameText = (TextView) mView.findViewById(R.id.tvUserName);
        }


    }
}
