package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ImgForWorkoutActivity extends AppCompatActivity {

    ImageView imgVjezba;
    FirebaseAuth mFirebaseAuth;
    FirebaseFirestore mFirestore;
    FirebaseStorage mStorage;
    StorageReference storageRef;
    Uri uriImg;
    boolean picChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img_for_workout2);

        imgVjezba = findViewById(R.id.imgVjezba);

        imgVjezba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                ImagePicker.Companion.with(ImgForWorkoutActivity.this)
                        .crop(1f,1f)
                        .compress(1024)
                        .maxResultSize(1080, 1080)
                        .start();
            }
        });

//        btnUslikaj.setOnClickListener(v -> {
//            Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
//            ImagePicker.Companion.with(ImgForWorkoutActivity.this)
//                    .crop(1f, 1f)
//                    .maxResultSize(246, 225)
//                    .cameraOnly()
//                    .start();
//        });
//
//        btnPrilozi.setOnClickListener(v -> {
//            Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
//            ImagePicker.Companion.with(ImgForWorkoutActivity.this)
//                    .crop(1f, 1f)
//                    .maxResultSize(246, 225)
//                    .galleryOnly()
//                    .start();
//        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImagePicker.REQUEST_CODE) {
            if (resultCode == ImgForWorkoutActivity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                if (data == null) {
                    Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
                    return;
                }
                uriImg = data.getData();
                imgVjezba.setImageURI(uriImg);
                picChanged = true;

                //You can get File object from intent
                File file = ImagePicker.Companion.getFile(data);

                //You can also get File Path from intent
                String filePath = ImagePicker.Companion.getFilePath(data);
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.Companion.getError(data), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Ne bi trebao dobiti ovu gresku", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            case R.id.action_menu_done:
                if (!picChanged) {
                    Toast.makeText(this, "Potrebno je odabrati sliku", Toast.LENGTH_SHORT).show();
                    break;
                }

                mFirebaseAuth = FirebaseAuth.getInstance();
                mFirestore = FirebaseFirestore.getInstance();
                mStorage = FirebaseStorage.getInstance();
                storageRef = mStorage.getReference();



                Map<String, Object> data = new HashMap<>();
                data.put("naziv", getIntent().getStringExtra("IME_VJEZBE"));
                data.put("opis", getIntent().getStringExtra("OPIS_VJEZBE"));
                data.put("lagano", Integer.parseInt(getIntent().getStringExtra("LAGANO_VALUE")));
                data.put("srednje", Integer.parseInt(getIntent().getStringExtra("SREDNJE_VALUE")));
                data.put("tesko", Integer.parseInt(getIntent().getStringExtra("INTENZIVNO_VALUE")));
                data.put("is_deleted", false);

                DocumentReference ref = mFirestore.collection("vjezbe").document();
                String myId = ref.getId();

                mFirestore.collection("vjezbe").document(myId).set(data)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Toast.makeText(ImgForWorkoutActivity.this, "Uspjesno dodano u bazu", Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ImgForWorkoutActivity.this, "Došlo je do greške, pokušaj ponovno", Toast.LENGTH_SHORT).show();
                    }
                });


                StorageReference vjezbeImgRef = storageRef.child("vjezbePictures/"
                        + myId + ".jpg");
                vjezbeImgRef.putFile(uriImg);
                setResult(2);
                finish();
                break;

        }
        return true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
