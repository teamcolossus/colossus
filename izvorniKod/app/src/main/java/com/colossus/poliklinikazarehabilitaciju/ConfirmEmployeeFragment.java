package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ConfirmEmployeeFragment extends Fragment {

    private static final String TAG = "FireLog";
    private RecyclerView mMainList;
    private FirebaseFirestore mFirestore;
    private EmployeeListAdapter employeeListAdapter;
    private List<Employee> employeeList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_confirm_employee, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mMainList = (RecyclerView) getView().findViewById(R.id.employeeConfirmList);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Activity mainActivity = getActivity();

        employeeList = new ArrayList<>();
        employeeListAdapter = new EmployeeListAdapter(employeeList);

        mMainList.setHasFixedSize(true);
        mMainList.setLayoutManager(new LinearLayoutManager(mainActivity));
        mMainList.setAdapter(employeeListAdapter);


        mFirestore = FirebaseFirestore.getInstance();
        mFirestore.collection("zaposlenik").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d(TAG, "Error : " + e.getMessage());
                    return;
                }

                for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                    if (doc.getType() == DocumentChange.Type.ADDED) {
                        String uid = doc.getDocument().getId();

                        Employee employee = doc.getDocument().toObject(Employee.class).withUid(uid);
                        if (employee.getPotvrden()) {
                            continue;
                        }
                        employeeList.add(employee);
                        employeeListAdapter.notifyDataSetChanged();

                    }
                    if (doc.getType() == DocumentChange.Type.MODIFIED) {
                        employeeListAdapter.notifyDataSetChanged();

                        String uid = doc.getDocument().getId();
                        Employee employee = doc.getDocument().toObject(Employee.class).withUid(uid);
                        if (employee.getPotvrden()) {
                            continue;
                        }
                        employeeList.add(employee);
                        employeeListAdapter.notifyDataSetChanged();
                    }
                }
            }
        });


    }
}
