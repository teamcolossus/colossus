package com.colossus.poliklinikazarehabilitaciju;

public class UserSparse extends  UserData {
    String ime,prezime;

    public UserSparse(){
        super();
    }

    public UserSparse(String ime, String prezime) {
        this.ime = ime;
        this.prezime = prezime;
    }

    public String getFullName() {
        return ime + " " + prezime;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }
}
