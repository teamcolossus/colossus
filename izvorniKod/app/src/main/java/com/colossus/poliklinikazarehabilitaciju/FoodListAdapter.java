package com.colossus.poliklinikazarehabilitaciju;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class FoodListAdapter extends RecyclerView.Adapter<FoodListAdapter.FoodViewHolder> {
    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    public List<Product> foodList;

    public FoodListAdapter(List<Product> foodList) {
        this.foodList = foodList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public FoodListAdapter.FoodViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_food, parent, false);
        FoodViewHolder vh = new FoodViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull final FoodViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        holder.tvProductName.setText(foodList.get(position).getNaziv());
        holder.tvBrand.setText("(" + foodList.get(position).getProizvodac() + ")");
        final Product currentProduct = foodList.get(position);
        final String uid = currentProduct.getUid();

        holder.buttonViewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // kad se klikne 3 gumbica, onda se otvara forma za edit kategorije
                Context con = v.getContext();
                Intent intent = new Intent(con, InsertProductActivity.class);
                intent.putExtra("BARCODE", currentProduct.getBarkod());
                con.startActivity(intent);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return foodList.size();
    }

    public class FoodViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        View textView;
        public TextView tvProductName, tvBrand;
        public RelativeLayout buttonViewOption;

        public FoodViewHolder(RelativeLayout v) {
            super(v);
            textView = v;

            tvBrand = (TextView) textView.findViewById(R.id.tvBrand);
            tvProductName = (TextView) textView.findViewById(R.id.tvProductName);
            buttonViewOption = textView.findViewById(R.id.adapter_wrapper);
        }
    }
}
