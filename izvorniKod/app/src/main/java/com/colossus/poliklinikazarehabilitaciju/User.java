package com.colossus.poliklinikazarehabilitaciju;

import java.io.Serializable;

public class User extends UserSparse implements Serializable {
    String doktor, email,ime,korisnicko_ime,prezime,trener;

    public User(){
        super();
    }

    public User(String doktor, String email, String ime, String korisnicko_ime, String prezime, String trener) {
        this.doktor = doktor;
        this.email = email;
        this.ime = ime;
        this.korisnicko_ime = korisnicko_ime;
        this.prezime = prezime;
        this.trener = trener;
    }


    public String getFullName() {
        return ime + " " + prezime;
    }

    public String getDoktor() {
        return doktor;
    }

    public void setDoktor(String doktor) {
        this.doktor = doktor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getKorisnicko_ime() {
        return korisnicko_ime;
    }

    public void setKorisnicko_ime(String korisnicko_ime) {
        this.korisnicko_ime = korisnicko_ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getTrener() {
        return trener;
    }

    public void setTrener(String trener) {
        this.trener = trener;
    }
}
