package com.colossus.poliklinikazarehabilitaciju;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class FoodConsumeAdapter extends RecyclerView.Adapter<FoodConsumeAdapter.ViewHolder> {
    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    public List<Product> categoryList;
    private Context context;
    private List<Product> returnList = new ArrayList<>();
    public final static int REQ = 101;


    public FoodConsumeAdapter(List<Product> categoryList) {
        this.categoryList = categoryList;
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_consume_food, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Product current = categoryList.get(position);
        final String barcode = current.getBarkod();

        holder.brandText.setText("(" + categoryList.get(position).getProizvodac() + ")");
        holder.nameText.setText(categoryList.get(position).getNaziv());

        holder.wrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToDiet = new Intent(context, EditConsume.class);
                intToDiet.putExtra("BARCODE", barcode);
                ((Activity) context).startActivityForResult(intToDiet, REQ);
            }
        });
    }

    public List<Product> getReturnList() {
        return returnList;
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        public TextView nameText, brandText;
        public RelativeLayout wrapper;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            wrapper = mView.findViewById(R.id.adapter_wrapper);
            nameText = (TextView) mView.findViewById(R.id.leftTv);
            brandText = (TextView) mView.findViewById(R.id.tvBrand);
        }

    }
}
