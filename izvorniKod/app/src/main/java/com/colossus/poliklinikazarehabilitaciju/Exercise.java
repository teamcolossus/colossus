package com.colossus.poliklinikazarehabilitaciju;

import java.io.Serializable;

public class Exercise extends UserData implements Serializable {
    private String naziv;
    private String opis;
    private Integer lagano;
    private Integer srednje;
    private Integer tesko;
    private Boolean is_deleted;

    public Exercise() {
        super();
    }

    public Exercise(String naziv, String opis, Integer lagano, Integer srednje, Integer tesko) {
        this.naziv = naziv;
        this.opis = opis;
        this.lagano = lagano;
        this.srednje = srednje;
        this.tesko = tesko;
    }

    public String getNaziv() {
        return naziv;
    }
    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
    public String getOpis() {
        return opis;
    }
    public void setOpis(String opis) {
        this.opis = opis;
    }
    public Integer getLagano() {
        return lagano;
    }
    public void setLagano(Integer lagano) {
        this.lagano = lagano;
    }
    public Integer getSrednje() {
        return srednje;
    }
    public void setSrednje(Integer srednje) {
        this.srednje = srednje;
    }
    public Integer getTesko() {
        return tesko;
    }
    public void setTesko(Integer tesko) {
        this.tesko = tesko;
    }
    public Boolean isIs_deleted() {
        return is_deleted;
    }
    public void setIs_deleted(Boolean is_deleted) {
        this.is_deleted = is_deleted;
    }
}
