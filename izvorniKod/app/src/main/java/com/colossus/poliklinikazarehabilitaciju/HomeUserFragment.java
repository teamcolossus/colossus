package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class HomeUserFragment extends Fragment {
    private LinearLayout diaryL, trainingL, dietL, statsL, doctorL, trainerL;
    FirebaseFirestore mFirestore;
    FirebaseAuth mFirebaseAuth;
    private RecyclerView mMainList;
    private HomeExAdapter homeExAdapter;
    private List<Training> trainingList;
    private Activity mainActivity;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home_user, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        diaryL = getView().findViewById(R.id.diary_wrapper);
        trainingL = getView().findViewById(R.id.my_training_wrapper);
        dietL = getView().findViewById(R.id.my_diet_wrapper);
        statsL = getView().findViewById(R.id.my_stats_wrapper);
        doctorL = getView().findViewById(R.id.my_doctor_wrapper);
        trainerL = getView().findViewById(R.id.my_trainer_wrapper);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = getActivity();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();


        trainingList = new ArrayList<>();
        homeExAdapter = new HomeExAdapter(trainingList);

        mMainList = getView().findViewById(R.id.rvHome);
        mMainList.setLayoutManager(new LinearLayoutManager(mainActivity));
        mMainList.setNestedScrollingEnabled(false);
        mMainList.setHasFixedSize(true);
        mMainList.setAdapter(homeExAdapter);


        diaryL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().findViewById(R.id.nav_diary).performClick();
            }
        });

        trainingL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mainActivity, UserTrainingActivity.class);
                intent.putExtra("userUid", mFirebaseAuth.getCurrentUser().getUid());
                intent.putExtra("ADD", false);
                startActivity(intent);
            }
        });

        dietL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToDiet = new Intent(mainActivity, MyDietActivity.class);
                intToDiet.putExtra("USER_UID", mFirebaseAuth.getCurrentUser().getUid());
                startActivity(intToDiet);
            }
        });

        statsL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), StatsActivity.class);
                intent.putExtra("USER_UID", mFirebaseAuth.getCurrentUser().getUid());
                startActivity(intent);
            }
        });

        doctorL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFirestore.collection("klijent")
                        .document(mFirebaseAuth.getCurrentUser().getUid())
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if (documentSnapshot.exists()) {
                                    String doctor = documentSnapshot.getString("doktor");

                                    if(doctor != null) {
                                        Intent intToEdit = new Intent(mainActivity, ProfileOfUsersEmployeeActivity.class);
                                        intToEdit.putExtra("EMPLOYEE_UID", doctor);
                                        startActivity(intToEdit);
                                    }else{
                                        Intent intToChooseDoc = new Intent(mainActivity, ChooseDoctor.class);
                                        startActivity(intToChooseDoc);
                                    }
                                } else {
                                    Toast.makeText(mainActivity, "Greška", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

        trainerL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFirestore.collection("klijent")
                        .document(mFirebaseAuth.getCurrentUser().getUid())
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if (documentSnapshot.exists()) {
                                    String trainer = documentSnapshot.getString("trener");

                                    if(trainer != null) {
                                        Intent intToProfile = new Intent(mainActivity, ProfileOfUsersEmployeeActivity.class);
                                        intToProfile.putExtra("EMPLOYEE_UID", trainer);
                                        startActivity(intToProfile);
                                    }else{
                                        Intent intToChooseTrainer = new Intent(mainActivity, ChooseTrainer.class);
                                        startActivity(intToChooseTrainer);
                                    }
                                } else {
                                    Toast.makeText(mainActivity, "Greška", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

        initializeRecycler();
    }

    private void initializeRecycler() {
        trainingList.clear();
        homeExAdapter.notifyDataSetChanged();

        String currentDate = new SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(new Date());

        mFirestore.collection("trening")
                .document(mFirebaseAuth.getCurrentUser().getUid())
                .collection("datum")
                .document(currentDate)
                .collection("trening")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d("HOME", "Error : " + e.getMessage());
                    return;
                }
                for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                    if (doc.getType() == DocumentChange.Type.ADDED) {
                        String uid = doc.getDocument().getId();

                        Training training = doc.getDocument().toObject(Training.class).withUid(uid);

                        if(training.getOdradeno()){
                            continue;
                        }
                        trainingList.add(training);
                        homeExAdapter.notifyDataSetChanged();

                    }
                    if (doc.getType() == DocumentChange.Type.MODIFIED) {
                        initializeRecycler();
                    }
                }
            }
        });
    }
}
