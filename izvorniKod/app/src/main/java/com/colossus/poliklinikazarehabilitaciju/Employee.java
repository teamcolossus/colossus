package com.colossus.poliklinikazarehabilitaciju;

import java.io.Serializable;

public class Employee extends UserData implements Serializable {
    int max_br_korisnika, trenutno_korisnika;
    Boolean potvrden;
    String email, id_ovlasti, ime, prezime, korisnicko_ime;
    float broj_recenzija, zbroj_ocjena;

    public String getFullName() {
        return ime + " " + prezime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId_ovlasti() {
        return id_ovlasti;
    }

    public void setId_ovlasti(String id_ovlasti) {
        this.id_ovlasti = id_ovlasti;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getKorisnicko_ime() {
        return korisnicko_ime;
    }

    public void setKorisnicko_ime(String korisnicko_ime) {
        this.korisnicko_ime = korisnicko_ime;
    }
    public Employee(){
        super();
    }

    public Employee(String email, String id_ovlasti, String ime, String prezime, String korisnicko_ime, int max_br_korisnika, int trenutno_korisnika, Boolean potvrden) {
        this.email = email;
        this.id_ovlasti = id_ovlasti;
        this.ime = ime;
        this.prezime = prezime;
        this.korisnicko_ime = korisnicko_ime;
        this.max_br_korisnika = max_br_korisnika;
        this.potvrden = potvrden;
        this.trenutno_korisnika = trenutno_korisnika;
    }

    public Employee(int max_br_korisnika, int trenutno_korisnika, Boolean potvrden, String email, String id_ovlasti, String ime, String prezime, String korisnicko_ime, float broj_recenzija, float zbroj_ocjena) {
        this.max_br_korisnika = max_br_korisnika;
        this.trenutno_korisnika = trenutno_korisnika;
        this.potvrden = potvrden;
        this.email = email;
        this.id_ovlasti = id_ovlasti;
        this.ime = ime;
        this.prezime = prezime;
        this.korisnicko_ime = korisnicko_ime;
        this.broj_recenzija = broj_recenzija;
        this.zbroj_ocjena = zbroj_ocjena;
    }

    public void setTrenutno_korisnika(int trenutno_korisnika) {
        this.trenutno_korisnika = trenutno_korisnika;
    }

    public float getBroj_recenzija() {
        return broj_recenzija;
    }

    public void setBroj_recenzija(float broj_recenzija) {
        this.broj_recenzija = broj_recenzija;
    }

    public float getZbroj_ocjena() {
        return zbroj_ocjena;
    }

    public void setZbroj_ocjena(float zbroj_ocjena) {
        this.zbroj_ocjena = zbroj_ocjena;
    }

    public int getMax_br_korisnika() {
        return max_br_korisnika;
    }

    public int getTrenutno_korisnika() {
        return trenutno_korisnika;
    }

    public void setMax_br_korisnika(int max_br_korisnika) {
        this.max_br_korisnika = max_br_korisnika;
    }

    public Boolean getPotvrden() {
        return potvrden;
    }

    public void setPotvrden(Boolean potvrden) {
        this.potvrden = potvrden;
    }

}
