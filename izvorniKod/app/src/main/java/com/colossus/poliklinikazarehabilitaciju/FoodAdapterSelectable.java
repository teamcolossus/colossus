package com.colossus.poliklinikazarehabilitaciju;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class FoodAdapterSelectable extends RecyclerView.Adapter<FoodAdapterSelectable.ViewHolder> {
    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    public List<Product> productList;
    public List<Product> oldProductList;
    private Context context;
    private List<Product> returnList = new ArrayList<>();


    public FoodAdapterSelectable(List<Product> productList, List<Product> oldProductList) {
        this.productList = productList;
        this.oldProductList = oldProductList;
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_forbidden_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.nameText.setText(productList.get(position).getNaziv());
        final Product currentCategory = productList.get(position);
        boolean click = false;

        if (oldProductList != null) {
            for (Product cat : oldProductList) {
                if (cat.getBarkod().equals(currentCategory.getBarkod())) {
                    click = true;
                }
            }
        }

        holder.wrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.isSelected) {
                    holder.select();
                    returnList.add(currentCategory);
                } else {
                    holder.select();
                    returnList.remove(currentCategory);
                }
            }
        });


        if(click){
            holder.wrapper.performClick();
        }
    }

    public List<Product> getReturnList() {
        return returnList;
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        public TextView nameText;
        public RelativeLayout wrapper;
        public boolean isSelected = false;
        ColorStateList oldColors;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            wrapper = mView.findViewById(R.id.adapter_wrapper);
            nameText = (TextView) mView.findViewById(R.id.leftTv);
            oldColors =  nameText.getTextColors();
        }

        public void select() {
            if (!isSelected) {
                wrapper.setBackground(ContextCompat.getDrawable(context, R.drawable.bottom_border_color_select));
                nameText.setTextColor(Color.WHITE);
                isSelected = !isSelected;
            } else {
                wrapper.setBackground(ContextCompat.getDrawable(context, R.drawable.bottom_border_color));
                nameText.setTextColor(oldColors);
                isSelected = !isSelected;
            }

        }
    }
}
