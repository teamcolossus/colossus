package com.colossus.poliklinikazarehabilitaciju;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class ReroutingActivity extends AppCompatActivity {
    FirebaseAuth mFirebaseAuth;
    FirebaseFirestore mFirestore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rerouting);

        mFirestore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mFirebaseAuth.getCurrentUser();

        if (currentUser == null){
            rerouteToSignIn();
            return;
        }
        Log.d("REROUTING", "Started");

        mFirestore.collection("reroute")
                .document(currentUser.getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            Reroute reroute = documentSnapshot.toObject(Reroute.class);
                            String userType = reroute.getId_ovlasti();
                            boolean confirmed = reroute.isPotvrden();
                            boolean deleted = reroute.isDelete();

                            if (deleted) {
                                rerouteToSignIn();
                            } else if (!confirmed) {
                                Toast.makeText(ReroutingActivity.this, "Čeka se potvrda admina", Toast.LENGTH_SHORT).show();
                                rerouteToSignIn();
                            } else {
                                if (userType.equals("0")) {
                                    Intent intToHomeAdmin = new Intent(ReroutingActivity.this, AdminMainActivity.class);
//                                    intToHomeAdmin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intToHomeAdmin);
                                    Log.d("REROUTING", "admin");
                                } else if (userType.equals("1")) {
                                    Intent intToHomeUser = new Intent(ReroutingActivity.this, UserMainActivity.class);
//                                    intToHomeUser.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intToHomeUser);
                                    Log.d("REROUTING", "user");

                                } else if (userType.equals("2")) {
                                    Intent intToHomeEmployee = new Intent(ReroutingActivity.this, EmployeeMainActivity.class);
//                                    intToHomeEmployee.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intToHomeEmployee);
                                    Log.d("REROUTING", "employee2");

                                } else if (userType.equals("3")) {
                                    Intent intToHomeEmployee = new Intent(ReroutingActivity.this, EmployeeMainActivity.class);
//                                    intToHomeEmployee.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intToHomeEmployee);
                                    Log.d("REROUTING", "employee3");

                                }
                                finish();
                                Log.d("REROUTING", "Finished with transition to user");
                            }
                        } else {
                            Toast.makeText(ReroutingActivity.this, "Korisnik nema vrstu ovlasti", Toast.LENGTH_SHORT).show();
                            rerouteToSignIn();
                        }
                    }
                });
    }

    private void rerouteToSignIn(){
        mFirebaseAuth.signOut();
        Intent intToSignIn = new Intent(ReroutingActivity.this, SignInActivity.class);
        startActivity(intToSignIn);
        finish();
        Log.d("REROUTING", "Finished with going back to sign in");

    }
}
