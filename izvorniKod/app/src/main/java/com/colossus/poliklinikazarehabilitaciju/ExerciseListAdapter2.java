package com.colossus.poliklinikazarehabilitaciju;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class ExerciseListAdapter2 extends RecyclerView.Adapter<ExerciseListAdapter2.ExerciseViewHolder> implements Filterable {

    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    private List<Exercise> exerciseList;
    private List<Exercise> exerciseListFull;
    public final static int REQ = 101;

    private String userUid; // za dodavanje vjezbe korisnikovom treningu, moze se ljepse napraviti
    private String date;// za dodavanje vjezbe korisnikovom treningu

    public ExerciseListAdapter2(List<Exercise> exerciseList, String userUid, String date) {
        this.exerciseList = exerciseList;
        exerciseListFull = new ArrayList<>(exerciseList);
        this.userUid = userUid;
        this.date = date;
    }

    @Override
    public ExerciseListAdapter2.ExerciseViewHolder onCreateViewHolder(ViewGroup parent,
                                                                     int viewType) {
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_exercise, parent, false);
        ExerciseViewHolder vh = new ExerciseViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ExerciseViewHolder holder, int position) {

        holder.tvExerciseName.setText(exerciseList.get(position).getNaziv());
        final Exercise currentExercise = exerciseList.get(position);
        final String uid = currentExercise.getUid();


        // koristi se kod dodavanja vjezbe korisnikovom treningu,
        // trebalo bi odvojiti, al nema smisla radit novi ExerciseListAdapter
        holder.wrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context con = v.getContext();
                Intent intent = new Intent(con, InsertTrainingActivity.class);
                intent.putExtra("naziv_vjezbe", currentExercise.getNaziv());
                intent.putExtra("user_uid", userUid);
                intent.putExtra("datum", date);
                ((Activity) con).startActivityForResult(intent, REQ);
            }
        });
    }


    @Override
    public int getItemCount() {
        return exerciseList.size();
    }

    public class ExerciseViewHolder extends RecyclerView.ViewHolder {
        View textView;
        public TextView tvExerciseName;
        public RelativeLayout wrapper;

        public ExerciseViewHolder(RelativeLayout v) {
            super(v);
            textView = v;

            tvExerciseName = (TextView) textView.findViewById(R.id.tvExerciseName);
            wrapper = textView.findViewById(R.id.adapter_wrapper);
        }
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Exercise> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(exerciseListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Exercise exc : exerciseListFull) {
                    if (exc.getNaziv().toLowerCase().contains(filterPattern)) {
                        filteredList.add(exc);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            exerciseList.clear();
            exerciseList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


}
