package com.colossus.poliklinikazarehabilitaciju;

public class Review {
    private String komentar, opis, uid_klijent, uid_zaposlenik, ocjena;

    public Review() {
    }

    public Review(String komentar, String opis, String uid_klijent, String uid_zaposlenik, String ocjena) {
        this.komentar = komentar;
        this.opis = opis;
        this.uid_klijent = uid_klijent;
        this.uid_zaposlenik = uid_zaposlenik;
        this.ocjena = ocjena;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getUid_klijent() {
        return uid_klijent;
    }

    public void setUid_klijent(String uid_klijent) {
        this.uid_klijent = uid_klijent;
    }

    public String getUid_zaposlenik() {
        return uid_zaposlenik;
    }

    public void setUid_zaposlenik(String uid_zaposlenik) {
        this.uid_zaposlenik = uid_zaposlenik;
    }

    public String getOcjena() {
        return ocjena;
    }

    public void setOcjena(String ocjena) {
        this.ocjena = ocjena;
    }
}
