package com.colossus.poliklinikazarehabilitaciju;

public class DiaryExercise{
    private String intenzitet, id_vjezba;
    private boolean odradeno;
    private int trajanje;

    public DiaryExercise() {
        super();
    }

    public DiaryExercise(String intenzitet, String id_vjezba, boolean odradeno, int trajanje) {
        this.intenzitet = intenzitet;
        this.id_vjezba = id_vjezba;
        this.odradeno = odradeno;
        this.trajanje = trajanje;
    }

    public String getId_vjezba() {
        return id_vjezba;
    }

    public void setId_vjezba(String id_vjezba) {
        this.id_vjezba = id_vjezba;
    }

    public String getIntenzitet() {
        return intenzitet;
    }

    public void setIntenzitet(String intenzitet) {
        this.intenzitet = intenzitet;
    }

    public boolean isOdradeno() {
        return odradeno;
    }

    public void setOdradeno(boolean odradeno) {
        this.odradeno = odradeno;
    }

    public int getTrajanje() {
        return trajanje;
    }

    public void setTrajanje(int trajanje) {
        this.trajanje = trajanje;
    }
}
