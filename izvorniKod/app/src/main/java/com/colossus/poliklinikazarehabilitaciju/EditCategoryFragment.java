package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class EditCategoryFragment extends Fragment {
    private RelativeLayout addCategoryContainer, loadingContainer;
    private RecyclerView mMainList;
    private FirebaseFirestore mFirestore;
    private CategoryListAdapter categoryListAdapter;
    private List<Category> categoryList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_category_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        addCategoryContainer = getView().findViewById(R.id.addCategoryContainer);
        loadingContainer = getView().findViewById(R.id.loadingContainer);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Activity mainActivity = getActivity();

        mFirestore = FirebaseFirestore.getInstance();

        categoryList = new ArrayList<>();
        categoryListAdapter = new CategoryListAdapter(categoryList);


        mMainList =(RecyclerView) getView().findViewById(R.id.category_list);
        mMainList.setHasFixedSize(true);
        mMainList.setLayoutManager(new LinearLayoutManager(mainActivity));
        mMainList.setNestedScrollingEnabled(false);
        mMainList.setAdapter(categoryListAdapter);

        mFirestore.collection("kategorija").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for(DocumentChange docChange : queryDocumentSnapshots.getDocumentChanges()){
                    QueryDocumentSnapshot qds = docChange.getDocument();

                    String uid = qds.getId();

                    System.out.println(uid + " " + qds.getData());

                    Category category = qds.toObject(Category.class).withUid(uid);
                    System.out.println(category.toString());
                    categoryList.add(category);
                    categoryListAdapter.notifyDataSetChanged();
                }
                loadingContainer.setVisibility(View.GONE);
            }
        });

        addCategoryContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToInsertCategory = new Intent(mainActivity, InsertCategoryActivity.class);
                startActivity(intToInsertCategory);
            }
        });
    }
}
