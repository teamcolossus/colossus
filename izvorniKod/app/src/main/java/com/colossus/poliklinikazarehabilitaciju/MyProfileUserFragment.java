package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class MyProfileUserFragment extends Fragment {
    FirebaseFirestore mFirestore;
    FirebaseAuth mFirebaseAuth;
    FirebaseStorage mStorage;
    private Button editProfile;
    private TextView tvName, tvMail, doctorName, trainerName;
    private ImageView profileImg, emailImage;
    private Activity mainActivity;
    private RelativeLayout loadingContainer, docContainer, trainContainer;
    private User currentUser;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_profile_user, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        tvName = getView().findViewById(R.id.profileName);
        tvMail = getView().findViewById(R.id.emailProfileLabel);
        profileImg = getView().findViewById(R.id.profilePicture);
        emailImage = getView().findViewById(R.id.emailLogo);
        loadingContainer = getView().findViewById(R.id.loadingContainer);
        editProfile = getView().findViewById(R.id.btnEditProfile);
        doctorName = getView().findViewById(R.id.doctorNameProfileUser);
        trainerName = getView().findViewById(R.id.trainerNameProfileUser);
        docContainer = getView().findViewById(R.id.doctorConatiner);
        trainContainer = getView().findViewById(R.id.trainerConatiner);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = getActivity();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
        mStorage = FirebaseStorage.getInstance();

        initializeProfile();

        mFirestore.collection("klijent").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d("MAIN", "Error : " + e.getMessage());
                    return;
                }
                for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                    String uid = doc.getDocument().getId();

                    if (doc.getType() == DocumentChange.Type.MODIFIED) {
                        User user = doc.getDocument().toObject(User.class).withUid(uid);

                        if(user.getUid().equals(uid)){
                            loadingContainer.setVisibility(View.VISIBLE);
                            initializeProfile();
                        }
                    }
                }
            }
        });
    }


    private void initializeProfile() {
        mFirestore.collection("klijent")
                .document(mFirebaseAuth.getCurrentUser().getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            String uid = mFirebaseAuth.getCurrentUser().getUid();
                            currentUser = documentSnapshot.toObject(User.class).withUid(uid);
                            mainActivity.setTitle(currentUser.getKorisnicko_ime());
                            tvName.setText(currentUser.getFullName());
                            tvMail.setText(currentUser.getEmail());

                            if (currentUser.getDoktor() != null) {
                                mFirestore.collection("zaposlenik").document(currentUser.getDoktor()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        String ime = documentSnapshot.getString("ime");
                                        String prezime = documentSnapshot.getString("prezime");
                                        doctorName.setText(ime + " " + prezime);

                                        docContainer.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent intToProfile = new Intent(mainActivity, ProfileOfUsersEmployeeActivity.class);
                                                intToProfile.putExtra("EMPLOYEE_UID", currentUser.getDoktor());
                                                startActivity(intToProfile);
                                            }
                                        });
                                    }
                                });
                            } else {
                                docContainer.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intToChooseDoctor= new Intent(mainActivity, ChooseDoctor.class);
                                        startActivity(intToChooseDoctor);
                                    }
                                });
                                doctorName.setText("Nije odabran");
                            }

                            if (currentUser.getTrener() != null) {
                                mFirestore.collection("zaposlenik").document(currentUser.getTrener()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        String ime = documentSnapshot.getString("ime");
                                        String prezime = documentSnapshot.getString("prezime");
                                        trainerName.setText(ime + " " + prezime);

                                        trainContainer.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent intToProfile = new Intent(mainActivity, ProfileOfUsersEmployeeActivity.class);
                                                intToProfile.putExtra("EMPLOYEE_UID", currentUser.getTrener());
                                                startActivity(intToProfile);
                                            }
                                        });
                                    }
                                });
                            } else {
                                trainerName.setText("Nije odabran");
                                trainContainer.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intToChooseTrainer = new Intent(mainActivity, ChooseTrainer.class);
                                        startActivity(intToChooseTrainer);
                                    }
                                });
                            }

                            editProfile.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intToEdit = new Intent(mainActivity, EditProfileUser.class);
                                    intToEdit.putExtra("USER", currentUser);
                                    startActivity(intToEdit);
                                }
                            });

                            emailImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                                    emailIntent.setType("plain/text");
                                    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{currentUser.getEmail()});
                                    mainActivity.startActivity(Intent.createChooser(emailIntent, "Pošalji mail koristeći:"));
                                }
                            });

                            final String filepath = "profilePictures/" + uid + ".jpg";
                            mStorage.getReference().child(filepath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    StorageReference mImageRef = mStorage.getReferenceFromUrl(uri.toString());
                                    GlideApp.with(mainActivity)
                                            .load(mImageRef)
                                            .into(profileImg);
                                    loadingContainer.setVisibility(View.GONE);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    StorageReference mImageRef = mStorage.getReference("profilePictures/default.jpg");
                                    GlideApp.with(mainActivity)
                                            .load(mImageRef)
                                            .into(profileImg);
                                    loadingContainer.setVisibility(View.GONE);
                                }
                            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                }
                            });
                        }
                    }
                });
    }


}
