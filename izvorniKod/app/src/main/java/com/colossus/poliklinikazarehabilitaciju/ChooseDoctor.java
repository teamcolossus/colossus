package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ChooseDoctor extends AppCompatActivity {

    private static final String TAG = "FireLog";
    private RecyclerView mMainList;
    private FirebaseFirestore mFirestore;
    private ChooseDoctorAdapter chooseDoctorAdapter;
    private List<Employee> employeeList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_doctor);

        // toolbar
        setTitle("Odaberi doktora");
        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        employeeList = new ArrayList<>();
        chooseDoctorAdapter = new ChooseDoctorAdapter(employeeList);

        mMainList = (RecyclerView) findViewById(R.id.doctorList);
        mMainList.setHasFixedSize(true);
        mMainList.setLayoutManager(new LinearLayoutManager(this));
        mMainList.setAdapter(chooseDoctorAdapter);


        mFirestore = FirebaseFirestore.getInstance();
        mFirestore.collection("zaposlenik").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d(TAG, "Error : " + e.getMessage());
                    return;
                }

                for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                    if (doc.getType() == DocumentChange.Type.ADDED) {
                        String uid = doc.getDocument().getId();

                        Employee employee = doc.getDocument().toObject(Employee.class).withUid(uid);

                        if(!(employee.getMax_br_korisnika() - employee.getTrenutno_korisnika() > 0 && employee.getId_ovlasti().equals("2"))){
                            continue;
                        }
                        employeeList.add(employee);
                        chooseDoctorAdapter.notifyDataSetChanged();

                    }
                    if (doc.getType() == DocumentChange.Type.MODIFIED) {
                        chooseDoctorAdapter.notifyDataSetChanged();

                        String uid = doc.getDocument().getId();
                        Employee employee = doc.getDocument().toObject(Employee.class).withUid(uid);
                        if(!(employee.getMax_br_korisnika() - employee.getTrenutno_korisnika() > 0 && employee.getId_ovlasti().equals("2"))){
                            continue;
                        }
                        employeeList.add(employee);
                        chooseDoctorAdapter.notifyDataSetChanged();
                    }
                }
            }
        });


        AlertDialog alertDialog = new AlertDialog.Builder(ChooseDoctor.this).create();
        alertDialog.setTitle("Potrebno je odabrati doktora");
        alertDialog.setMessage("Pogledaj profile doktora i odaberi kojega želiš pomoću padajućeg izbornika");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
