package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class NewClientsActivity extends AppCompatActivity {
    private static final String TAG = "FireLog";
    private RecyclerView mMainList;
    private FirebaseFirestore mFirestore;
    private FirebaseAuth mFirebaseAuth;
    private NewClientsAdapter chooseDoctorAdapter;
    private List<UserSparse> userList, newList;
    private String ROLE;
    public final static int REQ_CODE_EDIT = 1101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_doctor);
        ROLE = (String) getIntent().getSerializableExtra("ROLE");

        mFirestore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();

        // toolbar
        setTitle("Novi klijenti");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (ROLE.equals("2")) {
            initializeClientsDoctor();
        } else if (ROLE.equals("3")) {
            initializeClientsTrainer();
        }else{
            finish();
        }

    }

    private void initializeClientsTrainer() {
        newList = new ArrayList<>();
        final NewClientsAdapter newClientsAdapter = new NewClientsAdapter(newList, ROLE);

        mMainList = (RecyclerView) findViewById(R.id.doctorList);
        mMainList.setHasFixedSize(true);
        mMainList.setLayoutManager(new LinearLayoutManager(this));
        mMainList.setNestedScrollingEnabled(false);
        mMainList.setAdapter(newClientsAdapter);

        mFirestore.collection("trener")
                .document(mFirebaseAuth.getCurrentUser().getUid())
                .collection("klijenti")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d(TAG, "Error : " + e.getMessage());
                            return;
                        }
                        userList = new ArrayList<>();

                        for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                            if (doc.getType() == DocumentChange.Type.ADDED) {
                                String uid = doc.getDocument().getId();
                                UserSparse user = doc.getDocument().toObject(UserSparse.class).withUid(uid);
                                userList.add(user);
                            }
                        }
                        for (final UserSparse user : userList) {
                            mFirestore.collection("trening").document(user.getUid()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    if (!documentSnapshot.exists()) {
                                        newList.add(user);
                                        newClientsAdapter.notifyDataSetChanged();
                                    }
                                }
                            });
                        }
                    }
                });
    }

    private void initializeClientsDoctor() {
        newList = new ArrayList<>();
        final NewClientsAdapter newClientsAdapter = new NewClientsAdapter(newList, ROLE);

        mMainList = (RecyclerView) findViewById(R.id.doctorList);
        mMainList.setHasFixedSize(true);
        mMainList.setLayoutManager(new LinearLayoutManager(this));
        mMainList.setNestedScrollingEnabled(false);
        mMainList.setAdapter(newClientsAdapter);

        mFirestore.collection("doktor")
                .document(mFirebaseAuth.getCurrentUser().getUid())
                .collection("klijenti")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d(TAG, "Error : " + e.getMessage());
                            return;
                        }
                        userList = new ArrayList<>();

                        for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                            if (doc.getType() == DocumentChange.Type.ADDED) {
                                String uid = doc.getDocument().getId();
                                UserSparse user = doc.getDocument().toObject(UserSparse.class).withUid(uid);
                                userList.add(user);
                            }
                        }
                        for (final UserSparse user : userList) {
                            mFirestore.collection("dijeta").document(user.getUid()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    if (!documentSnapshot.exists()) {
                                        newList.add(user);
                                        newClientsAdapter.notifyDataSetChanged();
                                    }
                                }
                            });
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE_EDIT) {
            if (resultCode == RESULT_OK) {
                finish();
                startActivity(getIntent());
            }
        }
    }
}
