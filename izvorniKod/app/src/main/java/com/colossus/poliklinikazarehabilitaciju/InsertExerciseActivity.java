package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.HashMap;
import java.util.Map;


public class InsertExerciseActivity extends AppCompatActivity {

    EditText imeVjezbe, laganoValue, srednjeValue, intenzivnoValue, opisVjezbe;
    FirebaseFirestore mFirestore;
    private int any_integer_variable;

    FirebaseAuth mFirebaseAuth;
    FirebaseFirestore mFireStore;
    private FirebaseStorage mStorage;

    String odabranaVjezba = null; // ako treba urediti proizvod pa se salje id
    QueryDocumentSnapshot dokument;
    Bundle extra_params;
    private String EX_ID;
    private ImageView exPic;
    private RelativeLayout loadingContainer;
    boolean picChanged = false;
    private Exercise exercise;
    private Uri uriImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_exercise);

        EX_ID = getIntent().getStringExtra("EX_ID");

        // toolbar
        setTitle("Uredi vježbu");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mFireStore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mStorage = FirebaseStorage.getInstance();

        mFirestore = FirebaseFirestore.getInstance();
        imeVjezbe = findViewById(R.id.imeVjezbe);
        laganoValue = findViewById(R.id.laganoValue);
        srednjeValue = findViewById(R.id.srednjeValue);
        intenzivnoValue = findViewById(R.id.intenzivnoValue);
        opisVjezbe = findViewById(R.id.opisVjezbe);
        exPic = findViewById(R.id.exPic);
        loadingContainer = findViewById(R.id.loadingContainer);

        extra_params = getIntent().getExtras();
        String nazivVjezbe = null;

        try {
            nazivVjezbe = String.valueOf(extra_params.get("naziv_vjezbe"));
            System.out.println("Naziv vjezbe je " + nazivVjezbe);
        } catch (Exception e) {
            e.printStackTrace();
        }

        exPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                ImagePicker.Companion.with(InsertExerciseActivity.this)
                        .crop(1f,1f)
                        .compress(1024)
                        .maxResultSize(1080, 1080)
                        .start();
            }
        });


        mFireStore.collection("vjezbe")
                .document(EX_ID)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            exercise = documentSnapshot.toObject(Exercise.class);

                            imeVjezbe.setText(exercise.getNaziv());
                            opisVjezbe.setText(exercise.getOpis());
                            laganoValue.setText(Integer.toString(exercise.getLagano()));
                            srednjeValue.setText(Integer.toString(exercise.getSrednje()));
                            intenzivnoValue.setText(Integer.toString(exercise.getTesko()));

                            final String filepath = "vjezbePictures/" + EX_ID + ".jpg";
                            mStorage.getReference().child(filepath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    StorageReference mImageRef = mStorage.getReferenceFromUrl(uri.toString());
                                    GlideApp.with(InsertExerciseActivity.this)
                                            .load(mImageRef)
                                            .into(exPic);
                                    loadingContainer.setVisibility(View.GONE);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    StorageReference mImageRef = mStorage.getReference("vjezbePictures/default.jpg");
                                    GlideApp.with(InsertExerciseActivity.this)
                                            .load(mImageRef)
                                            .into(exPic);
                                    loadingContainer.setVisibility(View.GONE);
                                }
                            });
                        }
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            case R.id.action_menu_done:
                if (imeVjezbe.getText().toString().equals("")) {
                    Toast.makeText(InsertExerciseActivity.this, "Napisite ime vjezbe", Toast.LENGTH_SHORT).show();
                    break;
                }

                if (opisVjezbe.getText().toString().equals("")) {
                    Toast.makeText(InsertExerciseActivity.this, "Napisite opis vjezbe", Toast.LENGTH_SHORT).show();
                    break;
                }

                try {
                    Integer.parseInt(laganoValue.getText().toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(InsertExerciseActivity.this, "Neispravan unos kod Lagano", Toast.LENGTH_SHORT).show();
                    break;
                }

                try {
                    Integer.parseInt(srednjeValue.getText().toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(InsertExerciseActivity.this, "Neispravan unos kod Srednje", Toast.LENGTH_SHORT).show();
                    break;
                }

                try {
                    Integer.parseInt(intenzivnoValue.getText().toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(InsertExerciseActivity.this, "Neispravan unos kod Intenzivno", Toast.LENGTH_SHORT).show();
                    break;
                }

                Map<String, Object> data = new HashMap<>();
                data.put("naziv", imeVjezbe.getText().toString());
                data.put("opis", opisVjezbe.getText().toString());
                data.put("lagano", Integer.parseInt(laganoValue.getText().toString()));
                data.put("srednje", Integer.parseInt(srednjeValue.getText().toString()));
                data.put("tesko", Integer.parseInt(intenzivnoValue.getText().toString()));

                mFirestore.collection("vjezbe").document(EX_ID).update(data);

                if (picChanged){
                    StorageReference vjezbeImgRef = mStorage.getReference().child("vjezbePictures/"
                            + EX_ID + ".jpg");
                    vjezbeImgRef.putFile(uriImg);
                }

                finish();
                break;

        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImagePicker.REQUEST_CODE) {
            if (resultCode == ImgForWorkoutActivity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                if (data == null) {
                    Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
                    return;
                }
                uriImg = data.getData();
                exPic.setImageURI(uriImg);
                picChanged = true;

                //You can get File object from intent
                File file = ImagePicker.Companion.getFile(data);

                //You can also get File Path from intent
                String filePath = ImagePicker.Companion.getFilePath(data);
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.Companion.getError(data), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Ne bi trebao dobiti ovu gresku", Toast.LENGTH_SHORT).show();

        }
    }
}
