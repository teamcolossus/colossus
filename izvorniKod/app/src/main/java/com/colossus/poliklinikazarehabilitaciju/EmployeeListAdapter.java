package com.colossus.poliklinikazarehabilitaciju;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmployeeListAdapter extends RecyclerView.Adapter<EmployeeListAdapter.ViewHolder> {
    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    public List<Employee> employeeList;
    private Context context;
    private FirebaseStorage mStorage = FirebaseStorage.getInstance();

    public EmployeeListAdapter(List<Employee> employeeList){
        this.employeeList = employeeList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_employee,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }
    
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.nameText.setText(employeeList.get(position).getFullName());

        final Employee currentEmployee = employeeList.get(position);
        final String uid = currentEmployee.getUid();

        final String filepath = "profilePictures/" + uid + ".jpg";
        mStorage.getReference().child(filepath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                StorageReference mImageRef = mStorage.getReferenceFromUrl(uri.toString());
                GlideApp.with(context)
                        .load(mImageRef)
                        .into(holder.profileImg);
                holder.loadingContainer.setVisibility(View.GONE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                StorageReference mImageRef = mStorage.getReference("profilePictures/default.jpg");
                GlideApp.with(context)
                        .load(mImageRef)
                        .into(holder.profileImg);
                holder.loadingContainer.setVisibility(View.GONE);
            }
        });


        holder.wrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToProfile = new Intent(context, ProfileEmployeeActivity.class);
                intToProfile.putExtra("EMPLOYEE_UID", uid);
                context.startActivity(intToProfile);
            }
        });

        holder.buttonViewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(view.getContext(), holder.buttonViewOption);
                //inflating menu from xml resource
                popup.inflate(R.menu.confirm_employee_popup);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.popConfirm:
                                Map<String,Object> confirmMap = new HashMap<>();
                                confirmMap.put("potvrden",true);
                                mFirebaseFirestore.collection("reroute").document(uid).update(confirmMap);
                                mFirebaseFirestore.collection("zaposlenik").document(uid).update(confirmMap);
                                employeeList.remove(currentEmployee);
                                return true;
                            case R.id.popDelete:
                                Map<String,Object> deleteMap = new HashMap<>();
                                deleteMap.put("delete",true);
                                mFirebaseFirestore.collection("reroute").document(uid).update(deleteMap);
                                return true;

                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return employeeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View mView;
        public TextView nameText;
        public ImageView buttonViewOption;
        public RelativeLayout wrapper;
        public ImageView profileImg;
        public ProgressBar loadingContainer;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            profileImg = mView.findViewById(R.id.profilePicture);
            loadingContainer = (ProgressBar) mView.findViewById(R.id.progressBar);
            wrapper = mView.findViewById(R.id.adapter_wrapper);
            nameText = (TextView) mView.findViewById(R.id.tvEmployeeName);
            buttonViewOption = (ImageView) mView.findViewById(R.id.btnViewOption);
        }


    }
}
