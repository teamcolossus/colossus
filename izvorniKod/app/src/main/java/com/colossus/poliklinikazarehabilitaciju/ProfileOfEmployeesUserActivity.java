package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;

public class ProfileOfEmployeesUserActivity extends AppCompatActivity {
    FirebaseFirestore mFirestore;
    FirebaseAuth mFirebaseAuth;
    FirebaseStorage mStorage;
    private TextView tvName, tvMail, doctorName, trainerName;
    private ImageView profileImg, emailImage;
    private Button endRelation;
    private RelativeLayout loadingContainer, docContainer, trainContainer, statsContainer, dietContainer, trainingContainer;
    private User watchingUser;
    private String userUid, roleValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile_of_employees_user);
        userUid = (String) getIntent().getSerializableExtra("USER_UID");
        roleValue = (String) getIntent().getSerializableExtra("ROLE_ID");

        // toolbar
        setTitle("");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
        mStorage = FirebaseStorage.getInstance();

        endRelation = findViewById(R.id.btnEndRelation);
        tvName = findViewById(R.id.profileName);
        tvMail = findViewById(R.id.emailProfileLabel);
        profileImg = findViewById(R.id.profilePicture);
        emailImage = findViewById(R.id.emailLogo);
        loadingContainer = findViewById(R.id.loadingContainer);
        doctorName = findViewById(R.id.doctorNameProfileUser);
        trainerName = findViewById(R.id.trainerNameProfileUser);
        docContainer = findViewById(R.id.doctorConatiner);
        trainContainer = findViewById(R.id.trainerConatiner);
        statsContainer = findViewById(R.id.statsContainer);
        dietContainer = findViewById(R.id.dietContainer);
        trainingContainer = findViewById(R.id.trainingContainer);

        initializeProfile();

        if (roleValue.equals("2")) {
            dietContainer.setVisibility(View.VISIBLE);
            statsContainer.setVisibility(View.VISIBLE);
        } else if (roleValue.equals("3")) {
            trainingContainer.setVisibility(View.VISIBLE);
            statsContainer.setVisibility(View.VISIBLE);
        } else {
            endRelation.setVisibility(View.GONE);
        }

        dietContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToDiet = new Intent(ProfileOfEmployeesUserActivity.this, MyDietActivity.class);
                intToDiet.putExtra("USER_UID", userUid);
                intToDiet.putExtra("ROLE_ID", roleValue);
                startActivity(intToDiet);
            }
        });
        trainingContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileOfEmployeesUserActivity.this, TrainingExercisesListActivity.class);
                intent.putExtra("userUid", userUid);
                startActivity(intent);
            }
        });
        statsContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileOfEmployeesUserActivity.this, StatsActivity.class);
                intent.putExtra("USER_UID", userUid);
                startActivity(intent);
            }
        });

        endRelation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(ProfileOfEmployeesUserActivity.this).create();
                alertDialog.setTitle("Potvrdi prekid suradnje s:");
                alertDialog.setMessage(watchingUser.getFullName());
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Map<String, Object> assignMap = new HashMap<>();
                                if (roleValue.equals("3")) {
                                    assignMap.put("trener", null);
                                    mFirestore.collection("trener").document(mFirebaseAuth.getCurrentUser().getUid()).collection("klijenti").document(userUid).delete();
                                } else {
                                    assignMap.put("doktor", null);
                                    mFirestore.collection("doktor").document(mFirebaseAuth.getCurrentUser().getUid()).collection("klijenti").document(userUid).delete();
                                }

                                mFirestore.collection("klijent").document(userUid).update(assignMap);
                                mFirestore.collection("zaposlenik").document(mFirebaseAuth.getCurrentUser().getUid()).update("trenutno_korisnika", FieldValue.increment(-1));
                                dialog.dismiss();
                                finish();
                            }
                        });
                alertDialog.show();
            }
        });
    }

    private void initializeProfile() {
        mFirestore.collection("klijent")
                .document(userUid)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            watchingUser = documentSnapshot.toObject(User.class).withUid(userUid);
                            setTitle(watchingUser.getKorisnicko_ime());
                            tvName.setText(watchingUser.getFullName());
                            tvMail.setText(watchingUser.getEmail());

                            if (watchingUser.getDoktor() != null) {
                                mFirestore.collection("zaposlenik").document(watchingUser.getDoktor()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        String ime = documentSnapshot.getString("ime");
                                        String prezime = documentSnapshot.getString("prezime");
                                        doctorName.setText(ime + " " + prezime);

                                        docContainer.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent intToProfile = new Intent(getApplicationContext(), ProfileEmployeeActivity.class);
                                                intToProfile.putExtra("EMPLOYEE_UID", watchingUser.getDoktor());
                                                startActivity(intToProfile);
                                            }
                                        });
                                    }
                                });
                            } else {
                                docContainer.setClickable(false);
                                doctorName.setText("Nije odabran");
                            }

                            if (watchingUser.getTrener() != null) {
                                mFirestore.collection("zaposlenik").document(watchingUser.getTrener()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        String ime = documentSnapshot.getString("ime");
                                        String prezime = documentSnapshot.getString("prezime");
                                        trainerName.setText(ime + " " + prezime);

                                        trainContainer.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent intToProfile = new Intent(getApplicationContext(), ProfileEmployeeActivity.class);
                                                intToProfile.putExtra("EMPLOYEE_UID", watchingUser.getTrener());
                                                startActivity(intToProfile);
                                            }
                                        });
                                    }
                                });
                            } else {
                                trainerName.setText("Nije odabran");
                                trainContainer.setClickable(false);
                            }

                            emailImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                                    emailIntent.setType("plain/text");
                                    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{watchingUser.getEmail()});
                                    startActivity(Intent.createChooser(emailIntent, "Pošalji mail koristeći:"));
                                }
                            });

                            final String filepath = "profilePictures/" + userUid + ".jpg";
                            mStorage.getReference().child(filepath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    StorageReference mImageRef = mStorage.getReferenceFromUrl(uri.toString());
                                    GlideApp.with(ProfileOfEmployeesUserActivity.this)
                                            .load(mImageRef)
                                            .into(profileImg);
                                    loadingContainer.setVisibility(View.GONE);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    StorageReference mImageRef = mStorage.getReference("profilePictures/default.jpg");
                                    GlideApp.with(ProfileOfEmployeesUserActivity.this)
                                            .load(mImageRef)
                                            .into(profileImg);
                                    loadingContainer.setVisibility(View.GONE);
                                }
                            });
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
