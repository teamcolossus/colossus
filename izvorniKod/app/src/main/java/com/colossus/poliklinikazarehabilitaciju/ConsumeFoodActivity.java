package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ConsumeFoodActivity extends AppCompatActivity {

    private static final String TAG = "FireLog";
    private RecyclerView mMainList;
    private FirebaseFirestore mFirestore;
    private FirebaseAuth mFirebaseAuth;
    private FoodConsumeAdapter foodAdapter;
    private List<Product> foodList;
    private RelativeLayout loadingContainer;
    public final static int REQ = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consume_food);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        // toolbar
        setTitle("Odaberi hranu");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        loadingContainer = findViewById(R.id.loadingContainer);

        foodList = new ArrayList<>();
        foodAdapter = new FoodConsumeAdapter(foodList);

        mMainList = (RecyclerView) findViewById(R.id.rvFood);
        mMainList.setHasFixedSize(true);
        mMainList.setLayoutManager(new LinearLayoutManager(this));
        mMainList.setAdapter(foodAdapter);

        mFirestore = FirebaseFirestore.getInstance();
        mFirestore.collection("hrana").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d("DIJETA", "Error : " + e.getMessage());
                    return;
                }

                for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                    if (doc.getType() == DocumentChange.Type.ADDED) {
                        String uid = doc.getDocument().getId();

                        Product currentProd = doc.getDocument().toObject(Product.class).withUid(uid);
                        foodList.add(currentProd);
                        foodAdapter.notifyDataSetChanged();
                    }
                }
                loadingContainer.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ) {
            if (resultCode == RESULT_OK) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        }

        if (requestCode == BarcodeScannerActivity.REQ_SCANNER) {
            if (resultCode == RESULT_OK) {

                Intent intToDiet = new Intent(getApplicationContext(), EditConsume.class);
                intToDiet.putExtra("BARCODE", data.getStringExtra("BARCODE"));
                startActivityForResult(intToDiet, REQ);
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        if (item.getItemId() == R.id.action_menu_bar) {
            Intent intToBarcode = new Intent(getApplicationContext(), BarcodeScannerActivity.class);
            startActivityForResult(intToBarcode, BarcodeScannerActivity.REQ_SCANNER);
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bar, menu);
        return true;
    }
}
