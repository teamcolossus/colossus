package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditDiet extends AppCompatActivity implements NumberPicker.OnValueChangeListener {
    private RelativeLayout loadingContainer;
    private FirebaseFirestore mFirestore;
    private List<Category> categoryList, oldcategoryList;
    private CategoryAdapter categoryAdapter;
    private List<Product> foodList, oldfoodList;
    private FoodAdapter foodAdapter;
    FirebaseAuth mFirebaseAuth;
    String currentUid, roleValue;
    private RecyclerView mCategory, mFood;
    private RelativeLayout addFoodContainer, addForbiddenCategoryContainer;
    private Menu optionsMenu;
    public final static int REQ_CODE_CHOOSE = 1001;
    private boolean NEW;
    private EditText comment, cal, noSalt, noSaturated, noSugar, noFat, noProtein, noCarbs;
    private NumberPicker npFat, npProtein, npCarbs;
    private Diet diet;
    private ColorStateList oldColors;
    private int dupl = 0;
    private int del = 0;
    private TextView tvSum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_diet);

        currentUid = (String) getIntent().getSerializableExtra("USER_UID");
        roleValue = (String) getIntent().getSerializableExtra("ROLE_ID");
        NEW = (boolean) getIntent().getSerializableExtra("NEW");

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        //toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        addFoodContainer = findViewById(R.id.addFoodList);
        addForbiddenCategoryContainer = findViewById(R.id.addCategoryContainer);
        loadingContainer = findViewById(R.id.loadingContainer);
        comment = findViewById(R.id.dietComment);
        cal = findViewById(R.id.etCal);
        noSalt = findViewById(R.id.etSalt);
        noSaturated = findViewById(R.id.etSaturated);
        noSugar = findViewById(R.id.etSugar);
        noFat = findViewById(R.id.etFat);
        noProtein = findViewById(R.id.etProtein);
        noCarbs = findViewById(R.id.etCarbs);
        tvSum = findViewById(R.id.sum);

        TextView tvSample = findViewById(R.id.tvSample);
        oldColors = tvSample.getTextColors();

        NumberPicker.Formatter formatter = new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                int temp = value * 5;
                return "" + temp;
            }
        };


        npCarbs = (NumberPicker) findViewById(R.id.npCarbs);
        npProtein = (NumberPicker) findViewById(R.id.npProtein);
        npFat = (NumberPicker) findViewById(R.id.npFat);

        npFat.setFormatter(formatter);
        npCarbs.setFormatter(formatter);
        npProtein.setFormatter(formatter);

        npCarbs.setMinValue(0);
        npCarbs.setMaxValue(20);
        npProtein.setMinValue(0);
        npProtein.setMaxValue(20);
        npFat.setMinValue(0);
        npFat.setMaxValue(20);

        npFat.setOnValueChangedListener(this);
        npCarbs.setOnValueChangedListener(this);
        npProtein.setOnValueChangedListener(this);

        addForbiddenCategoryContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditDiet.this, ForbiddenDietActivity.class);
                intent.putExtra("FOOD", Boolean.FALSE);
                intent.putExtra("CATEGORY", Boolean.TRUE);
                intent.putExtra("PREV_CAT", (Serializable) categoryList);
                startActivityForResult(intent, REQ_CODE_CHOOSE);
            }
        });

        addFoodContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditDiet.this, ForbiddenDietActivity.class);
                intent.putExtra("FOOD", Boolean.TRUE);
                intent.putExtra("CATEGORY", Boolean.FALSE);
                intent.putExtra("PREV_FOOD", (Serializable) foodList);
                startActivityForResult(intent, REQ_CODE_CHOOSE);
            }
        });

        initializeEdit();
    }

    public int getCarbsValue() {
        return npCarbs.getValue() * 5;
    }

    public int getProtValue() {
        return npProtein.getValue() * 5;
    }

    public int getFatValue() {
        return npFat.getValue() * 5;
    }

    public void setCarbsValue(int value) {
        npCarbs.setValue(value / 5);
    }

    public void setProtValue(int value) {
        npProtein.setValue(value / 5);
    }

    public void setFatValue(int value) {
        npFat.setValue(value / 5);
    }

    private void initializeEdit() {
        loadingContainer.setVisibility(View.GONE);

        if (!NEW) {
            setTitle("Uredi dijetu");

            categoryList = (List<Category>) getIntent().getSerializableExtra("CAT_PREV");
            foodList = (List<Product>) getIntent().getSerializableExtra("FOOD_PREV");

            oldcategoryList = (List<Category>) getIntent().getSerializableExtra("CAT_PREV");
            oldfoodList = (List<Product>) getIntent().getSerializableExtra("FOOD_PREV");

            diet = (Diet) getIntent().getSerializableExtra("DIET");

            setFatValue(Integer.parseInt(diet.getMasti()));
            setProtValue(Integer.parseInt(diet.getProteini()));
            setCarbsValue(Integer.parseInt(diet.getUgljikohidrati()));
            comment.setText(diet.getOpis() == null ? "" : diet.getOpis());
            cal.setText(diet.getKalorija() == null ? "" : diet.getKalorija());
            noSalt.setText(diet.getNoSol() == null ? "" : diet.getNoSol());
            noSaturated.setText(diet.getNoZasicene() == null ? "" : diet.getNoZasicene());
            noSugar.setText(diet.getNoSecer() == null ? "" : diet.getNoSecer());
            noFat.setText(diet.getNoMasti() == null ? "" : diet.getNoMasti());
            noProtein.setText(diet.getNoProteini() == null ? "" : diet.getNoProteini());
            noCarbs.setText(diet.getNoUgljikohidrati() == null ? "" : diet.getNoUgljikohidrati());

            categoryAdapter = new CategoryAdapter(categoryList);
            mCategory = (RecyclerView) findViewById(R.id.categoryList);
            mCategory.setHasFixedSize(true);
            mCategory.setLayoutManager(new LinearLayoutManager(this));
            mCategory.setNestedScrollingEnabled(false);
            mCategory.setAdapter(categoryAdapter);
            categoryAdapter.notifyDataSetChanged();

            foodAdapter = new FoodAdapter(foodList);
            mFood = (RecyclerView) findViewById(R.id.foodList);
            mFood.setHasFixedSize(true);
            mFood.setLayoutManager(new LinearLayoutManager(this));
            mFood.setNestedScrollingEnabled(false);
            mFood.setAdapter(foodAdapter);
            foodAdapter.notifyDataSetChanged();

            checkSum();
        } else {
            setTitle("Postavi dijetu");
        }
    }

    public void forceCheckSum(){
        checkSum();
    }

    private void approve() {
        AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();
        alertDialog.setTitle("Potvrdi dijetu: ");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        uploadDiet();
                    }
                });
        alertDialog.show();
    }

    private void uploadDiet() {
        loadingContainer.setVisibility(View.VISIBLE);
        if (!NEW) {
            //obrisati i kolekcije

            mFirestore.collection("dijeta").document(currentUid).delete();

            Map<String, Object> dietMap = new HashMap<>();
            dietMap.put("opis", comment.getText().toString());
            if (cal.getText().toString().isEmpty()) {
                npCarbs = (NumberPicker) findViewById(R.id.npCarbs);
                npCarbs.setValue(0);
                npProtein = (NumberPicker) findViewById(R.id.npProtein);
                npProtein.setValue(0);
                npFat = (NumberPicker) findViewById(R.id.npFat);
                npFat.setValue(0);
            }
            dietMap.put("masti", Integer.toString(getFatValue()));
            dietMap.put("proteini", Integer.toString(getProtValue()));
            dietMap.put("ugljikohidrati", Integer.toString(getCarbsValue()));
            dietMap.put("kalorija", cal.getText().toString());
            dietMap.put("noSol", noSalt.getText().toString());
            dietMap.put("noZasicene", noSaturated.getText().toString());
            dietMap.put("noSecer", noSugar.getText().toString());
            dietMap.put("noMasti", noFat.getText().toString());
            dietMap.put("noProteini", noProtein.getText().toString());
            dietMap.put("noUgljikohidrati", noCarbs.getText().toString());


            mFirestore.collection("dijeta").document(currentUid).set(dietMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    for (Product product : foodList) {
                        dupl = 0;
                        for (Product p : oldfoodList) {
                            if (product.getBarkod().equals(p.getBarkod())) {
                                dupl = 1;
                                break;

                            }
                        }

                        if (dupl == 0) {
                            Map<String, Object> productMap = new HashMap<>();
                            productMap.put("naziv", product.getNaziv());
                            productMap.put("barkod", product.getBarkod());
                            productMap.put("energija", product.getEnergija());
                            productMap.put("zas_masne_kiseline", product.getZas_masne_kiseline());
                            productMap.put("ugljikohidrati", product.getUgljikohidrati());
                            productMap.put("bjelancevine", product.getBjelancevine());
                            productMap.put("masnoca", product.getMasnoca());
                            productMap.put("seceri", product.getSeceri());
                            productMap.put("sol", product.getSol());
                            productMap.put("is_deleted", product.getIs_deleted());
                            productMap.put("kategorija", product.getKategorija());

                            mFirestore.collection("dijeta").document(currentUid).collection("hrana").document(product.getBarkod()).set(productMap);
                        }
                    }

                    for (Product product : oldfoodList) {
                        del = 0;
                        for (Product p : foodList) {
                            if (product.getBarkod().equals(p.getBarkod())) {
                                del = 1;
                                break;
                            }
                        }
                        if (del == 0) {
                            mFirestore.collection("dijeta").document(currentUid).collection("hrana").document(product.getBarkod()).delete();
                        }
                    }
                    /*for (Category category : categoryList) {
                        Map<String, Object> categoryMap = new HashMap<>();
                        categoryMap.put("naziv", category.getNaziv());
                        categoryMap.put("is_deleted", category.getIs_deleted());
                        categoryMap.put("id",category.getId());
                        mFirestore.collection("dijeta").document(currentUid).collection("kategorija").document(category.getId()).set(categoryMap);
                    }
*/
                    for (Category category : categoryList) {
                        dupl = 0;
                        for (Category c : oldcategoryList) {
                            if (category.getId().equals(c.getId())) {
                                dupl = 1;
                                break;
                            }
                        }
                        if (dupl == 0) {
                            Map<String, Object> categoryMap = new HashMap<>();
                            categoryMap.put("id", category.getId());
                            categoryMap.put("naziv", category.getNaziv());
                            categoryMap.put("is_deleted", category.getIs_deleted());
                            mFirestore.collection("dijeta").document(currentUid).collection("kategorija").document(category.getId()).set(categoryMap);

                        }
                    }
                    for (Category category : oldcategoryList) {
                        del = 0;
                        for (Category c : categoryList) {
                            if (category.getId().equals(c.getId())) {
                                del = 1;
                                break;
                            }
                        }
                        if (del == 0) {
                            mFirestore.collection("dijeta").document(currentUid).collection("kategorija").document(category.getId()).delete();
                        }
                    }
                    setResult(RESULT_OK);
                    finish();

                }
            });


        } else {

            Map<String, Object> dietMap = new HashMap<>();
            dietMap.put("opis", comment.getText().toString());

            if (cal.getText().toString().isEmpty()) {
                npCarbs = (NumberPicker) findViewById(R.id.npCarbs);
                npCarbs.setValue(0);
                npProtein = (NumberPicker) findViewById(R.id.npProtein);
                npProtein.setValue(0);
                npFat = (NumberPicker) findViewById(R.id.npFat);
                npFat.setValue(0);
            }
            dietMap.put("masti", Integer.toString(getFatValue()));
            dietMap.put("proteini", Integer.toString(getProtValue()));
            dietMap.put("ugljikohidrati", Integer.toString(getCarbsValue()));
            dietMap.put("kalorija", cal.getText().toString());
            dietMap.put("noSol", noSalt.getText().toString());
            dietMap.put("noZasicene", noSaturated.getText().toString());
            dietMap.put("noSecer", noSugar.getText().toString());
            dietMap.put("noMasti", noFat.getText().toString());
            dietMap.put("noProteini", noProtein.getText().toString());
            dietMap.put("noUgljikohidrati", noCarbs.getText().toString());


            mFirestore.collection("dijeta").document(currentUid).set(dietMap);

            if (foodList != null) {
                for (Product product : foodList) {
                    Map<String, Object> productMap = new HashMap<>();
                    productMap.put("naziv", product.getNaziv());
                    productMap.put("barkod", product.getBarkod());
                    productMap.put("energija", product.getEnergija());
                    productMap.put("zas_masne_kiseline", product.getZas_masne_kiseline());
                    productMap.put("ugljikohidrati", product.getUgljikohidrati());
                    productMap.put("bjelancevine", product.getBjelancevine());
                    productMap.put("masnoca", product.getMasnoca());
                    productMap.put("seceri", product.getSeceri());
                    productMap.put("sol", product.getSol());
                    productMap.put("is_deleted", product.getIs_deleted());
                    productMap.put("kategorija", product.getKategorija());

                    mFirestore.collection("dijeta").document(currentUid).collection("hrana").document(product.getBarkod()).set(productMap);
                }
            }

            if (categoryList != null) {
                for (Category category : categoryList) {
                    Map<String, Object> categoryMap = new HashMap<>();
                    categoryMap.put("id", category.getId());
                    categoryMap.put("naziv", category.getNaziv());
                    categoryMap.put("is_deleted", category.getIs_deleted());

                    mFirestore.collection("dijeta").document(currentUid).collection("kategorija").document(category.getId()).set(categoryMap);
                }
            }
            setResult(RESULT_OK);
            finish();
        }
    }

    private void checkSum() {
        int sum = getCarbsValue() + getProtValue() + getFatValue();

        tvSum.setError(null);
//        Toast.makeText(getApplicationContext(), Integer.toString(sum), Toast.LENGTH_SHORT).show();
        tvSum.setText(Integer.toString(sum));

        if (sum == 100) {
            tvSum.setTextColor(Color.parseColor("#0ECE00"));
        } else {
            tvSum.setTextColor(Color.parseColor("#CE0000"));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        optionsMenu = menu;
        getMenuInflater().inflate(R.menu.menu_done, optionsMenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        if (item.getItemId() == R.id.action_menu_done) {
            if (validate()) {
                uploadDiet();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean validate() {
        boolean valid = true;

        int sum = getCarbsValue() + getProtValue() + getFatValue();
        String calories = cal.getText().toString();

        if (sum != 100) {
            tvSum.setError("Zbroj mora biti 100");
            valid = false;
        }
        if (calories.isEmpty()) {
            cal.setError("Unesi ukupan broj kalorija");
            cal.requestFocus();
            valid = false;
        }

        return valid;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE_CHOOSE) {
            if (resultCode == 1) {

                categoryList = (ArrayList<Category>) data.getSerializableExtra("categories");
                categoryAdapter = new CategoryAdapter(categoryList);

                mCategory = (RecyclerView) findViewById(R.id.categoryList);
                mCategory.setHasFixedSize(true);
                mCategory.setLayoutManager(new LinearLayoutManager(this));
                mCategory.setNestedScrollingEnabled(false);
                mCategory.setAdapter(categoryAdapter);

                categoryAdapter.notifyDataSetChanged();
            }

            if (resultCode == 2) {
                foodList = (ArrayList<Product>) data.getSerializableExtra("food");
                foodAdapter = new FoodAdapter(foodList);

                mFood = (RecyclerView) findViewById(R.id.foodList);
                mFood.setHasFixedSize(true);
                mFood.setLayoutManager(new LinearLayoutManager(this));
                mFood.setNestedScrollingEnabled(false);
                mFood.setAdapter(foodAdapter);

                foodAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        checkSum();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    Log.d("focus", "touchevent");
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}
