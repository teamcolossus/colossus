package com.colossus.poliklinikazarehabilitaciju;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;

import java.util.List;

public class HomeExAdapter extends RecyclerView.Adapter<HomeExAdapter.ViewHolder> {
    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    public List<Training> trainingList;
    private Context context;
    private FirebaseStorage mStorage = FirebaseStorage.getInstance();

    public HomeExAdapter(List<Training> trainingList){
        this.trainingList = trainingList;
    }



    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_home_ex,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        mFirebaseFirestore.collection("vjezbe")
                .document(trainingList.get(position).getId_vjezba())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            Exercise exercise = documentSnapshot.toObject(Exercise.class);
                            holder.nameText.setText(exercise.getNaziv());
                        }
                    }
                });
    }

    @Override
    public int getItemCount() {
        return trainingList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View mView;
        public TextView nameText;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            nameText = (TextView) mView.findViewById(R.id.tvName);
        }


    }
}
