package com.colossus.poliklinikazarehabilitaciju;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.HashMap;
import java.util.Map;

public class InsertCategoryActivity extends AppCompatActivity {
    TextView category;
    FirebaseAuth mFirebaseAuth;
    FirebaseFirestore mFirestore;

    String odabranaKategorija = null; // u slucaju da treba urediti kategoriju
    QueryDocumentSnapshot dokument;
    Bundle extra_params;
    String ID, EDIT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_category);

        mFirestore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        category = findViewById(R.id.tvNazivKategorije);

        // toolbar
        setTitle("Dodaj kategoriju");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        ID = getIntent().getStringExtra("ID");
        EDIT = getIntent().getStringExtra("EDIT");

        if (ID != null) {
            mFirestore.collection("kategorija")
                    .document(ID)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot.exists()) {
                                Category p = documentSnapshot.toObject(Category.class);
                                category.setText(p.getNaziv());
                            }
                        }
                    });
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_menu_done:
                final String kategorija = category.getText().toString();
                if (kategorija.isEmpty()) {
                    category.setError("Upiši naziv kategorije");
                    category.requestFocus();
                } else {
                    final Map<String, Object> category = new HashMap<>();
                    category.put("naziv", kategorija);
                    category.put(("is_deleted"), false);
                    if (EDIT != null) {
                        mFirestore.collection("kategorija")
                                .document(ID)
                                .update(category);
                    } else {
                        DocumentReference ref = mFirestore.collection("kategorija").document();
                        String myId = ref.getId();
                        category.put("id", myId);
                        mFirestore.collection("kategorija")
                                .document(myId)
                                .set(category);
                    }
                }
                finish();
        }
        return true;
    }
}
