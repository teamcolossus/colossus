package com.colossus.poliklinikazarehabilitaciju;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class TrainingListAdapter2 extends RecyclerView.Adapter<TrainingListAdapter2.TrainingViewHolder> {
    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    public List<Training> trainingList;

    public TrainingListAdapter2(List<Training> trainingList) {
        this.trainingList = trainingList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TrainingListAdapter2.TrainingViewHolder onCreateViewHolder(ViewGroup parent,
                                                                      int viewType) {
        // create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_training2, parent, false);
        TrainingListAdapter2.TrainingViewHolder vh = new TrainingListAdapter2.TrainingViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull final TrainingListAdapter2.TrainingViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Training curTraining = trainingList.get(position);

        String idVjezbe = curTraining.getId_vjezba();
        final String trajanje = curTraining.getTrajanje().toString();
        final String intenzitet = curTraining.getIntenzitet();
        String odradeno = curTraining.getOdradeno().toString();

        mFirebaseFirestore.collection("vjezbe")
                .document(idVjezbe)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            Exercise curExercise = documentSnapshot.toObject(Exercise.class);
                            int caloriesValue = 0;
                            int trajanje = curTraining.getTrajanje();

                            if (intenzitet.toLowerCase().equals("lagano")) {
                                caloriesValue = Math.round((float) trajanje / 60 * curExercise.getLagano());
                            } else if (intenzitet.toLowerCase().equals("srednje")) {
                                caloriesValue = Math.round((float) trajanje / 60 * curExercise.getSrednje());
                            } else if (intenzitet.toLowerCase().equals("tesko")) {
                                caloriesValue = Math.round((float) trajanje / 60 * curExercise.getTesko());
                            }

                            holder.name.setText(curExercise.getNaziv());
                            holder.calories.setText(Integer.toString(caloriesValue));
                            holder.duration.setText(Integer.toString(trajanje));
                            holder.intensity.setText(intenzitet);

                            if(curTraining.getOdradeno()){
                                holder.done.setText("odrađeno");
                                holder.done.setTextColor(Color.parseColor("#0ECE00"));
                            }else {
                                holder.done.setText("nije odrađeno");
                                holder.done.setTextColor(Color.parseColor("#CE0000"));
                            }
                        }
                    }
                });


        final Training currentTraining = trainingList.get(position);
        final String uid = currentTraining.getUid();

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return trainingList.size();
    }

    public class TrainingViewHolder extends RecyclerView.ViewHolder {

        View mView;
        public TextView name, duration, intensity, calories, done;
        public RelativeLayout elementContainer;


        public TrainingViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            name = (TextView) mView.findViewById(R.id.tvName);
            duration = mView.findViewById(R.id.tvTime);
            intensity = mView.findViewById(R.id.tvIntensity);
            calories = mView.findViewById(R.id.tvCalories);
            done = mView.findViewById(R.id.exDone);
            elementContainer = mView.findViewById(R.id.adapter_wrapper);
        }


    }

}
