package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class ExerciseInfoActivity extends AppCompatActivity {
    private ImageView exPic;
    private TextView tvDescription, tvIntensive, tvMedium, tvEasy;
    private String EX_ID;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseFirestore mFirestore;
    private FirebaseStorage mStorage;
    private RelativeLayout loadingContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_info);
        EX_ID = (String) getIntent().getSerializableExtra("EX_ID");

        // toolbar
        setTitle("");
        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
        mStorage = FirebaseStorage.getInstance();


        loadingContainer = findViewById(R.id.loadingContainer);
        tvDescription = findViewById(R.id.exInfo);
        tvIntensive = findViewById(R.id.intenzivnoValue);
        tvMedium = findViewById(R.id.srednjeValue);
        tvEasy = findViewById(R.id.laganoValue);
        exPic = findViewById(R.id.exPic);

        initializeDetails();
    }

    private void initializeDetails() {

        mFirestore.collection("vjezbe")
                .document(EX_ID)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()){
                            Exercise exercise = documentSnapshot.toObject(Exercise.class);

                            setTitle(exercise.getNaziv());
                            tvDescription.setText(exercise.getOpis());
                            tvIntensive.setText(Integer.toString(exercise.getTesko()));
                            tvMedium.setText(Integer.toString(exercise.getSrednje()));
                            tvEasy.setText(Integer.toString(exercise.getLagano()));

                            final String filepath = "vjezbePictures/" + EX_ID + ".jpg";
                            mStorage.getReference().child(filepath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    StorageReference mImageRef = mStorage.getReferenceFromUrl(uri.toString());
                                    GlideApp.with(ExerciseInfoActivity.this)
                                            .load(mImageRef)
                                            .into(exPic);
                                    loadingContainer.setVisibility(View.GONE);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    StorageReference mImageRef = mStorage.getReference("vjezbePictures/default.jpg");
                                    GlideApp.with(ExerciseInfoActivity.this)
                                            .load(mImageRef)
                                            .into(exPic);
                                    loadingContainer.setVisibility(View.GONE);
                                }
                            });
                        }
                    }
                });
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
