package com.colossus.poliklinikazarehabilitaciju;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.CategoryViewHolder> {
    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    public List<Category> categoryList;

    public CategoryListAdapter(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CategoryListAdapter.CategoryViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_category, parent, false);
        CategoryListAdapter.CategoryViewHolder vh = new CategoryListAdapter.CategoryViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull final CategoryListAdapter.CategoryViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        holder.tvCategoryName.setText(categoryList.get(position).getNaziv());
        final Category currentCategory = categoryList.get(position);
        final String uid = currentCategory.getUid();

        holder.buttonViewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // kad se klikne 3 gumbica, onda se otvara forma za edit kategorije
                Context con = v.getContext();
                Intent intent = new Intent(con, InsertCategoryActivity.class);
                intent.putExtra("ID", currentCategory.getId());
                intent.putExtra("EDIT", currentCategory.getId());
                con.startActivity(intent);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        View textView;
        public TextView tvCategoryName;
        public RelativeLayout buttonViewOption;

        public CategoryViewHolder(RelativeLayout v) {
            super(v);
            textView = v;

            tvCategoryName = textView.findViewById(R.id.tvCategoryName);
            buttonViewOption = textView.findViewById(R.id.adapter_wrapper);
        }
    }

}
