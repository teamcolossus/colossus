package com.colossus.poliklinikazarehabilitaciju;


import java.io.Serializable;

public class Category extends UserData implements Serializable {
    private String naziv, id;
    private boolean is_deleted;

    public Category(){
        super();
    }

    public Category(String naziv, String id, boolean is_deleted) {
        this.naziv = naziv;
        this.id = id;
        this.is_deleted = is_deleted;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

    @Override
    public String toString() {
        return "Category{" +
                "naziv='" + naziv + '\'' +
                '}';
    }
}
