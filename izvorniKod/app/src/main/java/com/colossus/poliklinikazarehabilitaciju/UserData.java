package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;

public class UserData {
    String uid;

    public String getUid() {
        return uid;
    }



    public <T extends UserData> T withUid(@NonNull final String uid){
        this.uid = uid;
        return (T) this;
    }
}
