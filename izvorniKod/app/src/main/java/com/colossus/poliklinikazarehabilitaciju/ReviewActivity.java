package com.colossus.poliklinikazarehabilitaciju;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class ReviewActivity extends AppCompatActivity {
    String senderUid, receiverUid;
    FirebaseFirestore mFirestore;
    FirebaseAuth mFirebaseAuth;
    private TextView userUname, employeeUname;
    private EditText tvReview, tvComment, tvScore;
    private LinearLayout review, comment;
    private RelativeLayout loadingContainer;
    User user;
    Employee employee;
    Menu optionsMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        receiverUid = (String) getIntent().getSerializableExtra("EMPLOYEE_UID");
        senderUid = (String) getIntent().getSerializableExtra("USER_UID");
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
        review = findViewById(R.id.reviewContainer);
        comment = findViewById(R.id.commentContainer);
        tvReview = findViewById(R.id.review);
        tvComment = findViewById(R.id.comment);
        userUname = findViewById(R.id.usernameSender);
        employeeUname = findViewById(R.id.usernameReply);
        tvScore = findViewById(R.id.reviewScore);
        loadingContainer = findViewById(R.id.loadingContainer);


        // toolbar
        setTitle("Recenzija");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        initialize();
    }

    private void initialize() {
        Task result = mFirestore.collection("klijent")
                .document(senderUid)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            user = documentSnapshot.toObject(User.class).withUid(senderUid);

                            mFirestore.collection("zaposlenik")
                                    .document(receiverUid)
                                    .get()
                                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                        @Override
                                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                                            if (documentSnapshot.exists()) {
                                                employee = documentSnapshot.toObject(Employee.class).withUid(receiverUid);
                                            }

                                            mFirestore.collection("recenzija")
                                                    .document(receiverUid)
                                                    .collection("recenzija")
                                                    .document(senderUid)
                                                    .get()
                                                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                        @Override
                                                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                            if (documentSnapshot.exists()) {
                                                                String reviewContent = documentSnapshot.getString("opis");
                                                                String commentContent = documentSnapshot.getString("komentar");
                                                                String score = documentSnapshot.getString("ocjena");

                                                                userUname.setText("@" + user.getKorisnicko_ime());
                                                                tvReview.setText(reviewContent);
                                                                tvScore.setText(score);
                                                                tvReview.setFocusable(false);
                                                                tvReview.setClickable(false);
                                                                tvReview.setFocusableInTouchMode(false);
                                                                tvScore.setFocusable(false);
                                                                tvScore.setClickable(false);
                                                                tvScore.setFocusableInTouchMode(false);
                                                                review.setVisibility(View.VISIBLE);

                                                                if (commentContent != null) {
                                                                    employeeUname.setText("@" + employee.getKorisnicko_ime());
                                                                    tvComment.setText(commentContent);
                                                                    tvComment.setFocusable(false);
                                                                    tvComment.setClickable(false);
                                                                    tvComment.setFocusableInTouchMode(false);
                                                                    comment.setVisibility(View.VISIBLE);
                                                                } else if (receiverUid.equals(mFirebaseAuth.getCurrentUser().getUid())) {
                                                                    employeeUname.setText("@" + employee.getKorisnicko_ime());
                                                                    getMenuInflater().inflate(R.menu.menu_send_employee, optionsMenu);
                                                                    comment.setVisibility(View.VISIBLE);
                                                                }
                                                            } else if (senderUid.equals(mFirebaseAuth.getCurrentUser().getUid())) {
                                                                userUname.setText("@" + user.getKorisnicko_ime());
                                                                getMenuInflater().inflate(R.menu.menu_send_user, optionsMenu);
                                                                review.setVisibility(View.VISIBLE);
                                                            } else {
                                                                Toast.makeText(getApplicationContext(), "Kako si dospio ovdje?", Toast.LENGTH_SHORT).show();
                                                                finish();
                                                            }
                                                            loadingContainer.setVisibility(View.GONE);
                                                        }
                                                    });
                                        }
                                    });
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        if (item.getItemId() == R.id.action_menu_send_user) {
            AlertDialog alertDialog = new AlertDialog.Builder(ReviewActivity.this).create();
            alertDialog.setTitle("Potvrdi slanje recenzije:");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            boolean valid = true;
                            String scoreContent = tvScore.getText().toString();
                            float scoreFloatContent = scoreContent.isEmpty() ? -1 : Float.parseFloat(scoreContent);
                            String reviewContent = tvReview.getText().toString();

                            if (reviewContent.isEmpty()) {
                                tvReview.setError("Napiši recenziju");
                                tvReview.requestFocus();
                                valid = false;
                            }

                            if (scoreContent.isEmpty()) {
                                tvScore.setError("Dodaj ocjenu");
                                tvScore.requestFocus();
                                valid = false;
                            } else if (scoreFloatContent > 5 || scoreFloatContent < 0) {
                                tvScore.setError("Ocjena je broj između 0 i 5");
                                tvScore.requestFocus();
                                valid = false;
                            }

                        if(valid) {
                            Map<String, Object> review = new HashMap<>();
                            review.put("uid_klijent", senderUid);
                            review.put("uid_zaposlenik", receiverUid);
                            review.put("ocjena", scoreContent);
                            review.put("opis", reviewContent);
                            review.put("komentar", null);

                            mFirestore.collection("recenzija")
                                    .document(receiverUid)
                                    .collection("recenzija")
                                    .document(senderUid)
                                    .set(review);


                            mFirestore.collection("zaposlenik").document(receiverUid).update("broj_recenzija", FieldValue.increment(1));
                            mFirestore.collection("zaposlenik").document(receiverUid).update("zbroj_ocjena", FieldValue.increment(scoreFloatContent));

                            tvReview.clearFocus();
                            tvReview.setFocusable(false);
                            tvReview.setClickable(false);
                            tvReview.setFocusableInTouchMode(false);
                            tvScore.clearFocus();
                            tvScore.setFocusable(false);
                            tvScore.setClickable(false);
                            tvScore.setFocusableInTouchMode(false);
                            Toast.makeText(getApplicationContext(), "Recenzija poslana", Toast.LENGTH_SHORT).show();
                            item.setVisible(false);
                        }
                    }
        });
        alertDialog.show();
    }
        if(item.getItemId()==R.id.action_menu_send_employee)

    {
        AlertDialog alertDialog = new AlertDialog.Builder(ReviewActivity.this).create();
        alertDialog.setTitle("Potvrdi slanje komentara:");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        boolean valid = true;
                        String commentContent = tvComment.getText().toString();

                        if (commentContent.isEmpty()) {
                            tvComment.setError("Napiši komentar");
                            tvComment.requestFocus();
                            valid = false;
                        }

                        if(valid) {
                            Map<String, Object> review = new HashMap<>();
                            review.put("komentar", commentContent);

                            mFirestore.collection("recenzija")
                                    .document(receiverUid)
                                    .collection("recenzija")
                                    .document(senderUid)
                                    .update(review);

                            tvComment.clearFocus();
                            tvComment.setFocusable(false);
                            tvComment.setClickable(false);
                            tvComment.setFocusableInTouchMode(false);
                            Toast.makeText(getApplicationContext(), "Komentar poslan", Toast.LENGTH_SHORT).show();
                            item.setVisible(false);
                        }
                    }
                });

        alertDialog.show();
    }
        return super.

    onOptionsItemSelected(item);

}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        optionsMenu = menu;
        return true;
    }
}
