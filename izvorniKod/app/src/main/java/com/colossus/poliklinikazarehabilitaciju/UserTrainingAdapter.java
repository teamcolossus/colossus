package com.colossus.poliklinikazarehabilitaciju;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class UserTrainingAdapter extends RecyclerView.Adapter<UserTrainingAdapter.UserTrainingViewHolder> {
    @NonNull

    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    private List<Training> trainingList;
    private Context context;
    private String date, user;
    private boolean ADD;
    private String currentDate;

    public UserTrainingAdapter(List<Training> trainingList, String date, String user, boolean ADD) {
        this.trainingList = trainingList;
        this.user = user;
        this.date = date;
        this.ADD = ADD;
        this.currentDate = new SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(new Date());
    }

    @Override
    public UserTrainingAdapter.UserTrainingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_training_user, parent, false);
        UserTrainingAdapter.UserTrainingViewHolder vh = new UserTrainingAdapter.UserTrainingViewHolder(v);
        return vh;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @Override
    public void onBindViewHolder(@NonNull final UserTrainingAdapter.UserTrainingViewHolder holder, int position) {
        final Training curTraining = trainingList.get(position);

        final String trainingId = curTraining.getUid();
        final String idVjezbe = curTraining.getId_vjezba();
        final String trajanje = curTraining.getTrajanje().toString();
        final String intenzitet = curTraining.getIntenzitet();

        mFirebaseFirestore.collection("vjezbe")
                .document(idVjezbe)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        Exercise exerciseCurrent = documentSnapshot.toObject(Exercise.class);

                        int kalorije = 0;
                        if (intenzitet.toLowerCase().equals("lagano")) {
                            kalorije = Math.round((float) curTraining.getTrajanje() / 60 * exerciseCurrent.getLagano());
                        } else if (intenzitet.toLowerCase().equals("srednje")) {
                            kalorije = Math.round((float) curTraining.getTrajanje() / 60 * exerciseCurrent.getSrednje());
                        } else if (intenzitet.toLowerCase().equals("tesko")) {
                            kalorije = Math.round((float) curTraining.getTrajanje() / 60 * exerciseCurrent.getTesko());
                        }

                        holder.tvExcCalories.setText(Integer.toString(kalorije));
                        holder.tvExcTime.setText(trajanje);
                        holder.tvExcInt.setText(intenzitet);
                        holder.tvExcName.setText(exerciseCurrent.getNaziv());
                    }
                });

        final Training currentTraining = trainingList.get(position);
        final String uid = currentTraining.getUid();

        if(currentTraining.getOdradeno()){
            holder.select();
        }else{
            holder.wrapper.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!holder.isSelected) {
                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setTitle("Postavi vježbu odrađenom?");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        holder.select();

                                        Map<String, Object> done = new HashMap<>();
                                        done.put("odradeno", true);
                                        mFirebaseFirestore.collection("trening")
                                                .document(user)
                                                .collection("datum")
                                                .document(date)
                                                .collection("trening")
                                                .document(trainingId)
                                                .update(done);
                                        dialog.dismiss();
                                        if(ADD){
                                            ((Activity) context).finish();
                                            }
                                    }
                                });
                        alertDialog.show();
                    }
                }
            });

            if(!currentDate.equals(date)){
                holder.wrapper.setClickable(false);
            }
        }

        holder.btnExcInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ExerciseInfoActivity.class);
                intent.putExtra("EX_ID", idVjezbe);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return trainingList.size();
    }

    public class UserTrainingViewHolder extends RecyclerView.ViewHolder {
        View textView;
        public TextView tvExcName;
        public TextView tvExcTime;
        public TextView tvExcInt;
        public TextView tvExcCalories;
        public TextView tv1,tv2,tv3;
        public ImageView btnExcInfo;
        public RelativeLayout wrapper;
        public boolean isSelected = false;
        ColorStateList oldColorsMain, oldColors;

        public UserTrainingViewHolder(RelativeLayout v) {
            super(v);
            textView = v;

            wrapper = textView.findViewById(R.id.adapter_wrapper);
            tvExcName = textView.findViewById(R.id.tvExcName);
            tvExcTime = textView.findViewById(R.id.tvExcTime);
            tvExcInt = textView.findViewById(R.id.tvExcInt);
            tvExcCalories = textView.findViewById(R.id.tvExcCalories);
            btnExcInfo = textView.findViewById(R.id.btnExcInfo);
            tv1 = textView.findViewById(R.id.tv1);
            tv2 = textView.findViewById(R.id.tv2);
            tv3 = textView.findViewById(R.id.tv3);
            oldColorsMain =  tvExcName.getTextColors();
            oldColors =  tv1.getTextColors();
        }

        public void select() {
            if (!isSelected) {
                wrapper.setBackground(ContextCompat.getDrawable(context, R.drawable.bottom_border_color_select));
                tvExcName.setTextColor(Color.WHITE);
                tvExcTime.setTextColor(Color.WHITE);
                tvExcInt.setTextColor(Color.WHITE);
                tvExcCalories.setTextColor(Color.WHITE);
                tv1.setTextColor(Color.WHITE);
                tv2.setTextColor(Color.WHITE);
                tv3.setTextColor(Color.WHITE);
                wrapper.setClickable(false);
                isSelected = !isSelected;
            } else {
                wrapper.setBackground(ContextCompat.getDrawable(context, R.drawable.bottom_border_color));
                tvExcName.setTextColor(oldColorsMain);
                tvExcTime.setTextColor(oldColors);
                tvExcInt.setTextColor(oldColors);
                tvExcCalories.setTextColor(oldColors);
                tv1.setTextColor(oldColors);
                tv2.setTextColor(oldColors);
                tv3.setTextColor(oldColors);
                isSelected = !isSelected;
            }

        }
    }
}
