package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class HomeEmployeeFragment extends Fragment {
    FirebaseFirestore mFirestore;
    FirebaseAuth mFirebaseAuth;
    private LinearLayout myClientsL, newClientsL, addFoodL, addExcerciseL;
    private Employee currentEmployee;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home_employee, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        myClientsL= getView().findViewById(R.id.my_clients_wrapper);
        newClientsL= getView().findViewById(R.id.new_clients_wrapper);
        addFoodL= getView().findViewById(R.id.add_food_employee_wrapper);
        addExcerciseL= getView().findViewById(R.id.add_excercise_employee_wrapper);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Activity mainActivity = getActivity();

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();


        addFoodL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToFoodAdd = new Intent(mainActivity, FoodAddActivity.class);
                startActivity(intToFoodAdd);
            }
        });

        addExcerciseL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent insertExercise = new Intent(mainActivity, CreateNewWorkoutActivity.class);
                startActivity(insertExercise);
            }
        });

        myClientsL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().findViewById(R.id.nav_clients).performClick();

            }
        });

        mFirestore.collection("zaposlenik")
                .document(mFirebaseAuth.getCurrentUser().getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            currentEmployee = documentSnapshot.toObject(Employee.class).withUid(mFirebaseAuth.getCurrentUser().getUid());
                            newClientsL.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intToNew = new Intent(mainActivity, NewClientsActivity.class);
                                    intToNew.putExtra("ROLE",currentEmployee.id_ovlasti);
                                    startActivity(intToNew);
                                }
                            });
                        }
                    }
                });


    }

}
