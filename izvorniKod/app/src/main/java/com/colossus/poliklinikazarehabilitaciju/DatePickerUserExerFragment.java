package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;

public class DatePickerUserExerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int dateYear = c.get(Calendar.YEAR);
        int dateMonth = c.get(Calendar.MONTH);
        int dateDay = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, dateYear, dateMonth, dateDay);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        month+=1;
        ((UserTrainingActivity)getActivity()).getExerForTraining(year, month, day);
    }
}
