package com.colossus.poliklinikazarehabilitaciju;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;

public class CreateNewWorkoutActivity extends AppCompatActivity {

    EditText imeVjezbe, laganoValue, srednjeValue, intenzivnoValue, opisVjezbe;
    FirebaseFirestore mFirestore;
    private int any_integer_variable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_workout2);

        mFirestore = FirebaseFirestore.getInstance();
        imeVjezbe = findViewById(R.id.imeVjezbe);
        laganoValue = findViewById(R.id.laganoValue);
        srednjeValue = findViewById(R.id.srednjeValue);
        intenzivnoValue = findViewById(R.id.intenzivnoValue);
        opisVjezbe = findViewById(R.id.opisVjezbe);

        // toolbar
        setTitle("Stvori vježbu");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dalje, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            case R.id.action_menu_dalje:
                if (imeVjezbe.getText().toString().equals("")) {
                    Toast.makeText(CreateNewWorkoutActivity.this, "Napisite ime vjezbe", Toast.LENGTH_SHORT).show();
                    break;
                }

                if (opisVjezbe.getText().toString().equals("")) {
                    Toast.makeText(CreateNewWorkoutActivity.this, "Napisite opis vjezbe", Toast.LENGTH_SHORT).show();
                    break;
                }

                try {
                    Integer.parseInt(laganoValue.getText().toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(CreateNewWorkoutActivity.this, "Neispravan unos kod Lagano", Toast.LENGTH_SHORT).show();
                    break;
                }

                try {
                    Integer.parseInt(srednjeValue.getText().toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(CreateNewWorkoutActivity.this, "Neispravan unos kod Srednje", Toast.LENGTH_SHORT).show();
                    break;
                }

                try {
                    Integer.parseInt(intenzivnoValue.getText().toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(CreateNewWorkoutActivity.this, "Neispravan unos kod Intenzivno", Toast.LENGTH_SHORT).show();
                    break;
                }

                Intent intent = new Intent(CreateNewWorkoutActivity.this, ImgForWorkoutActivity.class);

                intent.putExtra("IME_VJEZBE", imeVjezbe.getText().toString());
                intent.putExtra("OPIS_VJEZBE", opisVjezbe.getText().toString());
                intent.putExtra("LAGANO_VALUE", laganoValue.getText().toString());
                intent.putExtra("SREDNJE_VALUE", srednjeValue.getText().toString());
                intent.putExtra("INTENZIVNO_VALUE", intenzivnoValue.getText().toString());

                startActivityForResult(intent,any_integer_variable);
                break;

        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==2){
            finish();
        }
    }
}
