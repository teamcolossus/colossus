package com.colossus.poliklinikazarehabilitaciju;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class ExerciseDiaryAdapter extends RecyclerView.Adapter<ExerciseDiaryAdapter.ViewHolder> {
    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    public List<DiaryExercise> exerciseDiaryList;
    private Context context;

    public ExerciseDiaryAdapter(List<DiaryExercise> exerciseDiaryList) {
        this.exerciseDiaryList = exerciseDiaryList;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ExerciseDiaryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_diary_excercise, parent, false);
        return new ExerciseDiaryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ExerciseDiaryAdapter.ViewHolder holder, int position) {
        final DiaryExercise currentDiaryElement = exerciseDiaryList.get(position);
        String name = currentDiaryElement.getId_vjezba();
        final int duration = currentDiaryElement.getTrajanje();
        final String intensity = currentDiaryElement.getIntenzitet();


        mFirebaseFirestore.collection("vjezbe")
                .document(name)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            Exercise currentExercise = documentSnapshot.toObject(Exercise.class);
                            int caloriesValue = 0;

                            if (intensity.toLowerCase().equals("lagano")) {
                                caloriesValue = Math.round((float) duration / 60 * currentExercise.getLagano());
                            } else if (intensity.toLowerCase().equals("srednje")) {
                                caloriesValue = Math.round((float) duration / 60  * currentExercise.getSrednje());
                            } else if (intensity.toLowerCase().equals("tesko")) {
                                caloriesValue = Math.round((float) duration / 60 * currentExercise.getTesko());
                            }

                            holder.name.setText(currentExercise.getNaziv());
                            holder.duration.setText(Integer.toString(duration));
                            holder.intensity.setText(intensity);
                            holder.calories.setText(Integer.toString(caloriesValue));
                        }

                    }
                });
    }

    @Override
    public int getItemCount() {
        return exerciseDiaryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        public TextView name, duration, intensity, calories;
        public RelativeLayout elementContainer;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            name = (TextView) mView.findViewById(R.id.tvName);
            duration = mView.findViewById(R.id.tvTime);
            intensity = mView.findViewById(R.id.tvIntensity);
            calories = mView.findViewById(R.id.tvCalories);
            elementContainer = mView.findViewById(R.id.adapter_wrapper);
        }


    }
}
