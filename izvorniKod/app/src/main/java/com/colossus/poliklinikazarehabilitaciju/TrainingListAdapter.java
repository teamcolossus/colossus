package com.colossus.poliklinikazarehabilitaciju;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class TrainingListAdapter extends RecyclerView.Adapter<TrainingListAdapter.TrainingViewHolder> {
    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    public List<Training> trainingList;

    public TrainingListAdapter(List<Training> trainingList) {
        this.trainingList = trainingList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TrainingListAdapter.TrainingViewHolder onCreateViewHolder(ViewGroup parent,
                                                                     int viewType) {
        // create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_training, parent, false);
        TrainingListAdapter.TrainingViewHolder vh = new TrainingListAdapter.TrainingViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull final TrainingListAdapter.TrainingViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Training curTraining = trainingList.get(position);

        String idVjezbe = curTraining.getId_vjezba();
        String trajanje = curTraining.getTrajanje().toString();
        String intenzitet = curTraining.getIntenzitet();
        String odradeno = curTraining.getOdradeno().toString();

        mFirebaseFirestore.collection("vjezbe")
                .document(idVjezbe)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        holder.tvExerciseName.setText((String)documentSnapshot.get("naziv"));
                    }
                });
        holder.txtViewInfo.setText(trajanje + "min-" + intenzitet + "-" + odradeno);
        final Training currentTraining = trainingList.get(position);
        final String uid = currentTraining.getUid();

        holder.buttonViewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // kad se klikne 3 gumbica, onda se otvara forma za edit treninga
//                Context con = v.getContext();
//                Intent intent = new Intent(con, InsertTrainingActivity.class);
//                intent.putExtra("datum_treninga", currentTraining.getDatum());
//                con.startActivity(intent);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return trainingList.size();
    }

    public class TrainingViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        View textView;
        public TextView tvExerciseName;
        public TextView txtViewInfo;
        public ImageView buttonViewOption;

        public TrainingViewHolder(RelativeLayout v) {
            super(v);
            textView = v;

            tvExerciseName = textView.findViewById(R.id.tvExerciseName);
            txtViewInfo = textView.findViewById(R.id.txtViewInfo);
            buttonViewOption = textView.findViewById(R.id.btnViewOption);
        }
    }

}
