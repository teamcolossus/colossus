package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;


public class RegisterUserActivity extends AppCompatActivity {

    EditText usernameInput, nameInput, surnameInput, emailInput, passwordInput, confrimPasswordInput;
    FirebaseAuth mFirebaseAuth;
    FirebaseFirestore mFirestore;
    int userType;

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
        userType = getIntent().getIntExtra("EXTRA_USER_TYPE", 1);

        mFirestore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        usernameInput = findViewById(R.id.usernameSignUpUser);
        nameInput = findViewById(R.id.nameSignUpUser);
        surnameInput = findViewById(R.id.surnameSignUpUser);
        emailInput = findViewById(R.id.emailSignUpUser);
        passwordInput = findViewById(R.id.passwordSignUpUser);
        confrimPasswordInput = findViewById(R.id.confirmPasswordSignUpUser);

        emailInput.requestFocus();

        setTitle("Registracija klijenta");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu_done:
                final String email = emailInput.getText().toString().trim();
                final String username = usernameInput.getText().toString().trim();
                final String name = nameInput.getText().toString().trim();
                final String surname = surnameInput.getText().toString().trim();
                final String password = passwordInput.getText().toString();
                String confirmPassword = confrimPasswordInput.getText().toString();
                Boolean valid = true;

                if (confirmPassword.isEmpty()) {
                    confrimPasswordInput.setError("Potvrdi lozinku");
                    confrimPasswordInput.requestFocus();
                    valid = false;
                }

                if (password.isEmpty()) {
                    passwordInput.setError("Upiši lozinku");
                    passwordInput.requestFocus();
                    valid = false;
                } else if (!password.equals(confirmPassword) && !confirmPassword.isEmpty()) {
                    confrimPasswordInput.setError("Lozinke se ne podudaraju");
                    confrimPasswordInput.requestFocus();
                    valid = false;
                }


                if (surname.isEmpty()) {
                    surnameInput.setError("Upiši svoje prezime");
                    surnameInput.requestFocus();
                    valid = false;
                }

                if (name.isEmpty()) {
                    nameInput.setError("Upiši svoje ime");
                    nameInput.requestFocus();
                    valid = false;
                }

                if (username.isEmpty()) {
                    usernameInput.setError("Upiši korisničko ime");
                    usernameInput.requestFocus();
                    valid = false;
                }

                //TODO: provjeriti unikatnost usernamea i sadrzi li dobar format

                if (email.isEmpty()) {
                    emailInput.setError("Upiši email");
                    emailInput.requestFocus();
                    valid = false;
                } else if (!isValidEmail(email)) {
                    emailInput.setError("Email nije dobrog formata");
                    emailInput.requestFocus();
                    valid = false;
                }

                if (valid) {
                    mFirestore.collection("usernames").document(username).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if(documentSnapshot.exists()){
                                usernameInput.setError("Korisničko ime zauzeto");
                                usernameInput.requestFocus();
                            } else {
                                mFirebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(RegisterUserActivity.this, "Došlo je do greške, pokušaj ponovno", Toast.LENGTH_SHORT).show();
                                        } else {
                                            String uid = mFirebaseAuth.getCurrentUser().getUid();

                                            Map<String, Object> user = new HashMap<>();
                                            user.put("email", email);
                                            user.put("korisnicko_ime", username);
                                            user.put("ime", name);
                                            user.put("prezime", surname);
                                            user.put("doktor", null);
                                            user.put("trener", null);

                                            Map<String, Object> reroute = new HashMap<>();
                                            reroute.put("id_ovlasti", Integer.toString(userType));
                                            reroute.put("potvrden", true);
                                            reroute.put("delete", false);

                                            Map<String, Object> uName = new HashMap<>();
                                            uName.put("email", email);

                                            mFirestore.collection("klijent")
                                                    .document(uid)
                                                    .set(user);

                                            mFirestore.collection("reroute")
                                                    .document(uid)
                                                    .set(reroute);

                                            mFirestore.collection("usernames")
                                                    .document(username)
                                                    .set(uName);

                                            Intent intToHomeUser = new Intent(getApplicationContext(), ReroutingActivity.class);
                                            startActivity(intToHomeUser);
                                            finish();
                                        }
                                    }
                                });
                            }
                        }
                    });

                }
                break;

        }
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    Log.d("focus", "touchevent");
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}
