package com.colossus.poliklinikazarehabilitaciju;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class ChooseUserTypePopUp extends Activity {
    Button btnChooseUser, btnChooseDoctor, btnChooseTrainer;
    TextView tvRegisterAs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_user_type_pop_up);

        btnChooseUser = findViewById(R.id.btnChooseUser);
        btnChooseDoctor = findViewById(R.id.btnChooseDoctor);
        btnChooseTrainer = findViewById(R.id.btnChooseTrainer);
        tvRegisterAs = findViewById(R.id.tvRegisterAs);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*0.9), (int)(height*0.7));
        getWindow().setBackgroundDrawable(new ColorDrawable(
                0xE6FFFFFF));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = -20;
        getWindow().setAttributes(params);

        btnChooseUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToRegisterUser = new Intent(getApplicationContext(), RegisterUserActivity.class);
                intToRegisterUser.putExtra("EXTRA_USER_TYPE", 1);
                startActivity(intToRegisterUser);
                finish();
            }
        });


        btnChooseDoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToRegisterDoctor = new Intent(getApplicationContext(), RegisterEmployeeActivity.class);
                intToRegisterDoctor.putExtra("EXTRA_USER_TYPE", 2);
                startActivity(intToRegisterDoctor);
                finish();
            }
        });

        btnChooseTrainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToRegisterTrainer = new Intent(getApplicationContext(), RegisterEmployeeActivity.class);
                intToRegisterTrainer.putExtra("EXTRA_USER_TYPE", 3);
                startActivity(intToRegisterTrainer);
                finish();
            }
        });
    }
}
