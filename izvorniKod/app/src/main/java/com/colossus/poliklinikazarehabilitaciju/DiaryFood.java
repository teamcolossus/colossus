package com.colossus.poliklinikazarehabilitaciju;

public class DiaryFood extends UserData {

    private int masa;
    private String barkod;

    public DiaryFood() {
        super();
    }

    public DiaryFood(int masa, String barkod) {
        this.masa = masa;
        this.barkod = barkod;
    }

    public int getMasa() {
        return masa;
    }

    public void setMasa(int masa) {
        this.masa = masa;
    }

    public String getBarkod() {
        return barkod;
    }

    public void setBarkod(String barkod) {
        this.barkod = barkod;
    }
}
