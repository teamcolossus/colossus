package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class InsertTrainingActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private String userUid;
    private String nazivVjezbe;
    private String idVjezbe;
    private String datum;
    private Spinner intenzitetSpinner;
    private ArrayList<String> intenziteti = new ArrayList<>();
    private ArrayAdapter<String> adapter;

    TextView tvNaziv,
            tvLagano,
            tvSrednje,
            tvTesko,
            tvTrajanje,
            intensityLabel;

    FirebaseAuth mFirebaseAuth;
    FirebaseFirestore mFireStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_training);

        userUid = (String) getIntent().getSerializableExtra("user_uid");
        nazivVjezbe = (String) getIntent().getSerializableExtra("naziv_vjezbe");
        datum = (String) getIntent().getSerializableExtra("datum");

        mFireStore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();

        // toolbar
        setTitle("Postavi vježbu treninga");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        tvNaziv = findViewById(R.id.imeVjezbe);
        tvLagano = findViewById(R.id.laganoValue);
        tvSrednje = findViewById(R.id.srednjeValue);
        tvTesko = findViewById(R.id.intenzivnoValue);
        tvTrajanje = findViewById(R.id.trainingTime);
        intenzitetSpinner = findViewById(R.id.intensitySpinner);
        intensityLabel = findViewById(R.id.intensityLabel);

        intenziteti.add("-");
        intenziteti.add("lagano");
        intenziteti.add("srednje");
        intenziteti.add("tesko");

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, intenziteti);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        intenzitetSpinner.setAdapter(adapter);
        intenzitetSpinner.setOnItemSelectedListener(this);


        mFireStore.collection("vjezbe")
                .whereEqualTo("naziv", nazivVjezbe)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot qds : task.getResult()) {

                                idVjezbe = qds.getId();

                                tvNaziv.setText(qds.get("naziv").toString());
                                tvLagano.setText(qds.get("lagano").toString());
                                tvSrednje.setText(qds.get("srednje").toString());
                                tvTesko.setText(qds.get("tesko").toString());
                            }
                        } else {
                            System.out.println("Nisam uspio naći vježbu");
                        }
                    }
                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_menu_done:
                final String intenzitet = intenzitetSpinner.getSelectedItem().toString();
                final String trajanje = tvTrajanje.getText().toString();
                boolean valid = true;

                if (trajanje.isEmpty()) {
                    tvTrajanje.setError("Unesi trajanje vježbe");
                    tvTrajanje.requestFocus();
                    valid = false;
                }
                if (intenzitet.isEmpty() || intenzitet.equals("-")) {
                    intensityLabel.setError("Odaberi intenzitet vježbe");
                    intenzitetSpinner.requestFocus();
                    valid = false;
                }
                if (valid) {
                    final Map<String, Object> treningKlijent = new HashMap<>();
                    treningKlijent.put("intenzitet", intenzitet);
                    treningKlijent.put("trajanje", Integer.parseInt(trajanje));
                    treningKlijent.put("odradeno", false);
                    treningKlijent.put("id_vjezba", idVjezbe);


                    final Map<String, Object> treningPostavljen = new HashMap<>();
                    treningPostavljen.put("postavljen", true);
                    mFireStore.collection("trening")
                            .document(userUid)
                            .set(treningPostavljen);

                    mFireStore.collection("trening")
                            .document(userUid)
                            .collection("datum")
                            .document(datum)
                            .collection("trening")
                            .document()
                            .set(treningKlijent)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    System.out.println("Uspjesno dodana vjezba treningu");
//                                    Intent intent = new Intent(InsertTrainingActivity.this, TrainingExercisesListActivity.class);
//                                    startActivity(intent);

                                    setResult(Activity.RESULT_OK);
                                    finish();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    System.out.println("Neuspjelo dodavanje vjezbe treningu");
                                }
                            });

                }
                break;
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    Log.d("focus", "touchevent");
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}
