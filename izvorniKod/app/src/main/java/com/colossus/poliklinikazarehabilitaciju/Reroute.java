package com.colossus.poliklinikazarehabilitaciju;

public class Reroute {
    private String id_ovlasti;
    private boolean potvrden, delete;

    public Reroute() {
    }

    public Reroute(String id_ovlasti, boolean potvrden, boolean delete) {
        this.id_ovlasti = id_ovlasti;
        this.potvrden = potvrden;
        this.delete = delete;
    }

    public String getId_ovlasti() {
        return id_ovlasti;
    }

    public void setId_ovlasti(String id_ovlasti) {
        this.id_ovlasti = id_ovlasti;
    }

    public boolean isPotvrden() {
        return potvrden;
    }

    public void setPotvrden(boolean potvrden) {
        this.potvrden = potvrden;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
}
