package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class HomeAdminFragment extends Fragment {
    private LinearLayout clientsL, employeesL, confirmL, addFoodL, editFoodL, addExcerciseL, editExcerciseL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home_admin, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        clientsL= getView().findViewById(R.id.clients_wrapper_admin);
        employeesL= getView().findViewById(R.id.employees_wrapper_admin);
        confirmL= getView().findViewById(R.id.approval_settings_wrapper);
        addFoodL= getView().findViewById(R.id.add_food_settings_wrapper);
        editFoodL= getView().findViewById(R.id.edit_food_settings_wrapper);
        addExcerciseL= getView().findViewById(R.id.add_excercise_settings_wrapper);
        editExcerciseL= getView().findViewById(R.id.edit_excercise_settings_wrapper);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Activity mainActivity = getActivity();

        confirmL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().findViewById(R.id.nav_waiting).performClick();
            }
        });

        clientsL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToUsers = new Intent(mainActivity, AllUsersActivity.class);
                startActivity(intToUsers);
            }
        });

        employeesL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToEmployees = new Intent(mainActivity, AllEmployeesActivity.class);
                startActivity(intToEmployees);
            }
        });

        addFoodL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToFoodAdd = new Intent(mainActivity, FoodAddActivity.class);
                startActivity(intToFoodAdd);
            }
        });

        editFoodL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToFoodList = new Intent(mainActivity, EditFoodTabActivity.class);
                startActivity(intToFoodList);
            }
        });

        addExcerciseL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent insertExercise = new Intent(mainActivity, CreateNewWorkoutActivity.class);
                startActivity(insertExercise);
            }
        });

        editExcerciseL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent exerciseList = new Intent(mainActivity, ExerciseListActivity.class);
                startActivity(exerciseList);
            }
        });
    }
}
