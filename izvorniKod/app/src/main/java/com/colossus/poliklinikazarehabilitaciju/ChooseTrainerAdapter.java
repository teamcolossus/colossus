package com.colossus.poliklinikazarehabilitaciju;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChooseTrainerAdapter extends RecyclerView.Adapter<ChooseTrainerAdapter.ViewHolder> {
    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    public List<Employee> employeeList;
    private Context context;
    private FirebaseStorage mStorage = FirebaseStorage.getInstance();


    public ChooseTrainerAdapter(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_employee_choose_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.nameText.setText(employeeList.get(position).getFullName());

        final Employee currentEmployee = employeeList.get(position);
        final String uid = currentEmployee.getUid();

        final String filepath = "profilePictures/" + uid + ".jpg";
        mStorage.getReference().child(filepath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                StorageReference mImageRef = mStorage.getReferenceFromUrl(uri.toString());
                GlideApp.with(context)
                        .load(mImageRef)
                        .into(holder.profileImg);
                holder.loadingContainer.setVisibility(View.GONE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                StorageReference mImageRef = mStorage.getReference("profilePictures/default.jpg");
                GlideApp.with(context)
                        .load(mImageRef)
                        .into(holder.profileImg);
                holder.loadingContainer.setVisibility(View.GONE);
            }
        });

        holder.wrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToProfile = new Intent(context, ProfileEmployeeActivity.class);
                intToProfile.putExtra("EMPLOYEE_UID", uid);
                context.startActivity(intToProfile);
            }
        });

        holder.buttonViewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(view.getContext(), holder.buttonViewOption);
                //inflating menu from xml resource
                popup.inflate(R.menu.choose_employee_popup);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.popChoose:
                                final Map<String, Object> assignTrainerMap = new HashMap<>();
                                assignTrainerMap.put("trener", currentEmployee.getUid());

                                mFirebaseFirestore.collection("klijent")
                                        .document(mFirebaseAuth.getCurrentUser().getUid())
                                        .get()
                                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                            @Override
                                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                if (documentSnapshot.exists()) {
                                                    String trainerUid = documentSnapshot.getString("trener");

                                                    final Map<String, Object> assignMap = new HashMap<>();
                                                    assignMap.put("ime", documentSnapshot.getString("ime"));
                                                    assignMap.put("prezime", documentSnapshot.getString("prezime"));
                                                    if (trainerUid != null) {
                                                        //Toast.makeText(holder.this, "Trener je već odabran" + trainerUid, Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                                                        alertDialog.setTitle("Potvrdi odabir trenera:");
                                                        alertDialog.setMessage(currentEmployee.getFullName());
                                                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                                                new DialogInterface.OnClickListener() {
                                                                    public void onClick(DialogInterface dialog, int which) {
                                                                        mFirebaseFirestore.collection("klijent").document(mFirebaseAuth.getCurrentUser().getUid()).update(assignTrainerMap);
                                                                        mFirebaseFirestore.collection("trener").document(uid).collection("klijenti").document(mFirebaseAuth.getCurrentUser().getUid()).set(assignMap);
                                                                        mFirebaseFirestore.collection("zaposlenik").document(currentEmployee.getUid()).update("trenutno_korisnika", FieldValue.increment(1));
                                                                        dialog.dismiss();
                                                                        Intent intent = new Intent(context, HomeUserFragment.class);
                                                                        context.startActivity(intent);
                                                                        ((Activity) context).finish();
                                                                    }
                                                                });
                                                        alertDialog.show();
                                                    }
                                                }
                                            }
                                        });
                                return true;

                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return employeeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        public TextView nameText;
        public ImageView buttonViewOption;
        public RelativeLayout wrapper;
        public ImageView profileImg;
        public ProgressBar loadingContainer;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            profileImg = mView.findViewById(R.id.profilePicture);
            loadingContainer = (ProgressBar) mView.findViewById(R.id.progressBar);
            wrapper = mView.findViewById(R.id.adapter_wrapper);
            nameText = (TextView) mView.findViewById(R.id.tvEmployeeName);
            buttonViewOption = (ImageView) mView.findViewById(R.id.btnViewOption);
        }
    }
}
