package com.colossus.poliklinikazarehabilitaciju;

import java.io.Serializable;

class Product extends UserData implements Serializable {
    private String naziv;
    private String barkod;
    private String proizvodac;
    private String kategorija;
    private Float energija;
    private Float masnoca;
    private Float zas_masne_kiseline;
    private Float ugljikohidrati;
    private Float seceri;
    private Float bjelancevine;
    private Float sol;
    private Boolean is_deleted;

    public Product() {
        super();
    }

    public Product(String naziv, String barkod, String proizvodac, String kategorija, Float energija, Float masnoca, Float zas_masne_kiseline, Float ugljikohidrati, Float seceri, Float bjelancevine, Float sol, Boolean is_deleted) {
        this.naziv = naziv;
        this.barkod = barkod;
        this.proizvodac = proizvodac;
        this.kategorija = kategorija;
        this.energija = energija;
        this.masnoca = masnoca;
        this.zas_masne_kiseline = zas_masne_kiseline;
        this.ugljikohidrati = ugljikohidrati;
        this.seceri = seceri;
        this.bjelancevine = bjelancevine;
        this.sol = sol;
        this.is_deleted = is_deleted;
    }

    public Product(String naziv, String barkod, String kategorija, Float energija, Float masnoca, Float zas_masne_kiseline, Float ugljikohidrati, Float seceri, Float bjelancevine, Float sol, Boolean is_deleted) {
        this.naziv = naziv;
        this.barkod = barkod;
        this.kategorija = kategorija;
        this.energija = energija;
        this.masnoca = masnoca;
        this.zas_masne_kiseline = zas_masne_kiseline;
        this.ugljikohidrati = ugljikohidrati;
        this.seceri = seceri;
        this.bjelancevine = bjelancevine;
        this.sol = sol;
        this.is_deleted = is_deleted;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getBarkod() {
        return barkod;
    }

    public void setBarkod(String barkod) {
        this.barkod = barkod;
    }

    public String getProizvodac() {
        return proizvodac;
    }

    public void setProizvodac(String proizvodac) {
        this.proizvodac = proizvodac;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public Float getEnergija() {
        return energija;
    }

    public void setEnergija(Float energija) {
        this.energija = energija;
    }

    public Float getMasnoca() {
        return masnoca;
    }

    public void setMasnoca(Float masnoca) {
        this.masnoca = masnoca;
    }

    public Float getZas_masne_kiseline() {
        return zas_masne_kiseline;
    }

    public void setZas_masne_kiseline(Float zas_masne_kiseline) {
        this.zas_masne_kiseline = zas_masne_kiseline;
    }

    public Float getUgljikohidrati() {
        return ugljikohidrati;
    }

    public void setUgljikohidrati(Float ugljikohidrati) {
        this.ugljikohidrati = ugljikohidrati;
    }

    public Float getSeceri() {
        return seceri;
    }

    public void setSeceri(Float seceri) {
        this.seceri = seceri;
    }

    public Float getBjelancevine() {
        return bjelancevine;
    }

    public void setBjelancevine(Float bjelancevine) {
        this.bjelancevine = bjelancevine;
    }

    public Float getSol() {
        return sol;
    }

    public void setSol(Float sol) {
        this.sol = sol;
    }

    public Boolean getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

    @Override
    public String toString() {
        return "Product{" +
                "naziv='" + naziv + '\'' +
                ", barkod=" + barkod +
                ", energija=" + energija +
                ", masnoca=" + masnoca +
                ", zasiceneMasneKiseline=" + zas_masne_kiseline +
                ", ugljikohidrati=" + ugljikohidrati +
                ", seceri=" + seceri +
                ", bjelancevine=" + bjelancevine +
                ", sol=" + sol +
                ", isDeleted=" + is_deleted +
                '}';
    }

/*
    @Override
    public boolean equals(@Nullable Object o) {
        if(o == this){
            return true;
        }
        if(o == null || o.getClass() != this.getClass()){
            return false;
        }

        Product p = (Product) o;
        return getBarkod() == ((Product) o).getBarkod();
    }

*/

}