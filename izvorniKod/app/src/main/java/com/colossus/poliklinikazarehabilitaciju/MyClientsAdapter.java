package com.colossus.poliklinikazarehabilitaciju;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

public class MyClientsAdapter extends RecyclerView.Adapter<MyClientsAdapter.ViewHolder> {
    FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    public List<UserSparse> userSparseList;
    private Context context;
    private FirebaseStorage mStorage = FirebaseStorage.getInstance();


    public MyClientsAdapter(List<UserSparse> userSparseList){
        this.userSparseList = userSparseList;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( parent.getContext()).inflate(R.layout.activity_list_users, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.nameText.setText(userSparseList.get(position).getFullName());

        final UserSparse currentUserSparse = userSparseList.get(position);
        final String uid = currentUserSparse.getUid();


        holder.elementContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                mFirebaseFirestore.collection("zaposlenik").document(mFirebaseAuth.getCurrentUser().getUid()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        String idRole = documentSnapshot.getString("id_ovlasti");

                        Intent intToProfile = new Intent(context, ProfileOfEmployeesUserActivity.class);
                        intToProfile.putExtra("USER_UID", uid);
                        intToProfile.putExtra("ROLE_ID", idRole);
                        context.startActivity(intToProfile);
                    }
                });

            }
        });

        final String filepath = "profilePictures/" + uid + ".jpg";
        mStorage.getReference().child(filepath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                StorageReference mImageRef = mStorage.getReferenceFromUrl(uri.toString());
                GlideApp.with(context)
                        .load(mImageRef)
                        .into(holder.profileImg);
                holder.loadingContainer.setVisibility(View.GONE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                StorageReference mImageRef = mStorage.getReference("profilePictures/default.jpg");
                GlideApp.with(context)
                        .load(mImageRef)
                        .into(holder.profileImg);
                holder.loadingContainer.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userSparseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View mView;
        public TextView nameText;
        public RelativeLayout elementContainer;
        public ImageView profileImg;
        public ProgressBar loadingContainer;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            profileImg = mView.findViewById(R.id.profilePicture);
            loadingContainer = (ProgressBar) mView.findViewById(R.id.progressBar);
            nameText = (TextView) mView.findViewById(R.id.tvUserName);
            elementContainer = mView.findViewById(R.id.userElementContainer);
        }


    }
}
