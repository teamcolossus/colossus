package com.colossus.poliklinikazarehabilitaciju;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.*;


public class FoodAddActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private static final String TAG = "FoodAddActivity";
    FirebaseAuth mFirebaseAuth;
    FirebaseFirestore mFireStore;
    TextView tvNazivProizvoda,
            tvBarkod,
            tvKalorije,
            tvMasnoca,
            tvZasiceneMasneKiseline,
            tvUgljikohidrati,
            tvSeceri,
            tvBjelancevine,
            tvProizvodac,
            tvSol;
    ImageButton barcodeButton;
    Spinner kategorijaProizvodaSpinner;
    ArrayAdapter<String> adapter;
    ArrayList<String> sveKategorijeProizvoda = new ArrayList<>();
    ArrayList<String> sveKategorijeProizvodaId = new ArrayList<>();
    private String BARCODE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_add);

        BARCODE = (String) getIntent().getStringExtra("BARCODE");

        mFireStore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();

        sveKategorijeProizvoda.add("-");
        sveKategorijeProizvodaId.add("-");

        dohvatiKategorije();
        setTitle("Dodaj hranu");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        tvProizvodac = findViewById(R.id.nazivProizvodaca);
        tvNazivProizvoda = findViewById(R.id.nazivProizvoda);
        tvBarkod = findViewById(R.id.barkod);
        tvKalorije = findViewById(R.id.kalorije);
        tvMasnoca = findViewById(R.id.masnoca);
        tvZasiceneMasneKiseline = findViewById(R.id.zasiceneMasneKiseline);
        tvUgljikohidrati = findViewById(R.id.ugljikohidrati);
        tvSeceri = findViewById(R.id.seceri);
        tvBjelancevine = findViewById(R.id.bjelancevine);
        tvSol = findViewById(R.id.sol);


        kategorijaProizvodaSpinner = findViewById(R.id.kategorijaProizvoda);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, sveKategorijeProizvoda);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kategorijaProizvodaSpinner.setAdapter(adapter);
        kategorijaProizvodaSpinner.setOnItemSelectedListener(this);

        barcodeButton = findViewById(R.id.barcodeImageButton);

        barcodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToBarcode = new Intent(getApplicationContext(), BarcodeScannerActivity.class);
                startActivityForResult(intToBarcode, BarcodeScannerActivity.REQ_SCANNER);
            }
        });

        if (BARCODE != null) {
            tvBarkod.setText(BARCODE);
            tvBarkod.setError(null);
            tvBarkod.setEnabled(false);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sveKategorijeProizvoda.clear();
        adapter.notifyDataSetChanged(); /* Inform the adapter  we've changed items, which should force a refresh */
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    public void dohvatiKategorije() {
        mFireStore.collection("kategorija")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                String imeKategorije = document.getData().get("naziv").toString();
                                String idKAtegorije = document.getData().get("id").toString();
                                sveKategorijeProizvoda.add(imeKategorije);
                                sveKategorijeProizvodaId.add(idKAtegorije);
                                Log.d(TAG, document.getId() + " => " + document.getData());
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BarcodeScannerActivity.REQ_SCANNER) {
            if (resultCode == RESULT_OK) {
                final String result = data.getStringExtra("BARCODE");
                tvBarkod.setText(result);
                tvBarkod.setEnabled(false);
                tvBarkod.setError(null);

                checkExists(result);
            }
        }

    }

    private void checkExists(final String result) {
        mFireStore.collection("hrana")
                .document(result)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            AlertDialog alertDialog = new AlertDialog.Builder(FoodAddActivity.this).create();
                            alertDialog.setTitle("Proizvod već postoji");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            mFireStore.collection("zaposlenik").document(mFirebaseAuth.getCurrentUser().getUid()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                @Override
                                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                    if (documentSnapshot.exists()) {
                                                        Employee currentEmployee = documentSnapshot.toObject(Employee.class);

                                                        if (currentEmployee.getId_ovlasti().equals("0")) {
                                                            Intent intent = new Intent(getApplicationContext(), InsertProductActivity.class);
                                                            intent.putExtra("BARCODE", result);
                                                            startActivity(intent);
                                                        }
                                                    }
                                                }
                                            });
                                            finish();
                                        }
                                    });
                            alertDialog.show();
                        }
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        if (item.getItemId() == R.id.action_menu_done) {
            String proizvodac = tvProizvodac.getText().toString(),
                    nazivProizvoda = tvNazivProizvoda.getText().toString(),
                    kategorija = kategorijaProizvodaSpinner.getSelectedItem().toString(),
                    kalorije = tvKalorije.getText().toString(),
                    masnoca = tvMasnoca.getText().toString(),
                    zasiceneMasneKiseline = tvZasiceneMasneKiseline.getText().toString(),
                    ugljikohidrati = tvUgljikohidrati.getText().toString(),
                    seceri = tvSeceri.getText().toString(),
                    bjelancevine = tvBjelancevine.getText().toString(),
                    sol = tvSol.getText().toString(),
                    barkod = tvBarkod.getText().toString();


            boolean svePopunjeno = true;
            if (proizvodac.isEmpty()) {
                svePopunjeno = false;
                tvProizvodac.setError("Unesi naziv proizvođača");
                tvProizvodac.requestFocus();
            }
            if (nazivProizvoda.isEmpty()) {
                svePopunjeno = false;
                tvNazivProizvoda.requestFocus();
                tvNazivProizvoda.setError("Unesi naziv proizvoda");
            }
            if (barkod.isEmpty()) {
                svePopunjeno = false;
                tvBarkod.requestFocus();
                tvBarkod.setError("Unesi barkod");
            } else {
                checkExists(barkod);
            }
            if (kategorija == "-") {
                svePopunjeno = false;
                ((TextView) kategorijaProizvodaSpinner.getSelectedView()).setError("Izaberite kategoriju");
            }
            if (kalorije.isEmpty()) {
                svePopunjeno = false;
                tvKalorije.requestFocus();
                tvKalorije.setError("Unesi količinu kalorija");
            }
            if (masnoca.isEmpty()) {
                svePopunjeno = false;
                tvMasnoca.requestFocus();
                tvMasnoca.setError("Unesi količinu masnoca");

            }
            if (zasiceneMasneKiseline.isEmpty()) {
                svePopunjeno = false;
                tvZasiceneMasneKiseline.requestFocus();
                tvZasiceneMasneKiseline.setError("Unesi količinu zasićenih masnih kiselina");
            }
            if (ugljikohidrati.isEmpty()) {
                svePopunjeno = false;
                tvUgljikohidrati.requestFocus();
                tvUgljikohidrati.setError("Unesi količinu ugljikohidrata");
            }
            if (seceri.isEmpty()) {
                svePopunjeno = false;
                tvSeceri.requestFocus();
                tvSeceri.setError("Unesi količinu šećera");
            }
            if (bjelancevine.isEmpty()) {
                svePopunjeno = false;
                tvBjelancevine.requestFocus();
                tvBjelancevine.setError("Unesi količinu bjelančevina");
            }
            if (sol.isEmpty()) {
                svePopunjeno = false;
                tvSol.requestFocus();
                tvSol.setError("Unesi količinu soli");

            }


            if (svePopunjeno) {
                String categoryId = sveKategorijeProizvodaId.get(kategorijaProizvodaSpinner.getSelectedItemPosition());
                CollectionReference hranaKolekcija = mFireStore.collection("hrana");

                Map<String, Object> podaciHrane = new HashMap<>();
                podaciHrane.put("barkod", barkod);
                podaciHrane.put("proizvodac", proizvodac);
                podaciHrane.put("bjelancevine", Float.parseFloat(bjelancevine));
                podaciHrane.put("kategorija", categoryId);
                podaciHrane.put("energija", Float.parseFloat(kalorije));
                podaciHrane.put("masnoca", Float.parseFloat(masnoca));
                podaciHrane.put("naziv", nazivProizvoda);
                podaciHrane.put("seceri", Float.parseFloat(seceri));
                podaciHrane.put("sol", Float.parseFloat(sol));
                podaciHrane.put("ugljikohidrati", Float.parseFloat(ugljikohidrati));
                podaciHrane.put("zas_masne_kiseline", Float.parseFloat(zasiceneMasneKiseline));
                podaciHrane.put("is_deleted", false);

                hranaKolekcija.document(barkod).set(podaciHrane);
                Toast.makeText(FoodAddActivity.this, "Uspjesno dodano", Toast.LENGTH_SHORT).show();
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
