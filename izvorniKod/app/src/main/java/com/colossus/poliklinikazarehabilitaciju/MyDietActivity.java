package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MyDietActivity extends AppCompatActivity {
    FirebaseFirestore mFirestore;
    FirebaseAuth mFirebaseAuth;
    String currentUid, roleValue;

    private List<DualString> ingridientList;
    private List<Category> categoryList;
    private List<Product> foodList;
    private DualStringAdapter ingridientAdapter;
    private CategoryAdapter categoryAdapter;
    private FoodAdapter foodAdapter;
    private RecyclerView mCategory, mFood, mIngridients;
    private TextView tvCalories, tvProtein, tvCarbs, tvComment, tvFat;
    private RelativeLayout loadingContainer;
    private Menu optionsMenu;
    private Diet diet;
    public final static int REQ_CODE_EDIT = 1101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_diet);
        currentUid = (String) getIntent().getSerializableExtra("USER_UID");
        roleValue = (String) getIntent().getSerializableExtra("ROLE_ID");

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
        tvCalories = findViewById(R.id.tvCal);
        tvProtein = findViewById(R.id.tvProtein);
        tvCarbs = findViewById(R.id.tvCarbs);
        tvComment = findViewById(R.id.dietComment);
        tvFat = findViewById(R.id.tvFat);
        loadingContainer = findViewById(R.id.loadingContainer);

        // toolbar
        setTitle("Dijeta");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }



        initializeDiet();

    }

    private void initializeDiet() {
        categoryList = new ArrayList<>();
        foodList= new ArrayList<>();
        categoryAdapter = new CategoryAdapter(categoryList);
        foodAdapter = new FoodAdapter(foodList);

        mCategory = (RecyclerView) findViewById(R.id.categoryList);
        mCategory.setHasFixedSize(true);
        mCategory.setLayoutManager(new LinearLayoutManager(this));
        mCategory.setNestedScrollingEnabled(false);
        mCategory.setAdapter(categoryAdapter);

        mFood = (RecyclerView) findViewById(R.id.foodList);
        mFood.setHasFixedSize(true);
        mFood.setLayoutManager(new LinearLayoutManager(this));
        mFood.setNestedScrollingEnabled(false);
        mFood.setAdapter(foodAdapter);

        mIngridients = (RecyclerView) findViewById(R.id.ingridientList);
        mIngridients.setHasFixedSize(true);
        mIngridients.setLayoutManager(new LinearLayoutManager(this));
        mIngridients.setNestedScrollingEnabled(false);

        mFirestore.collection("dijeta")
                .document(currentUid)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            diet = documentSnapshot.toObject(Diet.class);

                            mFirestore.collection("dijeta").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                @Override
                                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                    if (e != null) {
                                        Log.d("DIJETA", "Error : " + e.getMessage());
                                        return;
                                    }

                                    for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                                        if (doc.getType() == DocumentChange.Type.MODIFIED || doc.getType() == DocumentChange.Type.ADDED ) {
                                            String uid = doc.getDocument().getId();

                                            if(uid.equals(currentUid)){
                                                ingridientList = new ArrayList<>();
                                                ingridientAdapter = new DualStringAdapter(ingridientList);
                                                mIngridients.setAdapter(ingridientAdapter);

                                                String comment = (String) doc.getDocument().getData().get("opis");
                                                String fat = (String) doc.getDocument().getData().get("masti");
                                                String protein = (String) doc.getDocument().getData().get("proteini");
                                                String carbs = (String) doc.getDocument().getData().get("ugljikohidrati");
                                                String cal = (String) doc.getDocument().getData().get("kalorija");
                                                String noSalt = (String) doc.getDocument().getData().get("noSol");
                                                String noSaturated = (String) doc.getDocument().getData().get("noZasicene");
                                                String noSugar = (String) doc.getDocument().getData().get("noSecer");
                                                String noFat = (String) doc.getDocument().getData().get("noMasti");
                                                String noProtein = (String) doc.getDocument().getData().get("noProteini");
                                                String noCarbs = (String) doc.getDocument().getData().get("noUgljikohidrati");

                                                diet = new Diet(comment, cal, fat, protein, noSugar, noSalt, noSaturated, carbs, noFat, noProtein, noCarbs);

                                                tvCalories.setText(cal + " kCal");
                                                tvProtein.setText(protein + "%");
                                                tvCarbs.setText(carbs + "%");
                                                tvFat.setText(fat + "%");
                                                tvComment.setText(comment);

                                                if (!noSalt.isEmpty()) {
                                                    ingridientList.add(new DualString("Sol", noSalt + " g"));
                                                }
                                                if (!noSaturated.isEmpty()) {
                                                    ingridientList.add(new DualString("Zasićene masne kiseline", noSaturated + " g"));
                                                }
                                                if (!noSugar.isEmpty()) {
                                                    ingridientList.add(new DualString("Šećer", noSugar + " g"));
                                                }
                                                if (!noFat.isEmpty()) {
                                                    ingridientList.add(new DualString("Masti", noFat + " g"));
                                                }
                                                if (!noProtein.isEmpty()) {
                                                    ingridientList.add(new DualString("Proteini", noProtein + " g"));
                                                }
                                                if (!noCarbs.isEmpty()) {
                                                    ingridientList.add(new DualString("Ugljikohidrati", noCarbs + " g"));
                                                }

                                                ingridientAdapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            });

                            mFirestore.collection("dijeta").document(currentUid).collection("kategorija").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                @Override
                                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                    if (e != null) {
                                        Log.d("DIJETA", "Error : " + e.getMessage());
                                        return;
                                    }

                                    for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                                        if (doc.getType() == DocumentChange.Type.ADDED) {
                                            String uid = doc.getDocument().getId();

                                            Category currentCat = doc.getDocument().toObject(Category.class).withUid(uid);
                                            categoryList.add(currentCat);
                                            categoryAdapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            });

                            mFirestore.collection("dijeta").document(currentUid).collection("hrana").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                @Override
                                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                    if (e != null) {
                                        Log.d("DIJETA", "Error : " + e.getMessage());
                                        return;
                                    }

                                    for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                                        if (doc.getType() == DocumentChange.Type.ADDED) {
                                            String uid = doc.getDocument().getId();

                                            Product currentProd = doc.getDocument().toObject(Product.class).withUid(uid);
                                            foodList.add(currentProd);
                                            foodAdapter.notifyDataSetChanged();

                                        }
                                    }
                                    loadingContainer.setVisibility(View.GONE);
                                }
                            });

                            if (roleValue != null) {
                                getMenuInflater().inflate(R.menu.menu_edit, optionsMenu);
                            }


                        } else {
                            if (roleValue == null) {
                                AlertDialog alertDialog = new AlertDialog.Builder(MyDietActivity.this).create();
                                alertDialog.setTitle("Dijeta ne postoji");
                                alertDialog.setMessage("Doktor još nije postavio dijetu");
                                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                finish();
                                            }
                                        });
                                alertDialog.show();
                            } else {
                                AlertDialog alertDialog = new AlertDialog.Builder(MyDietActivity.this).create();
                                alertDialog.setTitle("Dijeta ne postoji");
                                alertDialog.setMessage("Potrebno je postaviti dijetu");
                                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intToEdit = new Intent(MyDietActivity.this, EditDiet.class);
                                                intToEdit.putExtra("NEW", true);
                                                intToEdit.putExtra("USER_UID", currentUid);
                                                intToEdit.putExtra("ROLE_ID", roleValue);
                                                startActivityForResult(intToEdit, REQ_CODE_EDIT);
                                            }
                                        });
                                alertDialog.show();
                            }

                        }

                    }
                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        optionsMenu = menu;
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        if (item.getItemId() == R.id.action_edit_diet) {
            Intent intToEdit = new Intent(MyDietActivity.this, EditDiet.class);
            intToEdit.putExtra("NEW", false);
            intToEdit.putExtra("DIET", diet);
            intToEdit.putExtra("CAT_PREV", (Serializable) categoryList);
            intToEdit.putExtra("FOOD_PREV", (Serializable) foodList);
            intToEdit.putExtra("USER_UID", currentUid);
            intToEdit.putExtra("ROLE_ID", roleValue);

            startActivityForResult(intToEdit, REQ_CODE_EDIT);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE_EDIT) {
            if (resultCode == RESULT_OK) {
                finish();
                startActivity(getIntent());
            }
        }
    }
}
