package com.colossus.poliklinikazarehabilitaciju;

public class Training extends UserData {
    private String id_vjezba;
    private String intenzitet;
    private Integer trajanje;
    private Boolean odradeno;

    public Training(){
        super();
    }

    public Training(String id_vjezba, String intenzitet, Integer trajanje, Boolean odradeno) {
        this.id_vjezba = id_vjezba;
        this.intenzitet = intenzitet;
        this.trajanje = trajanje;
        this.odradeno = odradeno;
    }

    public String getId_vjezba() {
        return id_vjezba;
    }

    public void setId_vjezba(String id_vjezba) {
        this.id_vjezba = id_vjezba;
    }

    public String getIntenzitet() {
        return intenzitet;
    }

    public void setIntenzitet(String intenzitet) {
        this.intenzitet = intenzitet;
    }

    public Integer getTrajanje() {
        return trajanje;
    }

    public void setTrajanje(Integer trajanje) {
        this.trajanje = trajanje;
    }

    public Boolean getOdradeno() {
        return odradeno;
    }

    public void setOdradeno(Boolean odradeno) {
        this.odradeno = odradeno;
    }

    @Override
    public String toString() {
        return "Training{" +
                "uid='" + uid + '\'' +
                ", intenzitet=" + intenzitet +
                ", trajanje=" + trajanje +
                ", odradeno=" + odradeno +
                '}';
    }
}
