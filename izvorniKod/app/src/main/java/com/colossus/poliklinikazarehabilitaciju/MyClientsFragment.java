package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class MyClientsFragment extends Fragment {

    private static final String TAG = "FireLog";
    private RecyclerView mMainList;
    private FirebaseFirestore mFirestore;
    private FirebaseAuth mFirebaseAuth;
    private MyClientsAdapter myClientsAdapter;
    private List<UserSparse> userSparseList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_clients, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mMainList = (RecyclerView) getView().findViewById(R.id.myClientsList);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Activity mainActivity = getActivity();
        mainActivity.setTitle("Moji klijenti");
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        mainActivity.setTitle("");

        mMainList.setHasFixedSize(true);
        mMainList.setLayoutManager(new LinearLayoutManager(mainActivity));

        userSparseList = new ArrayList<>();
        myClientsAdapter = new MyClientsAdapter(userSparseList);
        mMainList.setAdapter(myClientsAdapter);

        mFirestore.collection("zaposlenik")
                .document(mFirebaseAuth.getCurrentUser().getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            Integer userType = Integer.parseInt(documentSnapshot.getString("id_ovlasti"));

                            if (userType == 2){
                                mFirestore.collection("doktor").document(mFirebaseAuth.getCurrentUser().getUid()).collection("klijenti").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                    @Override
                                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                        if (e != null) {
                                            Log.d(TAG, "Error : " + e.getMessage());
                                            return;
                                        }
                                        for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                                            if (doc.getType() == DocumentChange.Type.ADDED) {
                                                String uid = doc.getDocument().getId();

                                                UserSparse userSparse = doc.getDocument().toObject(UserSparse.class).withUid(uid);

                                                userSparseList.add(userSparse);
                                                myClientsAdapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                });
                            }else if (userType == 3){
                                mFirestore.collection("trener").document(mFirebaseAuth.getCurrentUser().getUid()).collection("klijenti").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                    @Override
                                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                        if (e != null) {
                                            Log.d(TAG, "Error : " + e.getMessage());
                                            return;
                                        }
                                        for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                                            if (doc.getType() == DocumentChange.Type.ADDED) {
                                                String uid = doc.getDocument().getId();

                                                UserSparse userSparse = doc.getDocument().toObject(UserSparse.class).withUid(uid);

                                                userSparseList.add(userSparse);
                                                myClientsAdapter.notifyDataSetChanged();

                                            }
                                        }
                                    }
                                });
                            }


                        }
                    }
                });




    }

}
