package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class MyProfileEmployeeFragment extends Fragment {
    FirebaseFirestore mFirestore;
    FirebaseAuth mFirebaseAuth;
    FirebaseStorage mStorage;
    //private Button endRelation, leaveReview;
    private Button editProfile;
    private TextView tvName, tvJob, tvScore, tvMail, tvReviewHeader;
    private ImageView profileImg, emailImage;
    private Activity mainActivity;
    private RelativeLayout loadingContainer;
    private Employee currentEmployee;
    private List<Review> reviewList;
    private ReviewAdapter reviewAdapter;
    private RecyclerView mMainList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_profile_employee, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        //endRelation= getView().findViewById(R.id.btnEndRelation);
        //leaveReview= getView().findViewById(R.id.btnLeaveReview);
        tvName= getView().findViewById(R.id.profileName);
        tvJob= getView().findViewById(R.id.profileJob);
        tvScore= getView().findViewById(R.id.profileScore);
        tvMail= getView().findViewById(R.id.emailProfileLabel);
        tvReviewHeader= getView().findViewById(R.id.reviewLabelHeader);
        profileImg= getView().findViewById(R.id.profilePicture);
        emailImage= getView().findViewById(R.id.emailLogo);
        loadingContainer= getView().findViewById(R.id.loadingContainer);
        editProfile= getView().findViewById(R.id.btnEditProfile);
        mMainList = (RecyclerView) getView().findViewById(R.id.reviewsList);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = getActivity();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
        mStorage = FirebaseStorage.getInstance();

        mainActivity.setTitle("");

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToEdit = new Intent(mainActivity, EditProfileEmployee.class);
                intToEdit.putExtra("EMPLOYEE", currentEmployee);
                startActivity(intToEdit);
            }
        });

        initializeProfile();

        mFirestore.collection("zaposlenik").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d("MAIN", "Error : " + e.getMessage());
                    return;
                }

                for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                    String uid = doc.getDocument().getId();

                    if (doc.getType() == DocumentChange.Type.MODIFIED) {
                        Employee employee = doc.getDocument().toObject(Employee.class).withUid(uid);

                        if(employee.getUid().equals(uid)){
                            loadingContainer.setVisibility(View.VISIBLE);
                            initializeProfile();
                        }
                    }
                }
            }
        });
    }

    private  void initializeReviews(){
        reviewList = new ArrayList<>();
        reviewAdapter = new ReviewAdapter(reviewList);

        mMainList.setHasFixedSize(true);
        mMainList.setLayoutManager(new LinearLayoutManager(mainActivity));
        mMainList.setNestedScrollingEnabled(false);
        mMainList.setAdapter(reviewAdapter);

        mFirestore.collection("recenzija").document(currentEmployee.getUid()).collection("recenzija").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d("PROFIL", "Error : " + e.getMessage());
                    return;
                }

                for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                    if (doc.getType() == DocumentChange.Type.ADDED) {
                        String uid = doc.getDocument().getId();

                        Review review = doc.getDocument().toObject(Review.class);

                        reviewList.add(review);
                        reviewAdapter.notifyDataSetChanged();

                    }
                }
            }
        });

    }

    private void initializeProfile() {
        mFirestore.collection("zaposlenik")
                .document(mFirebaseAuth.getCurrentUser().getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            String uid = mFirebaseAuth.getCurrentUser().getUid();
                            currentEmployee = documentSnapshot.toObject(Employee.class).withUid(uid);
                            mainActivity.setTitle(currentEmployee.getKorisnicko_ime());
                            tvName.setText(currentEmployee.getFullName());
                            //tvUsername.setText("@" + username);
                            tvJob.setText(currentEmployee.getId_ovlasti().equals("2") ? "Doktor" : "Trener");
                            tvMail.setText(currentEmployee.getEmail());

                            double scoreValue =(Math.round(((float) currentEmployee.getZbroj_ocjena() / currentEmployee.getBroj_recenzija())*100)) /100.0;
                            tvScore.setText("Ocjena: "+Double.toString(scoreValue));

                            initializeReviews();

                            emailImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                                    emailIntent.setType("plain/text");
                                    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{currentEmployee.getEmail()});
                                    mainActivity.startActivity(Intent.createChooser(emailIntent, "Pošalji mail koristeći:"));
                                }
                            });

                            final String filepath = "profilePictures/"+uid+".jpg";
                            mStorage.getReference().child(filepath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    StorageReference mImageRef = mStorage.getReferenceFromUrl(uri.toString());
                                    GlideApp.with(mainActivity)
                                            .load(mImageRef)
                                            .into(profileImg);
                                    loadingContainer.setVisibility(View.GONE);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    StorageReference mImageRef = mStorage.getReference("profilePictures/default.jpg");
                                    GlideApp.with(mainActivity)
                                            .load(mImageRef)
                                            .into(profileImg);
                                    loadingContainer.setVisibility(View.GONE);
                                }
                            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                }
                            });
                        }
                    }
                });
    }
}
