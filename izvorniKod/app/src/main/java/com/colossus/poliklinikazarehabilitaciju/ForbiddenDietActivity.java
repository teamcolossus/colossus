package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ForbiddenDietActivity extends AppCompatActivity {
    private RelativeLayout loadingContainer;
    private RecyclerView mMainList;
    private FirebaseFirestore mFirestore;
    private FirebaseAuth mFirebaseAuth;
    private CategoryAdapterSelectable categoryAdapterSelectable;
    private FoodAdapterSelectable foodAdapterSelectable;
    private List<Category> categoryList, oldCategory;
    private List<Product> foodList, oldFood;
    private Boolean CATEGORY, FOOD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forbidden_diet);
        mFirestore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();

        CATEGORY = (Boolean) getIntent().getSerializableExtra("CATEGORY");
        FOOD = (Boolean) getIntent().getSerializableExtra("FOOD");

        //toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        loadingContainer = findViewById(R.id.loadingContainer);


        initializeLists();
    }

    private void finishing() {
        Intent intent = new Intent();
        if (FOOD) {
            intent.putExtra("food", (Serializable) foodAdapterSelectable.getReturnList());
            setResult(2, intent);
        } else if (CATEGORY) {
            intent.putExtra("categories", (Serializable) categoryAdapterSelectable.getReturnList());
            setResult(1, intent);
        }

        finish();
    }

    private void initializeLists() {
        if (CATEGORY) {
            oldCategory = (List<Category>) getIntent().getSerializableExtra("PREV_CAT");

            setTitle("Odaberi kategoriju");
            categoryList = new ArrayList<>();
            categoryAdapterSelectable = new CategoryAdapterSelectable(categoryList, oldCategory);

            mMainList = (RecyclerView) findViewById(R.id.rvList);
            mMainList.setHasFixedSize(true);
            mMainList.setLayoutManager(new LinearLayoutManager(this));
            mMainList.setAdapter(categoryAdapterSelectable);

            mFirestore.collection("kategorija").addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                    if (e != null) {
                        Log.d("FORBIDDEN", "Error : " + e.getMessage());
                        return;
                    }

                    for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                        if (doc.getType() == DocumentChange.Type.ADDED) {
                            String uid = doc.getDocument().getId();

                            Category currentCategory = doc.getDocument().toObject(Category.class).withUid(uid);
                            if (currentCategory.getIs_deleted()){
                                continue;
                            }
                            categoryList.add(currentCategory);
                            categoryAdapterSelectable.notifyDataSetChanged();

                        }
                    }
                    loadingContainer.setVisibility(View.GONE);
                }
            });
        } else if (FOOD) {
            oldFood = (List<Product>) getIntent().getSerializableExtra("PREV_FOOD");

            setTitle("Odaberi kategoriju");
            foodList = new ArrayList<>();
            foodAdapterSelectable = new FoodAdapterSelectable(foodList, oldFood);

            mMainList = (RecyclerView) findViewById(R.id.rvList);
            mMainList.setHasFixedSize(true);
            mMainList.setLayoutManager(new LinearLayoutManager(this));
            mMainList.setAdapter(foodAdapterSelectable);

            mFirestore.collection("hrana").addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                    if (e != null) {
                        Log.d("FORBIDDEN", "Error : " + e.getMessage());
                        return;
                    }

                    for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                        if (doc.getType() == DocumentChange.Type.ADDED) {
                            String uid = doc.getDocument().getId();

                            Product currentCategory = doc.getDocument().toObject(Product.class).withUid(uid);

                            if (currentCategory.getIs_deleted()){
                                continue;
                            }

                            foodList.add(currentCategory);
                            foodAdapterSelectable.notifyDataSetChanged();

                        }
                    }
                    loadingContainer.setVisibility(View.GONE);
                }
            });
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        if (item.getItemId() == R.id.action_menu_done) {
            finishing();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }
}
