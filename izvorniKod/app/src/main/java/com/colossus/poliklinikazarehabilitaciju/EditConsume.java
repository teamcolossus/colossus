package com.colossus.poliklinikazarehabilitaciju;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class EditConsume extends AppCompatActivity {
    private String BARCODE, CATEGORY;
    private FirebaseFirestore mFirestore;
    private FirebaseAuth mFirebaseAuth;
    private EditText etMass;
    private TextView tvCarbs, tvFat, tvProt, tvCal, tvName, tvButton;
    private RelativeLayout loadingContainer, loadingContainerWarning, colapse;
    private Float massInput, cal, carb, prot, fat, salt, saturated, sugar;
    private float calR, carbR, protR, fatR, saltR, saturatedR, sugarR;
    private boolean valid = true, handlingWarnings = false, isExpended = false, handlingIngridients = false;
    private List<DualString> warning, ingridients;
    private DualStringAdapter ingridientsAdapter;
    private WarningAdapter warningAdapter;
    private RecyclerView rvWarnings, details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_consume);

        BARCODE = (String) getIntent().getStringExtra("BARCODE");

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        // toolbar
        setTitle("Uredi izbor");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        tvCal = findViewById(R.id.tvCal);
        tvProt = findViewById(R.id.tvProtein);
        tvCarbs = findViewById(R.id.tvCarbs);
        tvFat = findViewById(R.id.tvFat);
        etMass = findViewById(R.id.etMass);
        tvName = findViewById(R.id.tvProductName);
        loadingContainer = findViewById(R.id.loadingContainer);
        loadingContainerWarning = findViewById(R.id.loadingContainerWarning);
        colapse = findViewById(R.id.colapse);
        rvWarnings = findViewById(R.id.dietWarinings);
        details = findViewById(R.id.rvIngridients);
        tvButton = findViewById(R.id.tvShowHide);

        initializeEdit();

        etMass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Nothing to replace
                if (s.length() == 0)
                    return;

                reset();
                checkDiet();
            }
        });
        final Animation aniSlideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        final Animation aniSlideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        aniSlideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                details.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        aniSlideDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                details.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        colapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isExpended) {
                    details.startAnimation(aniSlideUp);
                    tvButton.setText("Prikaži nutritivne vrijednosti");
                } else {
                    details.startAnimation(aniSlideDown);
                    tvButton.setText("Sakrij nutritivne vrijednosti");
                }
                isExpended = !isExpended;

            }
        });
    }

    private void initializeEdit() {
        mFirestore.collection("hrana")
                .document(BARCODE)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            Product product = documentSnapshot.toObject(Product.class);
                            tvName.setText(product.getProizvodac() + " - " + product.getNaziv());

                            carb = product.getUgljikohidrati();
                            prot = product.getBjelancevine();
                            fat = product.getMasnoca();
                            cal = product.getEnergija();
                            salt = product.getSol();
                            saturated = product.getZas_masne_kiseline();
                            sugar = product.getSeceri();
                            loadingContainer.setVisibility(View.GONE);

                            resetIngridients();

                            CATEGORY = product.getKategorija();
                        } else {
                            Intent intToFoodAdd = new Intent(getApplicationContext(), FoodAddActivity.class);
                            intToFoodAdd.putExtra("BARCODE", BARCODE);
                            startActivity(intToFoodAdd);
                            finish();
                        }
                    }
                });
    }

    private void reset() {
        if (etMass.getText().toString() != null) {
            int mass = Integer.parseInt(etMass.getText().toString());

            carbR = (float) carb / 100 * mass;
            protR = (float) prot / 100 * mass;
            fatR = (float) fat / 100 * mass;
            calR = (float) cal / 100 * mass;

            tvCal.setText(String.format(Locale.US, "%d", Math.round(calR)));
            tvProt.setText(String.format(Locale.US, "%.1f", protR));
            tvCarbs.setText(String.format(Locale.US, "%.1f", carbR));
            tvFat.setText(String.format(Locale.US, "%.1f", fatR));
            resetIngridients();
        }
    }

    private void resetIngridients() {
        if (etMass.getText().toString() != null) {
            if (!handlingIngridients) {
                handlingIngridients = true;
                ingridients = new ArrayList<>();
                ingridientsAdapter = new DualStringAdapter(ingridients);

                details = findViewById(R.id.rvIngridients);
                details.setHasFixedSize(true);
                details.setLayoutManager(new LinearLayoutManager(this));
                details.setNestedScrollingEnabled(false);
                details.setAdapter(ingridientsAdapter);

                String massStr = etMass.getText().toString();
                int mass;
                if (massStr.isEmpty()) {
                    mass = 0;
                } else {
                    mass = Integer.parseInt(massStr);
                }

                carbR = (float) carb / 100 * mass;
                protR = (float) prot / 100 * mass;
                fatR = (float) fat / 100 * mass;
                saltR = (float) salt / 100 * mass;
                saturatedR = (float) saturated / 100 * mass;
                sugarR = (float) sugar / 100 * mass;
                calR = (float) cal / 100 * mass;

                ingridients.add(new DualString("Kalorija", String.format(Locale.US, "%d", Math.round(calR))));
                ingridients.add(new DualString("Masti", String.format(Locale.US, "%.1f", fatR)));
                ingridients.add(new DualString("Zasićene masne kiseline", String.format(Locale.US, "%.1f", saturatedR)));
                ingridients.add(new DualString("Ugljikohidrati", String.format(Locale.US, "%.1f", carbR)));
                ingridients.add(new DualString("Šećeri", String.format(Locale.US, "%.1f", sugarR)));
                ingridients.add(new DualString("Proteini", String.format(Locale.US, "%.1f", protR)));
                ingridients.add(new DualString("Sol", String.format(Locale.US, "%.1f", saltR)));
                ingridientsAdapter.notifyDataSetChanged();
                handlingIngridients = false;
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_menu_done:
                if (etMass.getText().toString().isEmpty()) {
                    etMass.requestFocus();
                    etMass.setError("Upiši masu obroka");
                    break;
                }

                SimpleDateFormat sdf1 = new SimpleDateFormat("ddMMyyyy", Locale.getDefault());
                SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss", Locale.getDefault());

                String date = sdf1.format(new Date());
                String time = sdf2.format(new Date());


                Map<String, Object> update = new HashMap<>();
                update.put("barkod", BARCODE);
                update.put("masa", Integer.parseInt(etMass.getText().toString()));

                mFirestore.collection("konzumiranaHrana")
                        .document(mFirebaseAuth.getCurrentUser().getUid())
                        .collection("datum")
                        .document(date)
                        .collection("hrana")
                        .document()
                        .set(update);

                setResult(RESULT_OK);
                finish();
                break;
        }
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    Log.d("focus", "touchevent");
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private void checkDiet() {
        if (!handlingWarnings) {
            handlingWarnings = true;

            loadingContainerWarning.setVisibility(View.VISIBLE);
            warning = new ArrayList<>();
            warningAdapter = new WarningAdapter(warning);

            final boolean[] food = {false};
            final boolean[] category = {false};

            rvWarnings = findViewById(R.id.dietWarinings);
            rvWarnings.setHasFixedSize(true);
            rvWarnings.setLayoutManager(new LinearLayoutManager(this));
            rvWarnings.setNestedScrollingEnabled(false);
            rvWarnings.setAdapter(warningAdapter);

            final String massStr = etMass.getText().toString();
            if (!massStr.isEmpty()) {
                mFirestore.collection("dijeta").document(mFirebaseAuth.getCurrentUser().getUid()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            int mass = Integer.parseInt(massStr);
                            handlingWarnings = false;

                            carbR = (float) carb / 100 * mass;
                            protR = (float) prot / 100 * mass;
                            fatR = (float) fat / 100 * mass;
                            saltR = (float) salt / 100 * mass;
                            saturatedR = (float) saturated / 100 * mass;
                            sugarR = (float) sugar / 100 * mass;
                            calR = (float) cal / 100 * mass;

                            Diet diet = documentSnapshot.toObject(Diet.class);

                            String noWarning = "Nedopuštena količina sastojaka!";
                            if (!diet.getNoProteini().isEmpty()) {
                                if (protR > Float.valueOf(diet.getNoProteini())) {
                                    warning.add(new DualString(noWarning, "Proteini"));
                                }
                            }
                            if (!diet.getNoUgljikohidrati().isEmpty()) {
                                if (carbR > Float.valueOf(diet.getNoUgljikohidrati())) {
                                    warning.add(new DualString(noWarning, "Ugljikohidrati"));
                                }
                            }
                            if (!diet.getNoMasti().isEmpty()) {
                                if (fatR > Float.valueOf(diet.getNoMasti())) {
                                    warning.add(new DualString(noWarning, "Masti"));
                                }
                            }
                            if (!diet.getNoSecer().isEmpty()) {
                                if (sugarR > Float.valueOf(diet.getNoSecer())) {
                                    warning.add(new DualString(noWarning, "Šećeri"));
                                }
                            }
                            if (!diet.getNoSol().isEmpty()) {
                                if (saltR > Float.valueOf(diet.getNoSol())) {
                                    warning.add(new DualString(noWarning, "Soli"));
                                }
                            }
                            if (!diet.getNoZasicene().isEmpty()) {
                                if (saturatedR > Float.valueOf(diet.getNoZasicene())) {
                                    warning.add(new DualString(noWarning, "Zasićene masne kiseline"));
                                }
                            }

                            mFirestore.collection("dijeta").document(mFirebaseAuth.getCurrentUser().getUid()).collection("hrana").document(BARCODE).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    if (documentSnapshot.exists()) {
                                        warning.add(new DualString("Zabranjena hrana!", documentSnapshot.getString("naziv")));
                                        warningAdapter.notifyDataSetChanged();
                                    }
                                    loadingContainerWarning.setVisibility(View.GONE);
                                    handlingWarnings = false;
                                }
                            });
                            mFirestore.collection("dijeta").document(mFirebaseAuth.getCurrentUser().getUid()).collection("kategorija").document(CATEGORY).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    if (documentSnapshot.exists()) {
                                        warning.add(new DualString("Zabranjena kategorija!", documentSnapshot.getString("naziv")));
                                        warningAdapter.notifyDataSetChanged();
                                    }
                                    loadingContainerWarning.setVisibility(View.GONE);
                                    handlingWarnings = false;
                                }
                            });
                            warningAdapter.notifyDataSetChanged();
                        }
                    }
                });
            }
        }
    }

}
