package com.colossus.poliklinikazarehabilitaciju;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;


public class ExerciseListActivity extends AppCompatActivity {

    private RecyclerView mMainList;
    private FirebaseFirestore mFirestore;
    private ExerciseListAdapter exerciseListAdapter;
    private List<Exercise> exerciseList;

    private String userUid; // za dodavanje vjezbe korisnikovom treningu
    private String date;// za dodavanje vjezbe korisnikovom treningu

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_list);

        userUid = (String)getIntent().getSerializableExtra("user_uid");
        date = (String)getIntent().getSerializableExtra("datum");

        mFirestore = FirebaseFirestore.getInstance();

        // toolbar
        setTitle("Vježbe");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        exerciseList = new ArrayList<>();

        mFirestore.collection("vjezbe").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (DocumentChange documentChange : queryDocumentSnapshots.getDocumentChanges()) {
                    QueryDocumentSnapshot qds = documentChange.getDocument();

                    String uid = qds.getId();

                    System.out.println(uid + " " + qds.getData());
                    if ((boolean) qds.get("is_deleted")) {
                        continue;
                    }
                    Exercise exercise = qds.toObject(Exercise.class).withUid(uid);
                    exerciseList.add(exercise);
                    exerciseListAdapter.notifyDataSetChanged();
                }
            }

        });

        mMainList = findViewById(R.id.exercise_list);
        mMainList.setHasFixedSize(true);
        mMainList.setLayoutManager(new LinearLayoutManager(this));
        exerciseListAdapter = new ExerciseListAdapter(exerciseList, userUid, date);
        mMainList.setAdapter(exerciseListAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        if (item.getItemId() == R.id.action_menu_add) {
            Intent insertExercise = new Intent(ExerciseListActivity.this, CreateNewWorkoutActivity.class);
            startActivity(insertExercise);
        }
        return super.

                onOptionsItemSelected(item);

    }
}
