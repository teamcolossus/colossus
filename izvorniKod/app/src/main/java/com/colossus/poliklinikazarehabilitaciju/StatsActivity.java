package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class StatsActivity extends AppCompatActivity {

    private FirebaseFirestore mFirestore;
    private FirebaseAuth mFirebaseAuth;
    private TextView tvSumFood, tvSumExcercise, tvDate, tvGoal, tvHeaderSumFood, tvHeaderSumEx, tvResult;
    private TextView tvGoalCarbs, tvGoalFats, tvGoalProtein, tvCurrentCarbs, tvCurrentProtein, tvCurrentFats;
    private RelativeLayout loadingContainer;
    private LinearLayout dateContainer;
    private RecyclerView mFood, mExcercise;
    private List<DiaryFood> diaryFoodList;
    private List<DiaryExercise> diaryExcerciseList;
    private FoodDiaryAdapter foodAdapter;
    private ExerciseDiaryAdapter excerciseAdapter;
    private String selectedDate, currentDate;
    public final static int REQ_CODE_EDIT = 1111;
    DatePickerDialog.OnDateSetListener mDateListener;
    private ListenerRegistration food, exercise;
    private float sumCarbs, sumFat, sumProt;
    private String USER_UID, sCal, sFat, sCarb, sProt;
    PieChart chart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);

        USER_UID = (String) getIntent().getSerializableExtra("USER_UID");

        mFirestore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();

        tvSumFood = findViewById(R.id.tvSumFood);
        tvSumExcercise = findViewById(R.id.tvSumExcercise);
        tvDate = findViewById(R.id.tvDate);
        dateContainer = findViewById(R.id.dateContainer);
        loadingContainer = findViewById(R.id.loadingContainer);
        tvGoal = findViewById(R.id.tvGoal);
        tvHeaderSumEx = findViewById(R.id.headerSumEx);
        tvHeaderSumFood = findViewById(R.id.headerSumFood);
        tvResult = findViewById(R.id.tvResult);
        tvCurrentCarbs = findViewById(R.id.currentCarbs);
        tvCurrentFats = findViewById(R.id.currentFat);
        tvCurrentProtein = findViewById(R.id.currentProtein);
        tvGoalCarbs = findViewById(R.id.goalCarbs);
        tvGoalFats = findViewById(R.id.goalFats);
        tvGoalProtein = findViewById(R.id.goalProtein);
        mFood = (RecyclerView) findViewById(R.id.rvFood);
        mExcercise = (RecyclerView) findViewById(R.id.rvExcercise);
        chart = findViewById(R.id.piechart);


        currentDate = new SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(new Date());
        selectedDate = currentDate;

        setTitle("Statistika");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        tvGoal.setText("0");
        tvResult.setText("0");
        tvHeaderSumEx.setText("0");
        tvHeaderSumFood.setText("0");

        tvCurrentCarbs.setText("0%");
        tvCurrentProtein.setText("0%");
        tvCurrentFats.setText("0%");

        tvGoalCarbs.setText("0%");
        tvGoalFats.setText("0%");
        tvGoalProtein.setText("0%");

        sCal = "0";
        sFat = "0";
        sCarb = "0";
        sProt = "0";

        sumCarbs = 0;
        sumFat = 0;
        sumProt = 0;

        mFirestore.collection("dijeta").document(USER_UID).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    Diet diet = documentSnapshot.toObject(Diet.class);

                    sCal = diet.getKalorija();
                    sFat = diet.getMasti();
                    sCarb = diet.getUgljikohidrati();
                    sProt = diet.getProteini();
                    tvGoalCarbs.setText(sCarb + "%");
                    tvGoalFats.setText(sFat + "%");
                    tvGoalProtein.setText(sProt + "%");

                    tvGoal.setText(sCal);
                    initializeSums();
                }
            }
        });

        initializeFood();
        initializeExcercise();
        initializeDatepicker();
        initializeSums();

    }

    private void initializeGraph() {
        List<PieEntry> pieEntries = new ArrayList<>();

        float sum = sumCarbs + sumFat + sumProt;
        float percCarbs = (float) sumCarbs / sum * 100;
        float percFat = (float) sumFat / sum * 100;
        float percProt = (float) sumProt / sum * 100;

        float perc[] = {percCarbs, percFat, percProt};
        String label[] = {"Ugljikohidrati", "Masti", "Proteini"};

        for (int i = 0; i < perc.length; i++) {
            pieEntries.add(new PieEntry(Math.round(perc[i]), label[i]));
        }

        PieDataSet dataSet = new PieDataSet(pieEntries, "Dnevni postotak nutritivnih vrijednosti");
        dataSet.setColors(new int[]{R.color.carbs, R.color.fat, R.color.protein}, getApplicationContext());
        PieData data = new PieData(dataSet);

        chart.setDrawEntryLabels(false);
        chart.getDescription().setEnabled(false);
        chart.getLegend().setEnabled(false);
        data.setValueTextSize(18f);
        chart.setHoleRadius(0f);

        data.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return String.valueOf((int) Math.floor(value)) + "%";
            }
        });

        chart.setData(data);
        chart.invalidate();


        tvCurrentCarbs.setText(Integer.toString(Math.round(percCarbs)) + "%");
        tvCurrentProtein.setText(Integer.toString(Math.round(percProt)) + "%");
        tvCurrentFats.setText(Integer.toString(Math.round(percFat)) + "%");
    }

    private void initializeSums() {
        int goal = Integer.parseInt(sCal);
        int plus = Integer.parseInt(tvHeaderSumEx.getText().toString());
        int minus = Integer.parseInt(tvHeaderSumFood.getText().toString());
        int result = goal - minus + plus;

        tvResult.setText(Integer.toString(result));
        if (result < 0) {
            tvResult.setTextColor(Color.parseColor("#CE0000"));
        } else {
            tvResult.setTextColor(Color.parseColor("#FFB600"));
        }
    }

    private void initializeFood() {
        tvSumFood.setText("0");
        sumFat = 0;
        sumProt = 0;
        sumCarbs = 0;
        tvCurrentCarbs.setText("0%");
        tvCurrentProtein.setText("0%");
        tvCurrentFats.setText("0%");
        tvHeaderSumFood.setText("0");
        if (food != null) {
            food.remove();
        }
        diaryFoodList = new ArrayList<>();
        foodAdapter = new FoodDiaryAdapter(diaryFoodList, USER_UID, selectedDate);
        final int[] sumFood = new int[1];

        mFood.setHasFixedSize(true);
        mFood.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mFood.setNestedScrollingEnabled(false);
        mFood.setAdapter(foodAdapter);

        food = mFirestore.collection("konzumiranaHrana")
                .document(USER_UID)
                .collection("datum")
                .document(selectedDate)
                .collection("hrana")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d("DNEVNIK", "Error : " + e.getMessage());
                            loadingContainer.setVisibility(View.GONE);
                            return;
                        }
                        for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                            if (doc.getType() == DocumentChange.Type.ADDED) {
                                String uid = doc.getDocument().getId();
                                final DiaryFood diaryFood = doc.getDocument().toObject(DiaryFood.class).withUid(uid);

                                diaryFoodList.add(diaryFood);
                                foodAdapter.notifyDataSetChanged();

                                mFirestore.collection("hrana").document(diaryFood.getBarkod()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        if (documentSnapshot.exists()) {
                                            Product currentProduct = documentSnapshot.toObject(Product.class);

                                            int caloriesValue = Math.round((float) currentProduct.getEnergija() / 100 * diaryFood.getMasa());
                                            int carbs = Math.round((float) currentProduct.getUgljikohidrati() / 100 * diaryFood.getMasa());
                                            int prots = Math.round((float) currentProduct.getBjelancevine() / 100 * diaryFood.getMasa());
                                            int fats = Math.round((float) currentProduct.getMasnoca() / 100 * diaryFood.getMasa());
                                            sumFood[0] += caloriesValue;
                                            sumCarbs += carbs;
                                            sumProt += prots;
                                            sumFat += fats;
                                            tvSumFood.setText(Integer.toString(sumFood[0]));
                                            tvHeaderSumFood.setText(Integer.toString(sumFood[0]));
                                            initializeSums();
                                            initializeGraph();
                                        }
                                    }
                                });
                            }
                        }
                        loadingContainer.setVisibility(View.GONE);
                    }
                });
    }

    private void initializeExcercise() {
        tvSumExcercise.setText("0");
        tvHeaderSumEx.setText("0");
        if (exercise != null) {
            exercise.remove();
        }
        final int[] sumEx = new int[1];

        diaryExcerciseList = new ArrayList<>();
        excerciseAdapter = new ExerciseDiaryAdapter(diaryExcerciseList);

        mExcercise.setHasFixedSize(true);
        mExcercise.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mExcercise.setNestedScrollingEnabled(false);
        mExcercise.setAdapter(excerciseAdapter);

        exercise = mFirestore.collection("trening")
                .document(USER_UID)
                .collection("datum")
                .document(selectedDate)
                .collection("trening")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d("DNEVNIK", "Error : " + e.getMessage());
                            loadingContainer.setVisibility(View.GONE);
                            return;
                        }
                        for (final DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                            if (doc.getType() == DocumentChange.Type.ADDED || doc.getType() == DocumentChange.Type.MODIFIED) {
                                final DiaryExercise diaryExercise = doc.getDocument().toObject(DiaryExercise.class);
                                if (diaryExercise.isOdradeno()) {
                                    diaryExcerciseList.add(diaryExercise);
                                    excerciseAdapter.notifyDataSetChanged();

                                    mFirestore.collection("vjezbe").document(diaryExercise.getId_vjezba()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                        @Override
                                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                                            if (documentSnapshot.exists()) {
                                                int caloriesValue = 0;
                                                String intensity = diaryExercise.getIntenzitet();
                                                int duration = diaryExercise.getTrajanje();
                                                Exercise currentExercise = documentSnapshot.toObject(Exercise.class);

                                                if (intensity.toLowerCase().equals("lagano")) {
                                                    caloriesValue = Math.round(((float) duration / 60) * currentExercise.getLagano());
                                                } else if (intensity.toLowerCase().equals("srednje")) {
                                                    caloriesValue = Math.round(((float) duration / 60) * currentExercise.getSrednje());
                                                } else if (intensity.toLowerCase().equals("tesko")) {
                                                    caloriesValue = Math.round(((float) duration / 60) * currentExercise.getTesko());
                                                }
                                                sumEx[0] += caloriesValue;
                                                tvSumExcercise.setText(Integer.toString(sumEx[0]));
                                                tvHeaderSumEx.setText(Integer.toString(sumEx[0]));
                                                initializeSums();
                                                initializeGraph();
                                            }
                                        }
                                    });
                                }
                            }
                        }
                        loadingContainer.setVisibility(View.GONE);
                    }
                });
    }


    private void initializeDatepicker() {
        dateContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog dialog = new DatePickerDialog(StatsActivity.this, mDateListener, year, month, day);
                cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
                cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
                cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
                cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
                dialog.getDatePicker().setMaxDate(cal.getTimeInMillis());
                dialog.show();
            }
        });

        mDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                loadingContainer.setVisibility(View.VISIBLE);

                selectedDate = String.format("%02d%02d%04d", day, month, year);
                String selectedDateText = String.format("%d.%d.%d.", day, month, year);
                tvDate.setText(selectedDateText);
//
//                addExcercise.setVisibility(View.GONE);
//                addFood.setVisibility(View.GONE);

                if (Integer.parseInt(selectedDate) == Integer.parseInt(currentDate)) {
                    tvDate.setText("Danas");
//                    addExcercise.setVisibility(View.VISIBLE);
//                    addFood.setVisibility(View.VISIBLE);
                } else if (Integer.parseInt(selectedDate) == Integer.parseInt(currentDate) - 1000000) {
                    tvDate.setText("Jučer");
                }

                initializeFood();
                initializeExcercise();
                initializeGraph();
                initializeSums();
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
