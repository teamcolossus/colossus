package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class DiaryFragment extends Fragment {

    private FirebaseFirestore mFirestore;
    private FirebaseAuth mFirebaseAuth;
    private TextView tvSumFood, tvSumExcercise, tvDate;
    private RelativeLayout addFood, addExcercise, loadingContainer;
    private LinearLayout dateContainer;
    private RecyclerView mFood, mExcercise;
    private Activity mainActivity;
    private List<DiaryFood> diaryFoodList;
    private List<DiaryExercise> diaryExcerciseList;
    private FoodDiaryAdapter foodAdapter;
    private ExerciseDiaryAdapter excerciseAdapter;
    private String selectedDate, currentDate;
    public final static int REQ_CODE_EDIT = 1111;
    DatePickerDialog.OnDateSetListener mDateListener;
    private ListenerRegistration food, exercise;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_diary, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        tvSumFood = getView().findViewById(R.id.tvSumFood);
        tvSumExcercise = getView().findViewById(R.id.tvSumExcercise);
        tvDate = getView().findViewById(R.id.tvDate);
        dateContainer = getView().findViewById(R.id.dateContainer);
        addFood = getView().findViewById(R.id.addFoodContainer);
        addExcercise = getView().findViewById(R.id.addExcerciseContainer);
        loadingContainer = getView().findViewById(R.id.loadingContainer);

        mFood = (RecyclerView) getView().findViewById(R.id.rvFood);
        mExcercise = (RecyclerView) getView().findViewById(R.id.rvExcercise);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = getActivity();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        mainActivity.setTitle("Dnevnik");

        currentDate = new SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(new Date());
        selectedDate = currentDate;

        initializeDiaryFood();
        initializeDiaryExcercise();
        initializeDatepicker();

        addFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToDiet = new Intent(mainActivity, ConsumeFoodActivity.class);
                startActivityForResult(intToDiet, REQ_CODE_EDIT);
            }
        });

        addExcercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mainActivity, UserTrainingActivity.class);
                intent.putExtra("userUid", mFirebaseAuth.getCurrentUser().getUid());
                intent.putExtra("ADD", true);
                startActivity(intent);
            }
        });


    }

    private void initializeDiaryFood() {
        tvSumFood.setText("0");
        if (food != null) {
            food.remove();
        }
        diaryFoodList = new ArrayList<>();
        foodAdapter = new FoodDiaryAdapter(diaryFoodList, mFirebaseAuth.getCurrentUser().getUid(), selectedDate);
        final int[] sumFood = new int[1];

        mFood.setHasFixedSize(true);
        mFood.setLayoutManager(new LinearLayoutManager(mainActivity));
        mFood.setNestedScrollingEnabled(false);
        mFood.setAdapter(foodAdapter);

        food = mFirestore.collection("konzumiranaHrana")
                .document(mFirebaseAuth.getCurrentUser().getUid())
                .collection("datum")
                .document(selectedDate)
                .collection("hrana")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d("DNEVNIK", "Error : " + e.getMessage());
                            loadingContainer.setVisibility(View.GONE);
                            return;
                        }
                        for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                            if (doc.getType() == DocumentChange.Type.ADDED) {
                                String uid = doc.getDocument().getId();
                                final DiaryFood diaryFood = doc.getDocument().toObject(DiaryFood.class).withUid(uid);

                                diaryFoodList.add(diaryFood);
                                foodAdapter.notifyDataSetChanged();

                                mFirestore.collection("hrana").document(diaryFood.getBarkod()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        if (documentSnapshot.exists()) {
                                            Product currentProduct = documentSnapshot.toObject(Product.class);

                                            int caloriesValue = Math.round((float) currentProduct.getEnergija() / 100 * diaryFood.getMasa());
                                            sumFood[0] += caloriesValue;
                                            tvSumFood.setText(Integer.toString(sumFood[0]));
                                        }
                                    }
                                });
                            }
                        }
                        loadingContainer.setVisibility(View.GONE);
                    }
                });
    }

    private void initializeDiaryExcercise() {
        tvSumExcercise.setText("0");
        if (exercise != null) {
            exercise.remove();
        }
        final int[] sumEx = new int[1];

        diaryExcerciseList = new ArrayList<>();
        excerciseAdapter = new ExerciseDiaryAdapter(diaryExcerciseList);

        mExcercise.setHasFixedSize(true);
        mExcercise.setLayoutManager(new LinearLayoutManager(mainActivity));
        mExcercise.setNestedScrollingEnabled(false);
        mExcercise.setAdapter(excerciseAdapter);

        exercise = mFirestore.collection("trening")
                .document(mFirebaseAuth.getCurrentUser().getUid())
                .collection("datum")
                .document(selectedDate)
                .collection("trening")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d("DNEVNIK", "Error : " + e.getMessage());
                            loadingContainer.setVisibility(View.GONE);
                            return;
                        }
                        for (final DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                            if (doc.getType() == DocumentChange.Type.ADDED || doc.getType() == DocumentChange.Type.MODIFIED) {
                                final DiaryExercise diaryExercise = doc.getDocument().toObject(DiaryExercise.class);

                                if (diaryExercise.isOdradeno()) {
                                    diaryExcerciseList.add(diaryExercise);
                                    excerciseAdapter.notifyDataSetChanged();

                                    mFirestore.collection("vjezbe").document(diaryExercise.getId_vjezba()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                        @Override
                                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                                            if (documentSnapshot.exists()) {
                                                int caloriesValue = 0;
                                                String intensity = diaryExercise.getIntenzitet();
                                                int duration = diaryExercise.getTrajanje();
                                                Exercise currentExercise = documentSnapshot.toObject(Exercise.class);

                                                if (intensity.toLowerCase().equals("lagano")) {
                                                    caloriesValue = Math.round((float) duration / 60 * currentExercise.getLagano());
                                                } else if (intensity.toLowerCase().equals("srednje")) {
                                                    caloriesValue = Math.round((float) duration / 60  * currentExercise.getSrednje());
                                                } else if (intensity.toLowerCase().equals("tesko")) {
                                                    caloriesValue = Math.round((float) duration / 60 * currentExercise.getTesko());
                                                }
                                                sumEx[0] += caloriesValue;
                                                tvSumExcercise.setText(Integer.toString(sumEx[0]));
                                            }
                                        }
                                    });
                                }
                            }
                        }
                        loadingContainer.setVisibility(View.GONE);
                    }
                });
    }

    private void initializeDatepicker() {
        dateContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog dialog = new DatePickerDialog(mainActivity, mDateListener, year, month, day);
                cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
                cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
                cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
                cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
                dialog.getDatePicker().setMaxDate(cal.getTimeInMillis());
                dialog.show();
            }
        });

        mDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                loadingContainer.setVisibility(View.VISIBLE);

                selectedDate = String.format("%02d%02d%04d", day, month, year);
                String selectedDateText = String.format("%d.%d.%d.", day, month, year);
                tvDate.setText(selectedDateText);

                addExcercise.setVisibility(View.GONE);
                addFood.setVisibility(View.GONE);

                if (Integer.parseInt(selectedDate) == Integer.parseInt(currentDate)) {
                    tvDate.setText("Danas");
                    addExcercise.setVisibility(View.VISIBLE);
                    addFood.setVisibility(View.VISIBLE);
                } else if (Integer.parseInt(selectedDate) == Integer.parseInt(currentDate) - 1000000) {
                    tvDate.setText("Jučer");
                }

                initializeDiaryFood();
                initializeDiaryExcercise();
            }
        };
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE_EDIT) {
            if (resultCode == RESULT_OK) {
//                initializeDiaryFood();
            }
        }
    }
}
