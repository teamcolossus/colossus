package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class SignInActivity extends AppCompatActivity {
    //TextView tvNotRegistered;
    Button btnSignIn;
    EditText username, password;
    FirebaseAuth mFirebaseAuth;
    FirebaseFirestore mFirestore;
    LinearLayout NotRegistered;
    RelativeLayout loadingContainer;

    private FirebaseAuth.AuthStateListener mAuthStateListener;

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        FirebaseApp.initializeApp(this);

        mFirestore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        username = findViewById(R.id.usernameSignIn);
        password = findViewById(R.id.passwordSignIn);
        btnSignIn = findViewById(R.id.btnSignIn);
        NotRegistered = findViewById(R.id.NotRegistered);
        loadingContainer = findViewById(R.id.loadingContainer);

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
                if (mFirebaseUser != null) {
                    Intent inToReroute = new Intent(getApplicationContext(), ReroutingActivity.class);
                    startActivity(inToReroute);
                    finish();
                    loadingContainer.setVisibility(View.GONE);
                }
            }
        };



        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] email = {username.getText().toString()};
                final String pwd = password.getText().toString();


                if (pwd.isEmpty()) {
                    password.setError("Upiši lozinku");
                    password.requestFocus();
                    return;
                }
                if (email[0].isEmpty()) {
                    username.setError("Upiši Email adresu ili korisničko ime");
                    username.requestFocus();
                } else if (!isValidEmail(email[0])) {
                    loadingContainer.setVisibility(View.VISIBLE);
                    mFirestore.collection("usernames").document(email[0]).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot.exists()) {
                                email[0] = documentSnapshot.getString("email");
                                mFirebaseAuth.signInWithEmailAndPassword(email[0], pwd).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        loadingContainer.setVisibility(View.GONE);
                                        Toast.makeText(getApplicationContext(), "Pogrešno korisničko ime ili lozinka, pokušaj ponovno", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } else {
                                loadingContainer.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), "Korisničko ime ne postoji", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else if (!(pwd.isEmpty())) {
                    loadingContainer.setVisibility(View.VISIBLE);
                    mFirebaseAuth.signInWithEmailAndPassword(email[0], pwd).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            loadingContainer.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "Pogrešan email ili lozinka, pokušaj ponovno", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        NotRegistered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToPop = new Intent(getApplicationContext(), ChooseUserTypePopUp.class);
                startActivity(intToPop);
            }
        });

        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    Log.i("SIGNIN","Enter pressed");
                    btnSignIn.performClick();
                }
                return false;
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
        mAuthStateListener = null;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    Log.d("focus", "touchevent");
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


}
