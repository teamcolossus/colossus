package com.colossus.poliklinikazarehabilitaciju;

import java.io.Serializable;

public class Diet extends UserData implements Serializable {
    private String opis, kalorija, masti, proteini, noSecer, noSol, noZasicene, ugljikohidrati, noMasti, noProteini, noUgljikohidrati;

    public Diet() {
        super();
    }

    public Diet(String opis, String kalorija, String masti, String proteini, String noSecer, String noSol, String noZasicene, String ugljikohidrati, String noMasti, String noProteini, String noUgljikohidrati) {
        this.opis = opis;
        this.kalorija = kalorija;
        this.masti = masti;
        this.proteini = proteini;
        this.noSecer = noSecer;
        this.noSol = noSol;
        this.noZasicene = noZasicene;
        this.ugljikohidrati = ugljikohidrati;
        this.noMasti = noMasti;
        this.noProteini = noProteini;
        this.noUgljikohidrati = noUgljikohidrati;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getKalorija() {
        return kalorija;
    }

    public void setKalorija(String kalorija) {
        this.kalorija = kalorija;
    }

    public String getMasti() {
        return masti;
    }

    public void setMasti(String masti) {
        this.masti = masti;
    }

    public String getProteini() {
        return proteini;
    }

    public void setProteini(String protieni) {
        this.proteini = protieni;
    }

    public String getNoSecer() {
        return noSecer;
    }

    public void setNoSecer(String noSecer) {
        this.noSecer = noSecer;
    }

    public String getNoSol() {
        return noSol;
    }

    public void setNoSol(String noSol) {
        this.noSol = noSol;
    }

    public String getNoZasicene() {
        return noZasicene;
    }

    public void setNoZasicene(String noZasicene) {
        this.noZasicene = noZasicene;
    }

    public String getUgljikohidrati() {
        return ugljikohidrati;
    }

    public void setUgljikohidrati(String ugljikohidrati) {
        this.ugljikohidrati = ugljikohidrati;
    }

    public String getNoMasti() {
        return noMasti;
    }

    public void setNoMasti(String noMasti) {
        this.noMasti = noMasti;
    }

    public String getNoProteini() {
        return noProteini;
    }

    public void setNoProteini(String noProteini) {
        this.noProteini = noProteini;
    }

    public String getNoUgljikohidrati() {
        return noUgljikohidrati;
    }

    public void setNoUgljikohidrati(String noUgljikohidrati) {
        this.noUgljikohidrati = noUgljikohidrati;
    }
}
