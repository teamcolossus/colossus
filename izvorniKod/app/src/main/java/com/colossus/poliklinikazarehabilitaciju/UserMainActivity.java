package com.colossus.poliklinikazarehabilitaciju;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class UserMainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navigationView;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseFirestore mFirestore;
    private LinearLayout navHeader;
    private TextView tvUsername, tvName;
    private FirebaseStorage mStorage;
    private ImageView profileNav;
    private User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
        mStorage = FirebaseStorage.getInstance();

        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation_user);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

        initializeNavigationDrawer();
        initializeNavHeader();

        //I added this if statement to keep the selected fragment when rotating the device
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new HomeUserFragment()).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    private void initializeNavHeader(){
        NavigationView nesto = findViewById(R.id.navigationDrawer);
        View navigationHeaderWrapper = nesto.getHeaderView(0);
        navHeader = navigationHeaderWrapper.findViewById(R.id.navigation_header_container);
        tvName = navigationHeaderWrapper.findViewById(R.id.nav_user_full_name);
        tvUsername = navigationHeaderWrapper.findViewById(R.id.nav_user_name);
        profileNav = navigationHeaderWrapper.findViewById(R.id.nav_profile_image);


        mFirestore.collection("klijent")
                .document(mFirebaseAuth.getCurrentUser().getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            String name = documentSnapshot.getString("ime");
                            String surname = documentSnapshot.getString("prezime");
                            String username = documentSnapshot.getString("korisnicko_ime");
                            currentUser = documentSnapshot.toObject(User.class).withUid(mFirebaseAuth.getCurrentUser().getUid());
                            tvName.setText(name + " " + surname);
                            tvUsername.setText("@" + username);

                            final String filepath = "profilePictures/" + mFirebaseAuth.getCurrentUser().getUid() + ".jpg";
                            mStorage.getReference().child(filepath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    StorageReference mImageRef = mStorage.getReference(filepath);
                                    GlideApp.with(getApplicationContext())
                                            .load(mImageRef)
                                            .into(profileNav);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    StorageReference mImageRef = mStorage.getReference("profilePictures/default.jpg");
                                    GlideApp.with(getApplicationContext())
                                            .load(mImageRef)
                                            .into(profileNav);
                                }
                            });

                        }
                    }
                });

        mFirestore.collection("klijent").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d("MAIN", "Error : " + e.getMessage());
                    return;
                }

                for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                    String uid = doc.getDocument().getId();

                    if (doc.getType() == DocumentChange.Type.MODIFIED) {
                        User user = doc.getDocument().toObject(User.class).withUid(uid);

                        if(user.getUid().equals(uid)){
                            initializeNavHeader();
                        }
                    }
                }
            }
        });


        navHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.nav_profile).performClick();
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });
    }

    private void initializeNavigationDrawer() {
        drawerLayout = findViewById(R.id.activity_main_user);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);

        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigationView = findViewById(R.id.navigationDrawer);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.drawer_diary:
                        findViewById(R.id.nav_diary).performClick();
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.drawer_training:
                        Intent intToTraining = new Intent(UserMainActivity.this, UserTrainingActivity.class);
                        intToTraining.putExtra("userUid", mFirebaseAuth.getCurrentUser().getUid());
                        intToTraining.putExtra("ADD", false);
                        startActivity(intToTraining);
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.drawer_diet:
                        Intent intToDiet = new Intent(UserMainActivity.this, MyDietActivity.class);
                        intToDiet.putExtra("USER_UID", mFirebaseAuth.getCurrentUser().getUid());
                        startActivity(intToDiet);
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.drawer_stats:
                        Intent intToStat= new Intent(UserMainActivity.this, StatsActivity.class);
                        intToStat.putExtra("USER_UID", mFirebaseAuth.getCurrentUser().getUid());
                        startActivity(intToStat);
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.drawer_doctor:
                        mFirestore.collection("klijent")
                                .document(mFirebaseAuth.getCurrentUser().getUid())
                                .get()
                                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        if (documentSnapshot.exists()) {
                                            String doctor = documentSnapshot.getString("doktor");

                                            if (doctor != null) {
                                                Intent intToEmployee = new Intent(getApplicationContext(), ProfileOfUsersEmployeeActivity.class);
                                                intToEmployee.putExtra("EMPLOYEE_UID", doctor);
                                                startActivity(intToEmployee);
                                                drawerLayout.closeDrawer(GravityCompat.START);
                                            } else {
                                                Intent intToChooseDoc = new Intent(getApplicationContext(), ChooseDoctor.class);
                                                startActivity(intToChooseDoc);
                                                drawerLayout.closeDrawer(GravityCompat.START);
                                            }
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Greška", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                        break;
                    case R.id.drawer_trainer:
                        mFirestore.collection("klijent")
                                .document(mFirebaseAuth.getCurrentUser().getUid())
                                .get()
                                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        if (documentSnapshot.exists()) {
                                            String trainer = documentSnapshot.getString("trener");

                                            if (trainer != null) {
                                                Intent intToEmployee = new Intent(getApplicationContext(), ProfileOfUsersEmployeeActivity.class);
                                                intToEmployee.putExtra("EMPLOYEE_UID", trainer);
                                                startActivity(intToEmployee);
                                                drawerLayout.closeDrawer(GravityCompat.START);
                                            } else {
                                                Intent intToChooseTrainer = new Intent(getApplicationContext(), ChooseTrainer.class);
                                                startActivity(intToChooseTrainer);
                                                drawerLayout.closeDrawer(GravityCompat.START);
                                            }
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Greška", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                        break;
                    case R.id.drawer_edit_profile:
                        Intent intToEdit = new Intent(UserMainActivity.this, EditProfileUser.class);
                        intToEdit.putExtra("USER", currentUser);
                        startActivity(intToEdit);
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.signOutDrawer:
                        FirebaseAuth.getInstance().signOut();
                        Intent intToMain = new Intent(UserMainActivity.this, SignInActivity.class);
                        startActivity(intToMain);
                        finish();
                        break;
                    default:
                        return true;
                }
                return true;
            }
        });


    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;

                    switch (item.getItemId()) {
                        case R.id.nav_home:
                            selectedFragment = new HomeUserFragment();
                            break;
                        case R.id.nav_diary:
                            selectedFragment = new DiaryFragment();
                            break;
                        case R.id.nav_profile:
                            selectedFragment = new MyProfileUserFragment();
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            selectedFragment).commit();

                    return true;
                }
            };
}