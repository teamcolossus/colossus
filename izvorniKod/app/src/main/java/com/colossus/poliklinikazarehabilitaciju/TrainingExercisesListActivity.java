package com.colossus.poliklinikazarehabilitaciju;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TrainingExercisesListActivity extends AppCompatActivity {

    private TextView tvDate, tvSumExcercise;
    private LinearLayout dateContainer;
    private RecyclerView mMainList;
    private RelativeLayout addExcercise, loadingContainer;
    private FirebaseFirestore mFirestore;
    private TrainingListAdapter2 trainingListAdapter;
    private List<Training> trainingList;
    public final static int REQ = 101;

    private String userUid;
    private String currentDate, selectedDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_exercises_list);

        userUid = (String) getIntent().getSerializableExtra("userUid");

        mFirestore = FirebaseFirestore.getInstance();

        // toolbar
        setTitle("Dodaj trening klijentu");
        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        tvDate = findViewById(R.id.tvDate);
        dateContainer = findViewById(R.id.dateContainer);
        loadingContainer = findViewById(R.id.loadingContainer);
        addExcercise = findViewById(R.id.addExcerciseContainer);
        tvSumExcercise = findViewById(R.id.tvSumExcercise);

        trainingList = new ArrayList<>();
        trainingListAdapter = new TrainingListAdapter2(trainingList);

        mMainList = findViewById(R.id.rvExcercise);
        mMainList.setLayoutManager(new LinearLayoutManager(this));
        mMainList.setNestedScrollingEnabled(false);
        mMainList.setHasFixedSize(true);
        mMainList.setAdapter(trainingListAdapter);

        currentDate = new SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(new Date());
        int day = Integer.parseInt(currentDate.substring(0,2));
        int month = Integer.parseInt(currentDate.substring(2,4));
        int year = Integer.parseInt(currentDate.substring(4,8));
        selectedDate = currentDate;

        getExercisesForTraining(year, month, day);

        dateContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(tvDate);
            }
        });

        addExcercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrainingExercisesListActivity.this, ExcerciseListActivity2.class);
                intent.putExtra("user_uid", userUid);
                intent.putExtra("datum", selectedDate);
                startActivityForResult(intent, REQ);
            }
        });
    }

    public void showDatePickerDialog(View v){
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void getExercisesForTraining(int year, int month, int day){
        loadingContainer.setVisibility(View.VISIBLE);
        tvSumExcercise.setText("0");
        final int[] sumEx = new int[1];

        trainingList.clear();
        trainingListAdapter.notifyDataSetChanged();

        selectedDate = String.format("%02d%02d%04d", day, month, year);
        String selectedDateText = String.format("%d.%d.%d.", day, month, year);
        tvDate.setText(selectedDateText);
        if (Integer.parseInt(selectedDate) == Integer.parseInt(currentDate)) {
            tvDate.setText("Danas");
        } else if (Integer.parseInt(selectedDate) == Integer.parseInt(currentDate) - 1000000) {
            tvDate.setText("Jučer");
        }

        mFirestore.collection("trening")
                .document(userUid)
                .collection("datum")
                .document(selectedDate)
                .collection("trening")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for(DocumentChange docChange : queryDocumentSnapshots.getDocumentChanges()){
                            QueryDocumentSnapshot qds = docChange.getDocument();

                            String uid = qds.getId();
                            System.out.println(uid + " " + qds.getData());

                            Training training = qds.toObject(Training.class).withUid(uid);

                            System.out.println(training.toString());
                            trainingList.add(training);
                            trainingListAdapter.notifyDataSetChanged();

                            final int duration = training.getTrajanje();
                            final String intensity = training.getIntenzitet();

                            mFirestore.collection("vjezbe").document(training.getId_vjezba()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    if (documentSnapshot.exists()) {
                                        int caloriesValue = 0;
                                        Exercise currentExercise = documentSnapshot.toObject(Exercise.class);

                                        if (intensity.toLowerCase().equals("lagano")) {
                                            caloriesValue = Math.round((float) duration / 60 * currentExercise.getLagano());
                                        } else if (intensity.toLowerCase().equals("srednje")) {
                                            caloriesValue = Math.round((float) duration / 60  * currentExercise.getSrednje());
                                        } else if (intensity.toLowerCase().equals("tesko")) {
                                            caloriesValue = Math.round((float) duration / 60 * currentExercise.getTesko());
                                        }
                                        sumEx[0] += caloriesValue;
                                        tvSumExcercise.setText(Integer.toString(sumEx[0]));
                                    }
                                }
                            });
                        }
                        loadingContainer.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ) {
            if (resultCode == RESULT_OK) {
                finish();
                startActivity(getIntent());
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
