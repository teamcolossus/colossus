package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileOfUsersEmployeeActivity extends AppCompatActivity {
    FirebaseFirestore mFirestore;
    FirebaseAuth mFirebaseAuth;
    FirebaseStorage mStorage;
    private Button endRelation, leaveReview;
    private TextView tvName, tvJob, tvScore, tvMail, tvReviewHeader;
    private ImageView profileImg, emailImage;
    private RelativeLayout loadingContainer;
    private Employee watchingEmployee;
    private String employeeUid;
    private List<Review> reviewList;
    private ReviewAdapter reviewAdapter;
    private RecyclerView mMainList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_of_users_employee);
        employeeUid = (String) getIntent().getSerializableExtra("EMPLOYEE_UID");

        // toolbar
        setTitle("");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
        mStorage = FirebaseStorage.getInstance();

        endRelation= findViewById(R.id.btnEndRelation);
        leaveReview= findViewById(R.id.btnLeaveReview);
        tvName = findViewById(R.id.profileName);
        tvJob = findViewById(R.id.profileJob);
        tvScore = findViewById(R.id.profileScore);
        tvMail = findViewById(R.id.emailProfileLabel);
        tvReviewHeader = findViewById(R.id.reviewLabelHeader);
        profileImg = findViewById(R.id.profilePicture);
        emailImage = findViewById(R.id.emailLogo);
        loadingContainer = findViewById(R.id.loadingContainer);

        initializeProfile();

        endRelation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(ProfileOfUsersEmployeeActivity.this).create();
                alertDialog.setTitle("Potvrdi prekid suradnje s:");
                alertDialog.setMessage(watchingEmployee.getFullName());
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Map<String,Object> assignMap = new HashMap<>();
                                if (watchingEmployee.getId_ovlasti().equals("3")){
                                    assignMap.put("trener", null);
                                    mFirestore.collection("trener").document(employeeUid).collection("klijenti").document(mFirebaseAuth.getCurrentUser().getUid()).delete();
                                } else {
                                    assignMap.put("doktor", null);
                                    mFirestore.collection("doktor").document(employeeUid).collection("klijenti").document(mFirebaseAuth.getCurrentUser().getUid()).delete();
                                }

                                mFirestore.collection("klijent").document(mFirebaseAuth.getCurrentUser().getUid()).update(assignMap);
                                mFirestore.collection("zaposlenik").document(employeeUid).update("trenutno_korisnika", FieldValue.increment(-1));
                                dialog.dismiss();
                                finish();
                            }
                        });
                alertDialog.show();
            }
        });

        leaveReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToReview = new Intent(ProfileOfUsersEmployeeActivity.this, ReviewActivity.class);
                intToReview.putExtra("EMPLOYEE_UID", employeeUid);
                intToReview.putExtra("USER_UID", mFirebaseAuth.getCurrentUser().getUid());
                startActivity(intToReview);
            }
        });
    }




    private void initializeReviews() {
        reviewList = new ArrayList<>();
        reviewAdapter = new ReviewAdapter(reviewList);

        mMainList = (RecyclerView) findViewById(R.id.reviewsListUser);
        mMainList.setHasFixedSize(true);
        mMainList.setLayoutManager(new LinearLayoutManager(this));
        mMainList.setNestedScrollingEnabled(false);
        mMainList.setAdapter(reviewAdapter);

        mFirestore.collection("recenzija").document(watchingEmployee.getUid()).collection("recenzija").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d("PROFIL", "Error : " + e.getMessage());
                    return;
                }

                for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                    if (doc.getType() == DocumentChange.Type.ADDED) {
                        String uid = doc.getDocument().getId();

                        Review review = doc.getDocument().toObject(Review.class);

                        reviewList.add(review);
                        reviewAdapter.notifyDataSetChanged();

                    }
                }
            }
        });
    }

    private void initializeProfile() {
        mFirestore.collection("zaposlenik")
                .document(employeeUid)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            watchingEmployee = documentSnapshot.toObject(Employee.class).withUid(employeeUid);
                            setTitle(watchingEmployee.getKorisnicko_ime());
                            tvName.setText(watchingEmployee.getFullName());
                            //tvUsername.setText("@" + username);
                            tvJob.setText(watchingEmployee.getId_ovlasti().equals("2") ? "Doktor" : "Trener");
                            tvMail.setText(watchingEmployee.getEmail());

                            double scoreValue =(Math.round(((float) watchingEmployee.getZbroj_ocjena() / watchingEmployee.getBroj_recenzija())*100)) /100.0;
                            tvScore.setText("Ocjena: "+Double.toString(scoreValue));

                            initializeReviews();

                            emailImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                                    emailIntent.setType("plain/text");
                                    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{watchingEmployee.getEmail()});
                                    startActivity(Intent.createChooser(emailIntent, "Pošalji mail koristeći:"));
                                }
                            });

                            final String filepath = "profilePictures/" + employeeUid + ".jpg";
                            mStorage.getReference().child(filepath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    StorageReference mImageRef = mStorage.getReferenceFromUrl(uri.toString());
                                    GlideApp.with(ProfileOfUsersEmployeeActivity.this)
                                            .load(mImageRef)
                                            .into(profileImg);
                                    loadingContainer.setVisibility(View.GONE);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    StorageReference mImageRef = mStorage.getReference("profilePictures/default.jpg");
                                    GlideApp.with(ProfileOfUsersEmployeeActivity.this)
                                            .load(mImageRef)
                                            .into(profileImg);
                                    loadingContainer.setVisibility(View.GONE);
                                }
                            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                }
                            });
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
