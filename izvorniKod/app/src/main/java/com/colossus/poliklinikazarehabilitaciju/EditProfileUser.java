package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class EditProfileUser extends AppCompatActivity {
    User currenUser;
    EditText usernameInput, nameInput, surnameInput;
    FirebaseAuth mFirebaseAuth;
    FirebaseFirestore mFirestore;
    ImageView profilePic, addPhotoIcon;
    FirebaseStorage mStorage;
    StorageReference storageRef;
    Uri picForUpload;
    private boolean picChanged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_user);
        currenUser = (User) getIntent().getSerializableExtra("USER");

        // toolbar
        setTitle("Uredi profil");
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mFirestore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mStorage = FirebaseStorage.getInstance();
        storageRef = mStorage.getReference();
        usernameInput = findViewById(R.id.usernameSignUpUser);
        nameInput = findViewById(R.id.nameSignUpUser);
        surnameInput = findViewById(R.id.surnameSignUpUser);
        profilePic = findViewById(R.id.registerPicture);
        addPhotoIcon = findViewById(R.id.addPhotoIcon);
        picChanged = false;


        final String filepath = "profilePictures/" + mFirebaseAuth.getCurrentUser().getUid() + ".jpg";
        mStorage.getReference().child(filepath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                StorageReference mImageRef = mStorage.getReference(filepath);
                GlideApp.with(EditProfileUser.this)
                        .load(mImageRef)
                        .into(profilePic);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                StorageReference mImageRef = mStorage.getReference("profilePictures/default.jpg");
                GlideApp.with(getApplicationContext())
                        .load(mImageRef)
                        .into(profilePic);
            }
        });

        initializeLayout();


        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                ImagePicker.Companion.with(EditProfileUser.this)
                        .crop(1f, 1f)
                        .compress(1024)
                        .maxResultSize(1080, 1080)
                        .start();
                picChanged = true;
            }
        });
    }

    public void initializeLayout() {
        usernameInput.setText(currenUser.getKorisnicko_ime());
        nameInput.setText(currenUser.getIme());
        surnameInput.setText(currenUser.getPrezime());
        addPhotoIcon.setVisibility(View.GONE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_menu_done:
                final String username = usernameInput.getText().toString().trim();
                final String name = nameInput.getText().toString().trim();
                final String surname = surnameInput.getText().toString().trim();
                boolean valid = true;


                if (surname.isEmpty()) {
                    surnameInput.setError("Upiši svoje prezime");
                    surnameInput.requestFocus();
                    valid = false;
                }

                if (name.isEmpty()) {
                    nameInput.setError("Upiši svoje ime");
                    nameInput.requestFocus();
                    valid = false;
                }

                if (username.isEmpty()) {
                    usernameInput.setError("Upiši korisničko ime");
                    usernameInput.requestFocus();
                    valid = false;
                }

                //TODO: provjeriti unikatnost usernamea i sadrzi li dobar format


                if (valid) {
                    mFirestore.collection("usernames").document(username).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if(documentSnapshot.exists()){
                                usernameInput.setError("Korisničko ime zauzeto");
                                usernameInput.requestFocus();
                            } else {
                                Map<String, Object> user = new HashMap<>();
                                user.put("korisnicko_ime", username);
                                user.put("ime", name);
                                user.put("prezime", surname);

                                Map<String, Object> toEmployee = new HashMap<>();
                                toEmployee.put("ime", name);
                                toEmployee.put("prezime", surname);

                                Map<String, Object> uName = new HashMap<>();
                                uName.put("email", currenUser.getEmail());

                                mFirestore.collection("klijent")
                                        .document(mFirebaseAuth.getCurrentUser().getUid())
                                        .update(user);

                                if (currenUser.getTrener() != null){
                                    mFirestore.collection("trener")
                                            .document(currenUser.getTrener())
                                            .collection("klijenti")
                                            .document(mFirebaseAuth.getCurrentUser().getUid())
                                            .update(toEmployee);
                                }

                                if (currenUser.getDoktor() != null){
                                    mFirestore.collection("doktor")
                                            .document(currenUser.getDoktor())
                                            .collection("klijenti")
                                            .document(mFirebaseAuth.getCurrentUser().getUid())
                                            .update(toEmployee);
                                }

                                mFirestore.collection("usernames")
                                        .document(currenUser.getKorisnicko_ime())
                                        .delete();

                                mFirestore.collection("usernames")
                                        .document(username)
                                        .set(uName);

                                //Upload slike
                                if(picChanged) {
                                    StorageReference riversRef = storageRef.child("profilePictures/" + mFirebaseAuth.getCurrentUser().getUid() + ".jpg");
                                    riversRef.putFile(picForUpload);
                                }

                                finish();
                            }
                        }
                    });


                }
                break;
        }
        return true;
    }


    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImagePicker.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                if (data == null) {
                    Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
                    return;
                }
                picForUpload = data.getData();
                profilePic.setImageURI(picForUpload);
                picChanged = true;
                addPhotoIcon.setVisibility(View.GONE);

                //You can get File object from intent
                File file = ImagePicker.Companion.getFile(data);

                //You can also get File Path from intent
                String filePath = ImagePicker.Companion.getFilePath(data);
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.Companion.getError(data), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Ne bi trebao dobiti ovu gresku", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    Log.d("focus", "touchevent");
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}
