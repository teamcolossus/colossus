package com.colossus.poliklinikazarehabilitaciju;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;


public class InsertExcerciseActivity2 extends AppCompatActivity {

    TextView tvNaziv,
            tvOpis,
            tvLagano,
            tvSrednje,
            tvTesko;

    ImageButton submit;

    FirebaseAuth mFirebaseAuth;
    FirebaseFirestore mFireStore;

    String odabranaVjezba = null; // ako treba urediti proizvod pa se salje id
    QueryDocumentSnapshot dokument;
    Bundle extra_params;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_exercise2);

        mFireStore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();

        tvNaziv = findViewById(R.id.tvNazivVjezbe);
        tvOpis = findViewById(R.id.tvOpisVjezbe);
        tvLagano = findViewById(R.id.tvLagano);
        tvSrednje = findViewById(R.id.tvSrednje);
        tvTesko = findViewById(R.id.tvTesko);
        submit = findViewById(R.id.btnSubmitTraining);

        extra_params = getIntent().getExtras();
        String nazivVjezbe = null;

        try {
            nazivVjezbe = String.valueOf(extra_params.get("naziv_vjezbe"));
            System.out.println("Naziv vjezbe je " + nazivVjezbe);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (nazivVjezbe != null) {
            mFireStore.collection("vjezbe")
                    .whereEqualTo("naziv", nazivVjezbe)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot qds : task.getResult()) {
                                    dokument = qds;
                                    odabranaVjezba = dokument.getId();

                                    tvNaziv.setText(dokument.get("naziv").toString());
                                    tvOpis.setText(dokument.get("opis").toString());
                                    tvLagano.setText(dokument.get("lagano").toString());
                                    tvSrednje.setText(dokument.get("srednje").toString());
                                    tvTesko.setText(dokument.get("tesko").toString());
                                }
                            }
                            else {
                                System.out.println("Nisam uspio naći vježbu");
                            }
                        }
                    });
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String naziv = tvNaziv.getText().toString();
                final String opis = tvOpis.getText().toString();
                final String lagano = tvLagano.getText().toString();
                final String srednje = tvSrednje.getText().toString();
                final String tesko = tvTesko.getText().toString();

                if (naziv.isEmpty()) {
                    tvNaziv.setError("Unesi naziv vježbe");
                    tvNaziv.requestFocus();
                }
                if (opis.isEmpty()) {
                    tvOpis.setError("Unesi naziv vježbe");
                    tvOpis.requestFocus();
                }
                if (lagano.isEmpty()) {
                    tvLagano.setError("Unesi naziv vježbe");
                    tvLagano.requestFocus();
                }
                if (srednje.isEmpty()) {
                    tvSrednje.setError("Unesi naziv vježbe");
                    tvSrednje.requestFocus();
                }
                if (tesko.isEmpty()) {
                    tvTesko.setError("Unesi naziv vježbe");
                    tvTesko.requestFocus();
                } else {
                    final Map<String, Object> vjezba = new HashMap<>();
                    vjezba.put("naziv", naziv);
                    vjezba.put("opis", opis);
                    vjezba.put("lagano", Integer.parseInt(lagano));
                    vjezba.put("srednje", Integer.parseInt(srednje));
                    vjezba.put("tesko", Integer.parseInt(tesko));
                    vjezba.put("is_deleted", false);

                    if (odabranaVjezba != null) {
                        // update existing product
                        mFireStore.collection("vjezbe")
                                .document(odabranaVjezba)
                                .set(vjezba)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        System.out.println("Uspjesno azuriranje vjezbe s idjem " + odabranaVjezba);
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        System.out.println("Neuspjelo azuriranje vjezbe s idjem " + odabranaVjezba);
                                    }
                                });
                    } else {
                        mFireStore.collection("vjezbe")
                                .add(vjezba)
                                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                    @Override
                                    public void onSuccess(DocumentReference documentReference) {
                                        System.out.println("Uspjesno stvorio vjezbu");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        System.out.println("Neuspjesno stvorarnje vjezbu");
                                    }
                                });
                    }
                    Intent intent = new Intent(InsertExcerciseActivity2.this, AdminMainActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}
